import json


class BuildInfoError(Exception):
    def __init__(self, msg):
        self.msg = msg


class BuildInfo(object):
    def __init__(self, version_string="", file_path=None, **kwargs):
        if kwargs:
            self.__version_info = {k: v for k, v in kwargs.iteritems()}
        else:
            self.__version_info = {}
            self.__read_version_string(version_string)
            if file_path is not None:
                self.read(file_path)

    def __read_version_string(self, version_string):
        self.__version_info["major"] = '0'
        self.__version_info["minor"] = '0'
        self.__version_info["revision"] = '0'
        self.__version_info["hf"] = None
        self.__version_info["commitID"] = "N/A"
        self.__version_info["buildNumber"] = 0
        self.__version_info["has_major"] = False
        self.__version_info["has_minor"] = False
        self.__version_info["has_revision"] = False
        self.__version_info["has_hf"] = False
        self.__version_info["has_commitID"] = False
        self.__version_info["has_buildNumber"] = False

        if not version_string:
            return
        if version_string.find(".") != -1:
            nums = version_string.split(".")
            if len(nums) >= 4:
                self.__version_info["major"] = nums[0]
                self.__version_info["minor"] = nums[1]
                self.__version_info["revision"] = nums[2]
                self.__version_info["hf"] = nums[3]
                self.__version_info["has_minor"] = True
                self.__version_info["has_revision"] = True
                self.__version_info["has_major"] = True
                self.__version_info["has_hf"] = True
            elif len(nums) >= 3:
                self.__version_info["major"] = nums[0]
                self.__version_info["minor"] = nums[1]
                self.__version_info["revision"] = nums[2]
                self.__version_info["has_minor"] = True
                self.__version_info["has_revision"] = True
                self.__version_info["has_major"] = True
            elif len(nums) == 2:
                self.__version_info["major"] = nums[0]
                self.__version_info["minor"] = nums[1]
                self.__version_info["has_minor"] = True
                self.__version_info["has_revision"] = False
                self.__version_info["has_major"] = True
            else:
                self.__version_info["major"] = nums[0]
                self.__version_info["has_minor"] = False
                self.__version_info["has_major"] = True

    @property
    def commit_id(self):
        return self.__version_info["commitID"]

    @property
    def build_number(self):
        return self.__version_info["buildNumber"]

    @property
    def version_major(self):
        return self.__version_info["major"]

    @property
    def version_minor(self):
        return self.__version_info["minor"]

    @property
    def revision_value(self):
        return self.__version_info["revision"]

    @property
    def hotfix_value(self):
        return "" if not self.has_hotfix else self.__version_info["hf"]

    @property
    def has_version_major(self):
        return self.__version_info["has_major"]

    @property
    def has_version_minor(self):
        return self.__version_info["has_minor"]

    @property
    def has_revision_value(self):
        return self.__version_info["has_revision"]

    @property
    def has_build_number(self):
        return self.__version_info["has_buildNumber"]

    @property
    def has_commit_id(self):
        return self.__version_info["has_commitID"]

    @property
    def has_hotfix(self):
        return self.__version_info["has_hf"]

    def __repr__(self):
        return "{D}".format(D=repr(self.__version_info))

    def read(self, file_path):
        # Let's try to read the buildInfo.json
        try:
            with open(file_path, "r") as f:
                lines = [l.strip() for l in f]
            json_values = json.loads("".join(lines))
            for key, no_value_kw, no_value in [("commitID", "N/A", "N/A"), ("buildNumber", "", 0)]:
                if key in json_values:
                    if json_values[key] != no_value_kw:
                        self.__version_info.update({key: json_values[key], "has_%s" % key: True})
        except OSError:
            raise BuildInfoError("No build information found")
        except IOError:
            raise BuildInfoError("Cannot read build information.")
