import build.NX_ManifestReader
import build.NX_BuilderException
import unittest
import mock
import os


class TestPackageManifestReader(unittest.TestCase):
    def setUp(self):
        os.environ["NX_PLATFORM"] = "Win64"
        os.environ["NX_BUILD_MODE"] = "release"
        os.environ["NX_BUILD_BITS"] = "32"
        os.environ["NX_COMPILER_TYPE"] = "MSVC"
        os.environ["NX_COMPILER_VERSION"] = "1900"
        os.environ["NX_TOOLS"] = ""

    def test_simple_case(self):
        manifest_file_path = os.path.join(".", "data", "package_manifest_simple_case.xml")
        mr = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        try:
            mr.read()
        except build.NX_BuilderException.ManifestError:
            self.fail("Unwanted ManifestError caught.")
        self.assertEqual(len(mr.manifest.entries), 7)
        # Tests only expected entries.
        self.assertTrue(os.path.normpath("path/to/entry1_wo_exe_rights") in mr.manifest.entries)
        self.assertTrue(os.path.normpath("path/to/entry2_w_exe_rights") in mr.manifest.entries)
        self.assertTrue(os.path.normpath("path/to/entry3_wo_exe_rights") in mr.manifest.entries)
        self.assertTrue(os.path.normpath("path/to/sub/entry1_wo_exe_rights") in mr.manifest.entries)
        self.assertTrue(os.path.normpath("path/to/sub/entry2_w_exe_rights") in mr.manifest.entries)
        self.assertTrue(os.path.normpath("path/to/sub/entry3_wo_exe_rights") in mr.manifest.entries)
        self.assertTrue(os.path.normpath("will/be/renamed_wo_exe") in mr.manifest.entries)
        for entry in mr.manifest.entries:
            if entry.find("wo") == -1:
                self.assertTrue(mr.manifest.entries[entry][1], entry)
            else:
                self.assertFalse(mr.manifest.entries[entry][1], entry)
            if entry == os.path.normpath("will/be/renamed_wo_exe"):
                # special case with a rename attribute.
                value = os.path.normpath("install/here/with_this_filename.txt")
                self.assertEqual(mr.manifest.entries[entry][0], value, entry)
            else:
                self.assertEqual(mr.manifest.entries[entry][0], entry.replace("path", "install"), entry)
        self.assertEqual(len(mr.manifest.links), 4)
        for i in range(1,len(mr.manifest.links)+1):
            link_name = os.path.normpath("valid/link_{}".format(i))
            link_as = os.path.normpath("valid/link_as_{}".format(i))
            link_cwd = os.path.normpath("/somewhere/link_cwd_{}".format(i))
            self.assertTrue(link_name in mr.manifest.links)
            self.assertEqual(link_as, mr.manifest.links[link_name][0])
            self.assertEqual(link_cwd, mr.manifest.links[link_name][1])

    def test_bad_entry_situation(self):
        manifest_file_path = os.path.join(".", "data", "package_manifest_entry_bad_executable_value.xml")
        reader = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        msg = "Invalid value given for 'executable' attribute while reading entry '.*'\.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, reader.read)
        manifest_file_path = os.path.join(".", "data", "package_manifest_entry_no_install_path.xml")
        reader = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        msg = "Bad entry value : empty 'install_path' attribute associated to 'path' attribute with value '.*'\.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, reader.read)
        manifest_file_path = os.path.join(".", "data", "package_manifest_entry_no_path.xml")
        reader = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        msg = "Empty 'path' attribute encountered while reading a node of type 'entry'\.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, reader.read)
        manifest_file_path = os.path.join(".", "data", "package_manifest_entry_present_twice.xml")
        reader = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        msg = "While reading package manifest : entry .* inserted twice\.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, reader.read)

    def test_bad_link_situation(self):
        manifest_file_path = os.path.join(".", "data", "package_manifest_link_no_path.xml")
        reader = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        msg = "Empty 'path' attribute encountered while reading a node of type 'link'\.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, reader.read)
        manifest_file_path = os.path.join(".", "data", "package_manifest_link_no_link_as.xml")
        reader = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        msg = "Empty 'link_as' attribute associated to 'path' attribute '.*'\.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, reader.read)

    def test_manifest_without_node_package(self):
        manifest_file_path = os.path.join(".", "data", "package_manifest_error_no_package.xml")
        mr = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        self.assertRaises(build.NX_BuilderException.ManifestError, mr.read)

    def test_bad_manifest_file(self):
        manifest_file_path = os.path.join(".", "data", "package_manifest_bad_file.xml")
        mr = build.NX_ManifestReader.PackageManifestReader(manifest_file_path)
        self.assertRaises(build.NX_BuilderException.ManifestError, mr.read)


class TestExternalManifestReader(unittest.TestCase):
    def setUp(self):
        os.environ["NX_PLATFORM"] = "SOL10"
        os.environ["NX_BUILD_MODE"] = "release"
        os.environ["NX_BUILD_BITS"] = "64"
        os.environ["NX_COMPILER_TYPE"] = "GCC"
        os.environ["NX_COMPILER_VERSION"] = "50300"
        os.environ["NX_TOOLS"] = "/BUILD"

    def test_bad_file(self):
        manifest_file_path = os.path.join(".", "data", "external_manifest_bad_file.xml")
        mr = build.NX_ManifestReader.ExternalManifestReader(manifest_file_path, "ABC", "1.1")
        self.assertRaises(build.NX_BuilderException.ManifestError, mr.read)

    def test_no_externals(self):
        manifest_file_path = os.path.join(".", "data", "external_manifest_no_externals.xml")
        mr = build.NX_ManifestReader.ExternalManifestReader(manifest_file_path, "ABC", "1.1")
        msg = "Invalid dependency manifest file : missing 'externals' node.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr.read)

    def test_unknown_dependency(self):
        manifest_file_path = os.path.join(".", "data", "external_manifest_simple_case.xml")
        mr_unknown = build.NX_ManifestReader.ExternalManifestReader(manifest_file_path, "ABC", "1.1")
        msg = "Cannot find dependency \[.*\] with version \[.*\]$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr_unknown.read)
        mr_unknown = build.NX_ManifestReader.ExternalManifestReader(manifest_file_path, "DEF", "1.0")
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr_unknown.read)

    def test_simple_case(self):
        manifest_file_path = os.path.join(".", "data", "external_manifest_simple_case.xml")
        mr = build.NX_ManifestReader.ExternalManifestReader(manifest_file_path, "ABC", "1.0")
        mr.read()
        self.assertEqual("a=1 b=1", mr.manifest.defines)
        self.assertEqual({"a": '1', "b": '1'}, mr.manifest.defines_dict)
        self.assertEqual(["-da", "-db", "-dc"], mr.manifest.cflags)
        self.assertEqual(["-ga", "-gb", "-gc"], mr.manifest.cxxflags)
        self.assertEqual(["ha", "hb", "hc"], mr.manifest.lib)
        self.assertEqual([os.path.normpath(i) for i in ["/lib_a", "/lib_b", "/lib_c"]], mr.manifest.lib_path)
        self.assertEqual(["-ja", "-jb", "-jc"], mr.manifest.link_flags)
        self.assertEqual([os.path.normpath(i) for i in ["../ra", "../rb", "../rc"]], mr.manifest.rpath)
        self.assertEqual(["sa", "sb", "sc"], mr.manifest.stlib)
        self.assertEqual([os.path.normpath(i) for i in ["/st_a", "/st_b", "/st_c"]], mr.manifest.stlib_path)
        self.assertEqual([os.path.normpath(i) for i in ["/inc_a", "/inc_b", "/inc_c"]], mr.manifest.includes)
        mr = build.NX_ManifestReader.ExternalManifestReader(manifest_file_path, "GHI", "1.0")
        mr.read()
        self.assertEqual(len(mr.manifest.package), 4)
        for i in ["a1", "a2", "b1", "b2"]:
            key = os.path.normpath("/this/entry/file_{}".format(i))
            self.assertTrue(key in mr.manifest.package)
            self.assertEqual(key, mr.manifest.package[key][0])
            self.assertEqual(os.path.normpath("/that/path_{}".format(i)), mr.manifest.package[key][1])
        self.assertEqual(len(mr.manifest.package_links), 4)
        for i in ["a1", "a2", "b1", "b2"]:
            key = os.path.normpath("/this/link/file_{}".format(i))
            self.assertTrue(key in mr.manifest.package_links)
            self.assertEqual(key, mr.manifest.package_links[key][0])
            self.assertEqual(os.path.normpath("/link_as_{}".format(i)), mr.manifest.package_links[key][1])
            self.assertEqual(os.path.normpath("/here_{}".format(i)), mr.manifest.package_links[key][2])
        self.assertEqual("GHI", mr.manifest.name_version_key)


class TestToolManifestReader(unittest.TestCase):
    def test_simple_case(self):
        with mock.patch("os.pathsep", "@#@"):
            manifest_file_path = os.path.join(".", "data", "tool_manifest_simple_case.xml")
            mr = build.NX_ManifestReader.ToolManifestReader(manifest_file_path, "tester", "1.a0")
            mr.read()
            self.assertEqual(len(mr.manifest.tools), 4)
            for i, tool in enumerate(["A", "B", "C", "D"]):
                self.assertTrue(tool in mr.manifest.tools)
                self.assertEqual(mr.manifest.tools[tool][0], os.path.normpath("/path/to/{}".format(tool)))
                self.assertEqual(mr.manifest.tools[tool][1], "bin32" if i % 2 == 0 else "bin64")
            self.assertEqual(len(mr.manifest.os_env), 2)
            self.assertTrue("TEST_MANIFEST" in mr.manifest.os_env)
            self.assertTrue("TEST_MANIFEST_COND_TRUE" in mr.manifest.os_env)
            self.assertTrue("TEST_MANIFEST_COND_FALSE" not in mr.manifest.os_env)
            self.assertEqual(mr.manifest.os_env["TEST_MANIFEST"], "a_b_c_d_e@#@f")
            self.assertEqual(mr.manifest.os_env["TEST_MANIFEST_COND_TRUE"], "ok")
            self.assertEqual(len(mr.manifest.ld_path), 2)
            self.assertTrue(os.path.normpath("/some/path/added/to/ld_path") in mr.manifest.ld_path)
            self.assertTrue(os.path.normpath("/another/path/added/to/ld_path") in mr.manifest.ld_path)
            self.assertEqual(mr.manifest.rule, "{A} -b {B} -c {C} {D}")

    def test_unknown_tool(self):
        manifest_file_path = os.path.join(".", "data", "tool_manifest_simple_case.xml")
        mr = build.NX_ManifestReader.ToolManifestReader(manifest_file_path, "tester_unknown", "1.a0")
        msg = "Cannot find tool \[.*\] with version \[.*\]$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr.read)

    def test_bad_file(self):
        manifest_file_path = os.path.join(".", "data", "tool_manifest_bad_file.xml")
        mr = build.NX_ManifestReader.ToolManifestReader(manifest_file_path, "tester", "1.a0")
        self.assertRaises(build.NX_BuilderException.ManifestError, mr.read)
        manifest_file_path = os.path.join(".", "data", "tool_manifest_no_tools.xml")
        mr = build.NX_ManifestReader.ToolManifestReader(manifest_file_path, "tester", "1.a0")
        msg = "Invalid tool manifest file : missing 'tools' node\.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr.read)

    def test_bad_command(self):
        manifest_file_path = os.path.join(".", "data", "tool_manifest_no_command.xml")
        mr = build.NX_ManifestReader.ToolManifestReader(manifest_file_path, "tester", "1.a0")
        msg = "No command provided for tool \[.*\] on version \[.*\]$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr.read)
        mr = build.NX_ManifestReader.ToolManifestReader(manifest_file_path, "tester", "1.0")
        msg = "Empty command given for tool \[.*\] on version \[.*\]$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr.read)


class TestCompilerManifestReader(unittest.TestCase):
    def setUp(self):
        os.environ["NX_PLATFORM"] = "AIX71"
        os.environ["NX_BUILD_MODE"] = "release"
        os.environ["NX_BUILD_BITS"] = "64"
        os.environ["NX_COMPILER_TYPE"] = "GCC"
        os.environ["NX_COMPILER_VERSION"] = "50300"
        os.environ["NX_TOOLS"] = "/BUILD"

    def test_simple_case(self):
        os.environ["NX_COMPILER_VERSION"] = "50301"
        manifest_file_path = os.path.join(".", "data", "compiler_manifest_simple_case.xml")
        mr = build.NX_ManifestReader.CompilerManifestReader(manifest_file_path)
        msg = "No compiler have been found of type \[.*\] with version \[.*\]"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr.read)
        os.environ["NX_COMPILER_VERSION"] = "50300"
        mr = build.NX_ManifestReader.CompilerManifestReader(manifest_file_path)
        self.assertEqual("", mr.manifest.tool_cxx)
        self.assertEqual("", mr.manifest.tool_cc)
        self.assertEqual("", mr.manifest.tool_ld)
        self.assertEqual("", mr.manifest.tool_ar)
        self.assertEqual([], mr.manifest.ar_flags)
        self.assertEqual("compiler_cxx", mr.manifest.tool_waf)
        mr.manifest.tool_waf = "toto"
        self.assertEqual("toto", mr.manifest.tool_waf)
        mr.read()
        self.assertEqual("GCC", mr.manifest.family)
        self.assertEqual("a=1 b=1", mr.manifest.defines)
        self.assertEqual({"a": '1', "b": '1'}, mr.manifest.defines_dict)
        self.assertEqual(["-da", "-db", "-dc"], mr.manifest.cflags)
        self.assertEqual(["-ga", "-gb", "-gc"], mr.manifest.cxxflags)
        self.assertEqual(["ha", "hb", "hc"], mr.manifest.lib)
        values = [os.path.normpath(i) for i in ["/lib/a", "/lib/b", "/lib/c", "/lib_a", "/lib_b", "/lib_c"]]
        self.assertEqual(values, mr.manifest.lib_path)
        self.assertEqual(["-ja", "-jb", "-jc"], mr.manifest.link_flags)
        self.assertEqual([os.path.normpath(i) for i in ["../ra", "../rb", "../rc"]], mr.manifest.rpath)
        self.assertEqual(["sa", "sb", "sc"], mr.manifest.stlib)
        self.assertEqual([os.path.normpath(i) for i in ["/st_a", "/st_b", "/st_c"]], mr.manifest.stlib_path)
        self.assertEqual([os.path.normpath(i) for i in ["/inc_a", "/inc_b", "/inc_c"]], mr.manifest.includes)
        self.assertEqual(os.path.normpath("/BUILD/compilers/AIX71/GCC/50300/tool/cxx"), mr.manifest.tool_cxx)
        self.assertEqual(os.path.normpath("/BUILD/compilers/AIX71/GCC/50300/tool/cc"), mr.manifest.tool_cc)
        self.assertEqual(os.path.normpath("/BUILD/compilers/AIX71/GCC/50300/tool/ld"), mr.manifest.tool_ld)
        self.assertEqual(os.path.normpath("/BUILD/compilers/AIX71/GCC/50300/tool/ar"), mr.manifest.tool_ar)
        self.assertEqual(["c"], mr.manifest.ar_flags)
        self.assertEqual(len(mr.manifest.package), 2)
        for i in ["a1", "a2"]:
            key = os.path.normpath("/this/entry/file_{}".format(i))
            self.assertTrue(key in mr.manifest.package)
            self.assertEqual(key, mr.manifest.package[key][0])
            self.assertEqual(os.path.normpath("/that/path_{}".format(i)), mr.manifest.package[key][1])
        self.assertEqual(len(mr.manifest.package_links), 2)
        for i in ["a1", "a2"]:
            key = os.path.normpath("/this/link/file_{}".format(i))
            self.assertTrue(key in mr.manifest.package_links)
            self.assertEqual(key, mr.manifest.package_links[key][0])
            self.assertEqual(os.path.normpath("/link_as_{}".format(i)), mr.manifest.package_links[key][1])
            self.assertEqual(os.path.normpath("/here_{}".format(i)), mr.manifest.package_links[key][2])

    def test_bad_file(self):
        manifest_file_path = os.path.join(".", "data", "compiler_manifest_bad_file.xml")
        mr = build.NX_ManifestReader.CompilerManifestReader(manifest_file_path)
        self.assertRaises(build.NX_BuilderException.ManifestError, mr.read)
        manifest_file_path = os.path.join(".", "data", "compiler_manifest_no_compilers.xml")
        mr = build.NX_ManifestReader.CompilerManifestReader(manifest_file_path)
        msg = "Invalid compiler manifest file : missing 'compilers' node\.$"
        self.assertRaisesRegexp(build.NX_BuilderException.ManifestError, msg, mr.read)


class TestManifestBase(unittest.TestCase):
    def test_pass(self):
        self.assertTrue(True)


class TestManifestReaderBase(unittest.TestCase):
    def test_pass(self):
        self.assertTrue(True)


def create_test_suite():
    suite = unittest.TestLoader().loadTestsFromTestCase(TestManifestReaderBase)
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(TestManifestBase))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(TestCompilerManifestReader))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(TestExternalManifestReader))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(TestPackageManifestReader))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(TestToolManifestReader))
    return suite
