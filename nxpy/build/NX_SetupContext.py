import os

import NX_ManifestReader
import common.NX_IDLGenerator
import common.NX_Utils

import waflib.Context
import waflib.ConfigSet
import waflib.Logs


def compute_idl_out_path_of(target_module, filename):
    dedicated_dir = os.path.basename(filename) + ".out"
    if target_module:
        return os.path.join(target_module, os.path.dirname(filename), "idl_build", dedicated_dir)
    else:
        return os.path.join(os.path.dirname(filename), "idl_build", dedicated_dir)


class SetupContext(waflib.Context.Context):
    cmd = "setup"
    fun = "setup"

    def __init__(self, **kwargs):
        super(SetupContext, self).__init__(**kwargs)
        self.env = waflib.ConfigSet.ConfigSet()
        self.line_just = 0
        self.env.load(os.path.join(waflib.Context.out_dir, "c4che", "_cache.py"))

    def is_generation_needed(self, file_path):
        mtime_original = os.path.getmtime(os.path.join(self.path.abspath(), file_path))
        # Look at the youngest output file, if any
        res = self.path.ant_glob(os.path.join(compute_idl_out_path_of("", file_path), "*"))
        outputs = [os.path.getmtime(i.abspath()) for i in res]
        if not outputs or len(outputs) < 5:
            # Kind of a stupid guard
            # outputs should have at least 2 .cpp and associated .h files plus 1 inl
            return True
        return min(outputs) < mtime_original

    def generate_idl(self, idl_srcs):
        tmr = NX_ManifestReader.ToolManifestReader(os.path.join(self.env["config_dir"], "tools.xml"), "idl", "6.1.7")
        tmr.extend_mappings_from_context(self)
        tmr.read()
        for f in idl_srcs:
            idl = common.NX_IDLGenerator.IDL(tmr.manifest.tools["IDL"][0])
            idl.gperf = tmr.manifest.tools["GPERF"][0]
            idl.program_path = os.path.dirname(tmr.manifest.tools["IDL"][0])
            idl.input = f
            idl.output = compute_idl_out_path_of(self.path.abspath(), f)
            idl.add_loader_path(tmr.manifest.ld_path)
            idl.tool_env = tmr.manifest.os_env
            idl.cwd = self.path.abspath()
            idl.rule = tmr.manifest.rule
            if self.is_generation_needed(f):
                common.NX_Utils.erase_fs_entry(idl.output)
                os.makedirs(idl.output)
                waflib.Logs.pprint("PINK", "Generating source files for [%s]" % f)
                idl.run()
            else:
                waflib.Logs.pprint("PINK", "Generated source files for [%s] are up-to-date -> skip" % f)
