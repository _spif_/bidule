import os

import waflib.Context
import waflib.ConfigSet
import waflib.Logs

import NX_BuilderException
import common.NX_Env
import common.NX_Utils


class TestContext(waflib.Context.Context):
    cmd = "test"
    fun = "test"

    def __init__(self, **kwargs):
        super(TestContext, self).__init__(**kwargs)
        self.env = waflib.ConfigSet.ConfigSet()
        self.env.load(os.path.join(waflib.Context.out_dir, "c4che", "_cache.py"))
        env = common.NX_Env.Env()
        self.__platform_id = env.platform_id_ext
        # we override "pkg_root" by playground location in order to use manifest links to populate the playground
        self.env["pkg_root"] = self.playground_root

    @property
    def playground_root(self):
        return os.path.join(waflib.Context.top_dir, "playground")

    @property
    def playground_bin_dir(self):
        return os.path.join(self.playground_root, "bin", self.__platform_id)

    @property
    def playground_lib_dir(self):
        return os.path.join(self.playground_root, "lib", self.__platform_id)

    @property
    def playground_log_dir(self):
        return os.path.join(self.playground_root, "log", self.__platform_id)

    @property
    def playground_result_dir(self):
        return os.path.join(self.playground_root, "results")

    def tst_msg(self, **kwargs):
        runner = " {}{}{}".format(waflib.Logs.colors("GREEN"), kwargs.get("rid", ""), waflib.Logs.colors("NORMAL"))
        rc_str = kwargs.get("rc", None)
        if rc_str is not None:
            waflib.Logs.info("{}{}{}{}".format(waflib.Logs.colors("NORMAL"), kwargs.get("prefix", ""),
                                               kwargs.get("msg", ""), runner))
            if rc_str == 0:
                rc_str = "status " + waflib.Logs.colors("GREEN") + "[OK]" + waflib.Logs.colors("NORMAL")
            else:
                fmt = "status {} [KO] (return code {}){}"
                rc_str = fmt.format(waflib.Logs.colors("RED"), kwargs.get("rc", -1), waflib.Logs.colors("NORMAL"))
            waflib.Logs.info("{N}   > {STATUS}".format(STATUS=rc_str, N=waflib.Logs.colors("NORMAL")))
        elif kwargs.get("playground", None) is not None:
            fmt = "{}Only playground have been requested. Ending test procedure.{}"
            waflib.Logs.info(fmt.format(waflib.Logs.colors("YELLOW"), waflib.Logs.colors("NORMAL")))
        else:
            waflib.Logs.info("{}{}{}{}".format(waflib.Logs.colors("NORMAL"), kwargs.get("prefix", ""),
                                               kwargs.get("msg", ""), runner))

    def init_playground(self):
        common.NX_Utils.erase_fs_entry(self.playground_bin_dir)
        common.NX_Utils.erase_fs_entry(self.playground_lib_dir)
        try:
            for d in [self.playground_bin_dir, self.playground_lib_dir]:
                os.makedirs(d)
        except OSError as os_err:
            raise NX_BuilderException.TestError("", "Cannot create playground structure: [{R}]".format(R=os_err))
        try:
            os.makedirs(self.playground_log_dir)
        except OSError:
            pass
        except Exception as exc_err:
            msg = "Unknown exception caught while creating log directory : [{R}]".format(R=exc_err)
            raise NX_BuilderException.TestError("", msg)
        try:
            os.makedirs(self.playground_result_dir)
        except OSError:
            pass
        except Exception as exc_err:
            msg = "Unknown exception caught while creating result directory : [{R}]".format(R=exc_err)
            raise NX_BuilderException.TestError("", msg)

    def add_target_to_playground(self, project):
        if project.is_shared_library():
            self.add_to_playground(project.name, lib=[project.name], compute_name=True, rel_to_bld=True)
        elif project.is_executable():
            self.add_to_playground(project.name, bin=[project.name], compute_name=True, rel_to_bld=True)

    def add_to_playground(self, project_name, **kwargs):
        module_path = self.path.abspath()[len(waflib.Context.top_dir):].lstrip(os.sep)
        target = ""
        try:
            for target in kwargs.get("bin", []):
                if kwargs.get("compute_name", True):
                    target = common.NX_Utils.make_bin_name(target)
                if kwargs.get("rel_to_bld", True):
                    target = os.path.join(waflib.Context.out_dir, module_path, target)
                name = os.path.basename(target) if "name" not in kwargs else kwargs.get("name", None)
                common.NX_Utils.sym_link(target, os.path.join(self.playground_bin_dir, name))
        except Exception as exc_err:
            msg = "Cannot add [{F}] to playground's bin directory : {R}".format(F=target, R=exc_err)
            raise NX_BuilderException.TestError(project_name, msg)
        try:
            for target in kwargs.get("lib", []):
                if kwargs.get("compute_name", True):
                    target = common.NX_Utils.make_shlib_name(target, "")
                if kwargs.get("rel_to_bld", True):
                    target = os.path.join(waflib.Context.out_dir, module_path, target)
                name = os.path.basename(target) if "name" not in kwargs else kwargs.get("name", None)
                common.NX_Utils.sym_link(target, os.path.join(self.playground_lib_dir, name))
        except Exception as exc_err:
            msg = "Cannot add [{F}] to playground's lib directory : {R}".format(F=target, R=exc_err)
            raise NX_BuilderException.TestError(project_name, msg)
