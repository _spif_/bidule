import os
import waflib.Errors


class ManifestError(waflib.Errors.WafError):
    """
    Error raised whenever a malformed entry is found while reading a manifest file.
    """
    def __init__(self, message, file_path):
        msg = "Manifest Error (from [%s])%s  > %s" % (file_path, os.linesep, message)
        super(ManifestError, self).__init__(msg, None)
        self.raw_msg = message


class ProjectError(waflib.Errors.WafError):
    """
    Error raised whenever a serious issue is encountered during Project operation
    """
    def __init__(self, message, prj_name):
        msg = "Project Error (from [%s])%s  > %s" % (prj_name, os.linesep, message)
        super(ProjectError, self).__init__(msg, None)
        self.raw_msg = message


class TestError(waflib.Errors.WafError):
    """
    Error raised whenever a serious issue is encountered during Test stage.
    """
    def __init__(self, runner_name, message):
        msg = "Test Error"
        if runner_name is not None:
            msg += " (in runner [%s])" % runner_name
        msg += "%s  > %s" % (os.linesep, message)
        super(TestError, self).__init__(msg, None)
        self.raw_msg = message


class PackageError(waflib.Errors.WafError):
    """
    Error raised whenever a serious issue is encountered during Package stage.
    """
    def __init__(self, message):
        msg = "Package Error%s  > %s" % (os.linesep, message)
        super(PackageError, self).__init__(msg, None)
        self.raw_msg = message
