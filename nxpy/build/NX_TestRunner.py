import common.NX_Utils
import common.NX_Env

from abc import abstractmethod, ABCMeta


class AbstractTestRunner(object):
    __metaclass__ = ABCMeta

    def __init__(self, name):
        self.__name = name
        self.__rc = 0
        self.__env = common.NX_Env.Env()

    @property
    def env(self):
        return self.__env

    @property
    def return_code(self):
        return self.__rc

    @return_code.setter
    def return_code(self, rc):
        self.__rc = rc

    @property
    def name(self):
        return self.__name

    @abstractmethod
    def set_options(self, waf_context=None):
        """
        Extend configuration options.
        :param waf_context: WAF Context to use for extending options. If None, means that standalone setup is requested.
        :type waf_context: waflib.Options.OptionContext
        """
        pass

    @abstractmethod
    def configure(self, waf_context):
        """
        Read options and export appropriate ones.
        :param waf_context: WAF Context to use for initialization and persisting data.
                            If None, means that standalone setup is requested.
        :type waf_context: waflib.Configure.ConfigurationContext
        """
        pass

    @abstractmethod
    def setup(self, waf_context=None):
        """
        Prepare the environment for playing these tests.
        :param waf_context: WAF Context to use for setting up the environment for testing purpose.
                            If None, means that standalone setup is requested.
        :type waf_context: NX_TestContext.TestContext
        """
        pass

    @abstractmethod
    def execute(self):
        """
        Run tests. ReturnCode value should be set here.
        """
        pass

    @abstractmethod
    def post_process(self):
        """
        Perform any post process operation (removing environment variables, cleaning, report convertion...).
        Might change ReturnCode value.
        """
        pass
