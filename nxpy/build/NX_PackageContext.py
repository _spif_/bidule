import os
import shutil

import waflib.Context
import waflib.ConfigSet
import waflib.Logs

import NX_BuilderException
import common.NX_Utils


class PackageContext(waflib.Context.Context):
    cmd = "package"
    fun = "package"

    def __init__(self, **kwargs):
        super(PackageContext, self).__init__(**kwargs)
        self.env = waflib.ConfigSet.ConfigSet()
        self.env.load(os.path.join(waflib.Context.out_dir, "c4che", "_cache.py"))
        self.__manifest_file_path = None

    @property
    def manifest_file(self):
        return self.__manifest_file_path

    @manifest_file.setter
    def manifest_file(self, value):
        self.__manifest_file_path = value

    @property
    def package_name(self):
        if hasattr(self.env, "pkg_root"):
            return os.path.basename(getattr(self.env, "pkg_root"))
        else:
            msg = "Cannot retrieve Package root directory. Are you sure the project has been configured?"
            raise NX_BuilderException.PackageError(msg)

    @property
    def package_root(self):
        if hasattr(self.env, "pkg_root"):
            return os.path.abspath(getattr(self.env, "pkg_root"))
        else:
            msg = "Cannot retrieve Package root directory. Are you sure the project has been configured?"
            raise NX_BuilderException.PackageError(msg)

    def make_package_file_path(self, *sub_dirs):
        return os.path.join(self.package_root, *sub_dirs)

    def pkg_msg_start(self):
        waflib.Logs.info("{}Starting package [{}{}{}]".format(waflib.Logs.colors("NORMAL"), waflib.Logs.colors("GREEN"),
                                                              self.package_name, waflib.Logs.colors("NORMAL")))

    def pkg_msg_end(self):
        waflib.Logs.info("{}Finalizing package [{}{}{}]".format(waflib.Logs.colors("NORMAL"),
                                                                waflib.Logs.colors("GREEN"),
                                                                self.package_name, waflib.Logs.colors("NORMAL")))

    def pkg_msg_check(self):
        waflib.Logs.info("Checking package integrity")

    def pkg_msg(self, **kwargs):
        kid = kwargs.get("id", "project")
        final_message = ""
        kwhat = "{}{}{}".format(waflib.Logs.colors("BLUE"), kwargs.get("item", None), waflib.Logs.colors("NORMAL"))
        if kid in ["project", "solution"]:
            final_message = "{COLS} + Packaging {L} file from [{NAME}] ".format(L=kid, NAME=kwargs.get("name", ""),
                                                                                COLS=waflib.Logs.colors("NORMAL"))
        elif kid == "compiler":
            final_message = "{COLS} + Packaging compiler file from [{NAME}] ".format(COLS=waflib.Logs.colors("NORMAL"),
                                                                                     NAME=kwargs.get("name", ""))
        elif kid == "jam":
            final_message = "{COLS} + Downloading jam archive ".format(COLS=waflib.Logs.colors("NORMAL"))
        elif kid == "external":
            final_message = "{COLS} + Packaging external file from [{NAME}] ".format(COLS=waflib.Logs.colors("NORMAL"),
                                                                                     NAME=kwargs.get("name", ""))
        elif kid == "compiler link":
            final_message = "{C} + Linking compiler file [{F}] from [{NAME}] as ".format(F=kwargs.get("file", ""),
                                                                                         C=waflib.Logs.colors("NORMAL"),
                                                                                         NAME=kwargs.get("name", ""))
        elif kid == "external link":
            final_message = "{C} + Linking external file [{F}] from [{NAME}] as ".format(F=kwargs.get("file", ""),
                                                                                         C=waflib.Logs.colors("NORMAL"),
                                                                                         NAME=kwargs.get("name", ""))
        waflib.Logs.info(final_message.ljust(80) + kwhat)

    def create_package_dirs(self, pkg_rel_path):
        try:
            os.makedirs(os.path.join(self.package_root, pkg_rel_path))
        except OSError as e:
            msg = "Cannot create package subpath [{}] : {}".format(pkg_rel_path, e)
            raise NX_BuilderException.PackageError(msg)
        except Exception as e:
            msg = "Cannot create package subpath [{}] (unknown exception) : {}".format(pkg_rel_path, e)
            raise NX_BuilderException.PackageError(msg)

    def copy_file(self, **kwargs):
        src = kwargs.get("source", "")
        dst = kwargs.get("dest", "")
        if not src:
            raise NX_BuilderException.PackageError("Cannot package file : no source file provided!")
        if not dst:
            raise NX_BuilderException.PackageError("Cannot package file [{F}] : no destination provided!".format(F=src))
        if self.manifest_file is not None:
            if src == self.manifest_file:
                self.manifest_file = dst
        try:
            shutil.copyfile(src, dst)
            self.pkg_msg(**kwargs)
        except Exception as e:
            raise NX_BuilderException.PackageError("Cannot package file [{F}] : '{R}'".format(F=src, R=e))

    def link_file(self, **kwargs):
        src = kwargs.get("source", "")
        link_as = kwargs.get("link_as", "")
        link_in = kwargs.get("link_in", "")
        if not src:
            raise NX_BuilderException.PackageError("Cannot package link : no source file provided!")
        if not link_as:
            raise NX_BuilderException.PackageError("Cannot package link on [{}] : no alias given!".format(src))
        if not link_in:
            msg = "Cannot package link on [{}] : no final destination given!".format(src)
            raise NX_BuilderException.PackageError(msg)
        try:
            common.NX_Utils.sym_link(src, link_as)
        except Exception as e:
            msg = "Cannot link file [{F}] from [{EXT}] as [{L}] : '{R}'".format(R=e, F=src, EXT=kwargs.get("name", ""),
                                                                                L=link_as)
            raise NX_BuilderException.PackageError(msg)
        try:
            shutil.move(link_as, os.path.join(os.path.abspath(link_in), link_as))
            self.pkg_msg(**kwargs)
        except Exception as e:
            msg = "Cannot move external link [{L}] from [{EXT}] : '{R}'".format(R=e, L=link_as,
                                                                                EXT=kwargs.get("name", ""))
            raise NX_BuilderException.PackageError(msg)

    def add_target_to_package(self, project, **kwargs):
        """
        :param project: Project object asking for packaging feature.
        :type project: NX_Project.AbstractProject
        """
        module_path = self.path.abspath()[len(waflib.Context.top_dir):].lstrip(os.sep)
        libs = kwargs.get("lib", [])
        for l in libs:
            if l:
                l_final = common.NX_Utils.make_shlib_name(l, None)
                src = os.path.join(waflib.Context.out_dir, module_path, l_final)
                dst = os.path.join(self.package_root, "lib", l_final)
                self.copy_file(source=src, dest=dst, id="project", name=project.name, item=os.path.basename(l_final))
        libs = kwargs.get("stlib", [])
        for l in libs:
            if l:
                l_final = common.NX_Utils.make_stlib_name(l)
                src = os.path.join(waflib.Context.out_dir, module_path, l_final)
                dst = os.path.join(self.package_root, "lib", l_final)
                self.copy_file(source=src, dest=dst, id="project", name=project.name, item=os.path.basename(l_final))
        bins = kwargs.get("bin", [])
        for b in bins:
            if b:
                b_final = common.NX_Utils.make_bin_name(b)
                src = os.path.join(waflib.Context.out_dir, module_path, b_final)
                dst = os.path.join(self.package_root, "bin", b_final)
                self.copy_file(source=src, dest=dst, id="project", name=project.name, item=os.path.basename(b_final))
        datas = kwargs.get("data", [])
        for d in datas:
            if d:
                src = os.path.join(waflib.Context.out_dir, module_path, d)
                dst = os.path.join(self.package_root, "data", d)
                self.copy_file(source=src, dest=dst, id="project", name=project.name, item=os.path.basename(d))

    def write_ascii_file_by_contents(self, file_dst_path, contents):
        if os.path.exists(os.path.join(self.package_root, file_dst_path)):
            raise NX_BuilderException.PackageError("Cannot create file [%s] since it already exists!" % file_dst_path)
        try:
            with open(os.path.join(self.package_root, file_dst_path), "w") as f:
                for i in contents:
                    f.write("%s%s" % (i, os.linesep))
        except OSError as os_err:
            raise NX_BuilderException.PackageError("Cannot create file [%s] : %s" % (file_dst_path, os_err))
        except Exception as exc_err:
            msg = "Cannot create file [{}] (unknown exception) : {}".format(file_dst_path, exc_err)
            raise NX_BuilderException.PackageError(msg)
