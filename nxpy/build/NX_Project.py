import os
import logging
import platform
import time
import shutil

import common.NX_Utils
import common.NX_Env
import common.NX_Logs
import NX_ManifestReader
import NX_BuilderException
import NX_SetupContext
import NX_TestContext
import NX_BuildInfo

import waflib.Context
import waflib.Options
import waflib.Logs
from waflib.TaskGen import feature, after

LOGGER_PRJ = "build.Project"
LOGGER_SOL = "build.Solution"

SOLUTION = 0
PROJECT_HEADER_ONLY = 1
PROJECT_SHLIB = 2
PROJECT_STLIB = 3
PROJECT_EXE = 4
PROJECT_SET = 5
PROJECT_BUNCH_OBJECTS = 6


@feature('*')
@after('process_rule')
def process_local_uses(self):
    """
    Local requirements must be correctly inserted in the build, using build order constraints.
    However, they must be removed from dependencies to avoid other tasks linking on them.
    """
    deps = getattr(self, 'local_use', [])
    use = getattr(self, 'use', [])
    usel = getattr(self, "uselib", [])
    for name in self.to_list(deps):
        try:
            while use.count(name) > 0:
                use.remove(name)
        except Exception as e:
            print e
        try:
            while usel.count(name) > 0:
                usel.remove(name)
        except Exception as e:
            print e
    setattr(self, "use", use)
    setattr(self, "uselib", usel)


class Abstract(object):
    """
    Base class for handling C++ Project within Neoxam' Compliance team.
    Provides basic features and common properties.
    """

    def __init__(self, name, version):
        """
        Constructs an Abstract project with given name and version.
        :param name: Name of the project. This name is used whenever a log/exception concerning
                     the project is dump/raised.
        :type name: string
        :param version: Optional version no.
        :type version: string
        """
        self.__name = name
        self.__BuildInfo = NX_BuildInfo.BuildInfo(version_string=version)
        self.__env = common.NX_Env.Env()
        self.__kind = None

    @property
    def type(self):
        return self.__kind

    @type.setter
    def type(self, value):
        if value not in [SOLUTION, PROJECT_HEADER_ONLY, PROJECT_SHLIB, PROJECT_STLIB,
                         PROJECT_EXE, PROJECT_SET, PROJECT_BUNCH_OBJECTS]:
            msg = "Invalid project type (try to it to '{T}')".format(T=value)
            raise NX_BuilderException.ProjectError(self.name, msg)
        self.__kind = value

    @property
    def env(self):
        """
        Retrieve an environment wrapper  object.
        :return: Environment wrapper object.
        :rtype: NX_Env.Env
        """
        return self.__env

    def read_version_info(self, containing_dir, waf_context):
        try:
            self.__BuildInfo.read(os.path.join(containing_dir, "buildInfo.json"))
            build_info_str = "{}-bld{}-{}".format(self.get_version(minor=True, revision=True),
                                                  self.__BuildInfo.build_number,
                                                  self.__BuildInfo.commit_id)
            waflib.Logs.pprint("GREEN", "build information : {}".format(build_info_str))
        except NX_BuildInfo.BuildInfoError, bie:
            waflib.Logs.pprint("YELLOW", "build information : {}".format(bie.msg))

        waf_context.env["version_info"] = self.__BuildInfo

    def get_version(self, minor=True, revision=True):
        """
        Compute a version string given some wanted/unwanted values. Both minor and revision values are inserted
        if requested and provided. Revision value is inserted if and only if minor has been requested.
        :param minor: Indicates if the minor project version is wanted.
        :param revision:Indicates if the revision value of the project version is wanted.
        :return: Project Version
        :rtype: string
        """
        values = [self.version_major]
        if minor and self.has_minor:
            values.append(self.version_minor)
            if revision and self.has_revision:
                values.append(self.revision_value)
        return ".".join(values)

    @property
    def version_major(self):
        """
        Retrieve the project' major version.
        :return: Project Version Major number
        :rtype: string
        """
        return self.__BuildInfo.version_major

    @property
    def has_major(self):
        return self.__BuildInfo.has_version_major

    @property
    def has_minor(self):
        return self.__BuildInfo.has_version_minor

    @property
    def has_revision(self):
        return self.__BuildInfo.has_revision_value

    @property
    def version_minor(self):
        """
        Retrieve the project' major version.
        :return: Project Version Major number
        :rtype: string
        """
        return self.__BuildInfo.version_minor

    @property
    def revision_value(self):
        """
        Retrieve the project' major version.
        :return: Project Version Major number
        :rtype: string
        """
        return self.__BuildInfo.revision_value

    @property
    def name(self):
        """
        Retrieve the project name.
        :return: Project Name
        :rtype: string
        """
        return self.__name

    def export_compiler(self, waf_context, manifest):
        """
        Serialize the settings needed to compile this project. We leave to WAF the responsibility to correctly
        persist these data.
        :param waf_context: Context given by waf.
        :type waf_context: waflib.Context
        :param manifest: Manifest object that contains common compiler options plus the one that might have been set
                         specifically for this project.
        :type manifest: NX_ManifestReader.CompilerManifest
        """
        mapping_waf_2_manifest = {"CXXFLAGS": "cxxflags", "CFLAGS": "cflags", "LIB": "lib", "LIBPATH": "lib_path",
                                  "STLIB": "stlib", "RPATH": "rpath",
                                  "STLIBPATH": "stlib_path", "LINKFLAGS": "link_flags", "PRIVINCLUDES": "includes"}
        for k in mapping_waf_2_manifest:
            v = common.NX_Utils.super_get_attr(manifest, mapping_waf_2_manifest[k]).fget(manifest)
            if v:
                waf_context.env["{PName}_{K}".format(PName=self.name, K=k)] = v
        if manifest.public_includes:
            waf_context.env["{PName}_INCLUDES".format(PName=self.name)] = manifest.public_includes
        if manifest.defines:
            waf_context.env["{PName}_PRIVDEFINES".format(PName=self.name)] = manifest.defines.split(" ")
        if manifest.public_defines:
            waf_context.env["{PName}_DEFINES".format(PName=self.name)] = manifest.public_defines.split(" ")
        if manifest.link_deps:
            waf_context.env["LINKDEPS_{PName}".format(PName=self.name)] = manifest.link_deps
        module_name = waf_context.path.get_bld().abspath()
        module_name = module_name[
                      module_name.find(waf_context.env["solution_bld"]) + len(waf_context.env["solution_bld"]) + 1:]
        waf_context.env["{P}_bld_path".format(P=self.name)] = module_name
        waf_context.env["{P}_type".format(P=self.name)] = self.type
        waf_context.env["{P}_target".format(P=self.name)] = self.name

    def extends_manifest_mapping(self, project_path):
        dval = {tid: val for tid, val in [("nx.project.type", self.type), ("nx.project.name", self.name),
                                          ("nx.project.version", self.get_version(minor=True, revision=True)),
                                          ("nx.project.path", project_path)]}
        dcond = {tid: val for tid, val in [("nx.project.type", self.type), ("nx.project.name", "'%s'" % self.name),
                                           ("nx.project.version",
                                            "'%s'" % self.get_version(minor=True, revision=True)),
                                           ("nx.project.path", "'%s'" % project_path)]}
        for tid, val in [("solution", SOLUTION), ("header", PROJECT_HEADER_ONLY), ("shlib", PROJECT_SHLIB),
                         ("stlib", PROJECT_STLIB),
                         ("exe", PROJECT_EXE), ("set", PROJECT_SET), ("objects", PROJECT_BUNCH_OBJECTS)]:
            dval["nx.project." + tid] = val
            dcond["nx.project." + tid] = val
        return dcond, dval

    @staticmethod
    def export_dependency(waf_context, manifest):
        """
        Export all information of a dependency into the waf_context' environment. We leave to WAF the responsibility to
        correctly persist these data.
        :param waf_context: Context given by WAF.
        :type waf_context: waflib.Context
        :param manifest: Manifest object that contains common compiler options plus the one that might have been set
                         specifically for this dependency.
        :type manifest: NX_ManifestReader.ExternalManifest
        """
        mapping_waf_2_manifest = {"CXXFLAGS": "cxxflags", "CFLAGS": "cflags", "STLIBPATH": "stlib_path",
                                  "LIBPATH": "lib_path", "STLIB": "stlib", "RPATH": "rpath", "LIB": "lib",
                                  "LINKFLAGS": "link_flags", "INCLUDES": "includes"}
        for k in mapping_waf_2_manifest:
            v = common.NX_Utils.super_get_attr(manifest, mapping_waf_2_manifest[k]).fget(manifest)
            if v:
                waf_context.env["{K}_{DName}".format(DName=manifest.name_version_key, K=k)] = v
        if manifest.defines:
            waf_context.env["DEFINES_{DName}".format(DName=manifest.name_version_key)] = manifest.defines.split(" ")

    def read_compiler_manifest_from_env(self, waf_context, manifest):
        """
        Reverse operation of ExportCompiler. Populate by overriding the contents of the given Manifest object file.
        :param waf_context: Context given by waf.
        :type waf_context: waflib.Context
        :param manifest: Manifest object that will contains common compiler options plus the one that might
                         have been set specifically for this project.
        :type manifest: NX_ManifestReader.CompilerManifest
        """
        mapping_waf_2_manifest = {"CXXFLAGS": "add_cxxflags", "CFLAGS": "add_cflags", "LIB": "add_lib",
                                  "LIBPATH": "add_lib_path", "STLIB": "add_stlib", "RPATH": "add_rpath",
                                  "STLIBPATH": "add_stlib_path", "LINKFLAGS": "add_link_flags",
                                  "PRIVINCLUDES": "add_includes"}
        for k in mapping_waf_2_manifest:
            if "{PName}_{K}".format(K=k, PName=self.name) in waf_context.env:
                prop_fun = common.NX_Utils.super_get_attr(manifest, mapping_waf_2_manifest[k])
                prop_fun(manifest, waf_context.env["%s_%s" % (self.name, k)])
        if "{PName}_INCLUDES".format(PName=self.name) in waf_context.env:
            manifest.add_public_includes(waf_context.env["%s_INCLUDES" % self.name])
        if "{PName}_PRIVDEFINES".format(PName=self.name) in waf_context.env:
            manifest.update_defines(waf_context.env["{PName}_PRIVDEFINES".format(PName=self.name)])
        if "{PName}_DEFINES".format(PName=self.name) in waf_context.env:
            manifest.update_public_defines(waf_context.env["{PName}_DEFINES".format(PName=self.name)])


class Solution(Abstract):
    """
    These class abstract a whole build for a given set of projects. There should be only one solution for building
    a single application, though each component of this application can be described using a Project object.
    """

    def __init__(self, name, **kwargs):
        super(Solution, self).__init__(name, kwargs.get("version", ""))
        self.__modules = []
        self.add_modules(*(kwargs.get("modules", [])))
        self.__config_dir = None
        self.config_dir = kwargs.get("config_dir", None)
        self.__externals = []
        self.add_external_keys(*(kwargs.get("externals", [])))
        self.__package_name = None
        self.package_name = kwargs.get("package_name", None)
        self.__package_algo = kwargs.get("package_algo", "tar")
        self.__external_manifests = {}

    def manage_package_config(self, waf_context):
        """
        Test if the package root must be deleted or if an fatal error must be sent through
        a builder exception. Called by Configure, Build and Package
        :param waf_context: Any WAF' context having loaded the options from NXPY
        """
        pkg_root = os.path.abspath(waf_context.env["pkg_root"])
        if waf_context.env["pkg_del"]:
            if os.path.exists(pkg_root):
                try:
                    shutil.rmtree(pkg_root)
                except Exception, e:
                    raise NX_BuilderException.ProjectError(
                        "Cannot remove package root path [{}] ! Reason is '{}'.".format(pkg_root, e), self.name)
        else:
            if os.path.exists(pkg_root):
                msg = "Package root path [{}] already exists! Configure with --pkg-del to cleanup previous package."
                raise NX_BuilderException.ProjectError(msg.format(pkg_root), self.name)

    @property
    def build_dir(self):
        bld_dir = "build.{ID}".format(ID=self.env.platform_id_ext)
        return bld_dir

    @property
    def modules(self):
        return self.__modules

    def add_modules(self, *modules):
        """
        Add the given list of modules to the module part of this solution.
        :param modules: sequence of module to add
        :type modules: klist
        """
        for m in modules:
            if m not in self.__modules:
                self.__modules.append(m)

    @property
    def package_name(self):
        if self.__package_name is None:
            return ""
        return self.__package_name

    @property
    def package_algo(self):
        if self.__package_algo is None:
            return ""
        return self.__package_algo

    @package_name.setter
    def package_name(self, name):
        self.__package_name = "{base}.{nxptf}".format(base=name, nxptf=self.env.platform.capitalize())

    @package_algo.setter
    def package_algo(self, algo):
        self.__package_algo = algo

    @property
    def external_keys(self):
        return self.__externals

    def add_external_keys(self, *keys):
        """
        Make the given external keys part of the solution.
        :param keys: list of external keys. Format is <KEY>:<VERSION>
        """
        for k in keys:
            if k not in self.__externals and k:
                self.__externals.append(k)

    @property
    def config_dir(self):
        if self.__config_dir is None:
            return ""
        return self.__config_dir

    @config_dir.setter
    def config_dir(self, path):
        if os.path.exists(path):
            self.__config_dir = path

    def make_config_file_path(self, *sub_dirs):
        return os.path.join(self.config_dir, *sub_dirs)

    def set_options(self, waf_context, *args):
        """
        Put in place the generic solution's options.
        :param waf_context: WAF context
        :type waf_context: waflib.OptionsContext
        """
        if not self.config_dir:
            waf_context.to_log("At solution : No config directory given! Cannot read compiler setup")
            raise NX_BuilderException.ProjectError("Solution : No config directory given! Cannot read compiler setup",
                                                   self.name)
        if not os.path.exists(os.path.join(self.config_dir, "compilers.xml")):
            waf_context.to_log("At solution : No 'compilers.xml' found! Cannot read compiler setup")
            raise NX_BuilderException.ProjectError("Solution : No 'compilers.xml' found! Cannot read compiler setup",
                                                   self.name)

        sol_group = waf_context.add_option_group("NXPy Options")
        sol_group.add_option("--asan", default=False, action="store_true", dest="asan",
                             help="Activate Address Sanitizer.")
        sol_group.add_option("--tsan", default=False, action="store_true", dest="tsan",
                             help="Activate Thread Sanitizer.")
        sol_group.add_option("--usan", default=False, action="store_true", dest="usan",
                             help="Activate Undefined Behavior Sanitizer.")
        sol_group.add_option("--coverage", default=False, action="store_true", dest="coverage",
                             help="Activate coverage analysis.")
        sol_group.add_option("--playground", default=False, action="store_true", dest="playground",
                             help="Do not run any tests and stop after having created the playground")
        pkg_group = waf_context.add_option_group("Package options")
        pkg_group.add_option("--pkg-del", default=False, action="store_true", dest="pkg_del",
                             help="Delete the package root directory before packaging and once done.")
        algo_default = "bztar" if self.env.platform != "Win64" else "zip"
        pkg_group.add_option("--pkg-algo", dest="pkg_algo", default=algo_default,
                             choices=["zip", "gztar", "bztar", "tar", "none"],
                             help="Sets the algorithm to use for package archiving (zip, tar, gztar, *bztar*, none).")

        if len(args):
            for i, r in enumerate(args):
                r.set_options(waf_context)

        tool_name = NX_ManifestReader.CompilerManifestReader(
            os.path.join(self.config_dir, "compilers.xml")).read_compiler_tool()
        waf_context.load(tool_name)

    def configure(self, waf_context, *runners):
        """
        Configure the whole project. Read compilers and dependencies if any, make them available to all sub-project.
        Then, the solution will look for all its module and configure them.
        :param waf_context: WAF context
        :type waf_context: waflib.ConfigureContext
        :param runners: list of runners to configure for test.
        :type runners: klist of build.NX_TestRunner.AbstractRunner
        """
        if not os.path.exists(self.env.tools_root_dir):
            waf_context.fatal("Invalid tools root dir [{D}]".format(D=self.env.tools_root_dir))

        common.NX_Logs.init_log_systems(os.path.join(self.build_dir, "NXPy.log"), "build.Project")
        if waf_context.options.asan and self.env.build_mode != "debug":
            raise NX_BuilderException.ProjectError("Cannot use address sanitizer option under non-debug builds!",
                                                   self.name)
        if waf_context.options.asan and self.env.os_name != "linux":
            raise NX_BuilderException.ProjectError("Address Sanitizer is allowed under Linux OS only.", self.name)
        waf_context.env["asan"] = waf_context.options.asan
        if waf_context.options.tsan and self.env.build_mode != "debug":
            raise NX_BuilderException.ProjectError("Cannot use Thread Sanitizer option under non-debug builds!",
                                                   self.name)
        if waf_context.options.tsan and self.env.os_name != "linux":
            raise NX_BuilderException.ProjectError("Thread Sanitizer is allowed under Linux OS only.", self.name)
        if waf_context.options.tsan and waf_context.options.asan:
            raise NX_BuilderException.ProjectError(
                "Cannot activate both Thread Sanitizer and Address Sanitizer within the same build.", self.name)
        waf_context.env["tsan"] = waf_context.options.tsan
        if waf_context.options.usan and self.env.build_mode != "debug":
            raise NX_BuilderException.ProjectError("Cannot use Thread Sanitizer option under non-debug builds!",
                                                   self.name)
        if waf_context.options.usan and self.env.os_name != "linux":
            raise NX_BuilderException.ProjectError("Thread Sanitizer is allowed under Linux OS only.", self.name)
        if waf_context.options.usan and waf_context.options.asan:
            raise NX_BuilderException.ProjectError(
                "Cannot activate both Undefined Behavior Sanitizer and Address Sanitizer within the same build.",
                self.name)
        if waf_context.options.usan and waf_context.options.tsan:
            raise NX_BuilderException.ProjectError(
                "Cannot activate both Undefined Behavior Sanitizer and Thread Sanitizer within the same build.",
                self.name)
        if waf_context.options.usan or waf_context.options.tsan or waf_context.options.asan:
            if waf_context.options.coverage:
                raise NX_BuilderException.ProjectError("Coverage cannot be computed with sanitizers activated.",
                                                       self.name)
        if waf_context.options.coverage and self.env.build_mode != "debug":
            raise NX_BuilderException.ProjectError("Coverage option require debug build mode.", self.name)
        waf_context.env["usan"] = waf_context.options.usan
        waf_context.env["coverage"] = waf_context.options.coverage
        waf_context.env["playground"] = waf_context.options.playground

        if waf_context.env["usan"]:
            waf_context.msg("Special build mode", "Undefined behaviour sanitizer")
        if waf_context.env["tsan"]:
            waf_context.msg("Special build mode", "Thread sanitizer")
        if waf_context.env["asan"]:
            waf_context.msg("Special build mode", "Address sanitizer")
        if waf_context.env["coverage"]:
            waf_context.msg("Special build mode", "Coverage")

        waf_context.env["solution_root"] = waflib.Context.top_dir
        waf_context.env["solution_bld"] = os.path.join(waf_context.env["solution_root"], self.build_dir)
        waf_context.env["pkg_algo"] = waf_context.options.pkg_algo
        waf_context.env["pkg_del"] = waf_context.options.pkg_del
        waf_context.env["pkg_root"] = os.path.join(os.path.dirname(os.path.abspath(self.build_dir)), self.package_name)
        if self.env.platform == "Win64":
            waf_context.env["pkg_root"] = waf_context.env["pkg_root"].replace("\\", "/")
        self.manage_package_config(waf_context)
        waf_context.env["config_dir"] = os.path.abspath(self.config_dir)
        self.package_algo = waf_context.env["pkg_algo"]
        if self.package_name:
            waf_context.to_log("Setting package name to [{PKGNAME}]".format(PKGNAME=self.package_name))
            waf_context.msg("Package name", self.package_name)
            if self.package_algo not in ["zip", "gztar", "bztar", "tar", "none"]:
                raise NX_BuilderException.ProjectError(
                    "Invalid package algorithm selected! Only 'tarball' or 'zip' are allowed.", self.name)
            else:
                waf_context.msg("Package algorithm", self.package_algo)
                waf_context.to_log("Setting packaging algorithm to [{ALGO}]".format(ALGO=self.package_algo))
                waf_context.to_log("Validating requirements for selected package algorithm...")
                if self.package_algo == "gztar":
                    waf_context.find_program("tar")
                    waf_context.find_program("gzip")
                elif self.package_algo == "bztar":
                    waf_context.find_program("tar")
                    waf_context.find_program("bzip2")
                elif self.package_algo == "tar":
                    waf_context.find_program("tar")
                elif self.package_algo == "zip" and self.env.os_name != "Win64":
                    # zip comes in by default under windows and cannot be checked like that.
                    waf_context.find_program("zip")
                else:
                    waf_context.to_log("Disabling package algorithm.")
        for e in self.external_keys:
            t = e.split(":")
            if len(t) != 2:
                msg = "At solution level : malformed external dependency key ['{}']. Must be of form '<KEY>:<VERSION>'"
                waf_context.to_log(msg.format(e))
                raise NX_BuilderException.ProjectError(msg.format(e), self.name)
            ext_key, ext_version = t[0], t[1]
            manifest_file = os.path.join(self.config_dir, "{P}.xml".format(P=ext_key))
            if not os.path.exists(manifest_file):
                msg = "No '{F}' found! Cannot read external dependency setup!".format(F=manifest_file)
                waf_context.to_log("At solution level : {M}".format(M=msg))
                raise NX_BuilderException.ProjectError(msg, self.name)
            dmr = NX_ManifestReader.ExternalManifestReader(manifest_file, ext_key, ext_version)
            dmr.extend_mappings_from_context(waf_context)
            dmr.read()
            if dmr.manifest.name_version_key in self.__external_manifests:
                msg = "External dependency [{D}] is referenced twice!".format(D=dmr.manifest.name)
                waf_context.to_log("At solution level : {}".format(msg))
                version = self.__external_manifests[dmr.manifest.name].version
                waf_context.to_log("At solution level : version [{V}] has already been loaded.".format(V=version))
                version = "cannot load version [{}] due to name collision.".format(dmr.manifest.version)
                waf_context.to_log("At solution level : {}".format(version))
                raise NX_BuilderException.ProjectError(msg, self.name)
            self.export_dependency(waf_context, dmr.manifest)
            self.__external_manifests[dmr.manifest.name_version_key] = dmr.manifest
            waf_context.to_log("At solution level : External dependency information dump")
            waf_context.to_log(dmr.manifest)
        mr = NX_ManifestReader.CompilerManifestReader(os.path.join(self.config_dir, "compilers.xml"))
        mr.extend_mappings_from_context(waf_context)
        mr.read()
        for p, k in [(mr.manifest.tool_cxx, "CXX"), (mr.manifest.tool_ar, "AR"), (mr.manifest.tool_ld, "LD"),
                     (mr.manifest.tool_cc, "CC"), (mr.manifest.ar_flags, "ARFLAGS")]:
            if p:
                waf_context.env[k] = p
        if self.env.os_name == "Win64":
            if self.env.build_bits == "64":
                waf_context.env['MSVC_TARGETS'] = ['x64']
            else:
                waf_context.env['MSVC_TARGETS'] = ['x86']
        waf_context.load(mr.manifest.tool_waf)
        if self.env.os_name == "Win64":
            # We need to force the tool 'winres' to be loaded in order to provide resource file compilation.
            waf_context.load('winres')
        waf_context.msg("Setting platform to", self.env.platform_id_ext)
        waf_context.to_log("Setting platform to [{PLATFORM}]".format(PLATFORM=self.env.platform_id_ext))
        if len(runners):
            for r in runners:
                r.configure(waf_context)
        if self.modules:
            waf_context.recurse(" ".join(self.modules))

    def setup(self, waf_context):
        """
        Main entry point for the setup command.
        :param waf_context: waf_context given by WAF for the setup command.
        :type waf_context: NX_SetupContext.SetupContext
        """
        # Handling versions so that all project can access it...
        self.read_version_info(self.config_dir, waf_context)
        if self.modules:
            waf_context.recurse(" ".join(self.modules))

    def build(self, waf_context):
        """
        Main entry point for the build command.
        :param waf_context: waf_context given by WAF for the build command.
        :type waf_context: waflib.Build.BuildContext
        """
        common.NX_Logs.init_log_systems(os.path.join(self.build_dir, "NXPy.log"), "build.Project")
        self.manage_package_config(waf_context)
        if self.modules:
            waf_context.recurse(self.modules)

    def test(self, waf_context, *runners):
        """
        Main entry point for the test command.
        :param waf_context: waf_context given by WAF for the test command.
        :type waf_context: NX_TestContext.TestContext
        :param runners: list of runners to configure for test.
        :type runners: klist of build.NX_TestRunner.AbstractRunner
        """
        common.NX_Logs.init_log_systems(os.path.join(self.build_dir, "NXPy.log"), "build.Project")
        waf_context.init_playground()
        # Preparing playground : importing externals.
        for e in self.external_keys:
            tmp = e.split(":")
            e_name, e_version = tmp[0], tmp[1]
            manifest_file = os.path.join(self.config_dir, "{BNAME}.xml".format(BNAME=e_name))
            dmr = NX_ManifestReader.ExternalManifestReader(manifest_file, e_name, e_version)
            dmr.extend_mappings_from_context(waf_context)
            dmr.read()
            for entry in dmr.manifest.package:
                cf, cd = dmr.manifest.package[entry]
                waf_context.add_to_playground(self.name, lib=[cf], compute_name=False, rel_to_bld=False,
                                              name=os.path.basename(cd))
            for entry in dmr.manifest.package_links:
                pf, pl, pi = dmr.manifest.package_links[entry]
                common.NX_Utils.sym_link(pf, os.path.join(pi, self.env.platform_id_ext, pl),
                                         waf_context.playground_root)
        # Preparing playground : importing compilers.
        cmr = NX_ManifestReader.CompilerManifestReader(os.path.join(self.config_dir, "compilers.xml"))
        cmr.extend_mappings_from_context(waf_context)
        cmr.read()
        for entry in cmr.manifest.package:
            cf, cd = cmr.manifest.package[entry]
            waf_context.add_to_playground(self.name, lib=[cf], compute_name=False, rel_to_bld=False,
                                          name=os.path.basename(cd))
        # Preparing playground : importing projects.
        waf_context.recurse(" ".join(self.modules))
        if waf_context.env.playground:
            waf_context.tst_msg(playground=True)
            return
        if len(runners) == 0:
            raise NX_BuilderException.TestError(None, "Cannot complete test command : no runner(s) have been given!")
        final_rc = 0
        for r in runners:
            waf_context.tst_msg(prefix="Test: ", msg="Starting runner", rid=r.name)
            try:
                r.setup(waf_context)
                r.execute()
                r.post_process()
                waf_context.tst_msg(prefix="Test: ", msg="Finished running runner", rid=r.name, rc=r.return_code)
            except NX_BuilderException.TestError:
                final_rc += 1
                waf_context.tst_msg(prefix="Test: ", msg="Stop running runner", rid=r.name, rc=1)
            except Exception, e:
                final_rc += 1
                waf_context.tst_msg(prefix="Test: ", msg="Stop running runner (reason is {E})".format(E=str(e)),
                                    rid=r.name, rc=1)
        if final_rc != 0:
            raise NX_BuilderException.TestError("Message from solution '{S}'".format(S=self.name),
                                                "At least one runner failed to complete successfully!")

    def __package_init(self, waf_context):
        """
        Setup the location where the packaging will be done. Create required subdirectories and write all scripts.
        :param waf_context: context given by WAF.
        :type waf_context: NX_PackageContext.PackageContext
        """
        self.manage_package_config(waf_context)
        waf_context.create_package_dirs(os.path.abspath(self.package_name))
        for p in ["bin", "lib", "scripts", "data"]:
            waf_context.create_package_dirs(os.path.join(waf_context.package_root, p))
        waf_context.create_package_dirs(os.path.join(waf_context.package_root, "lib", "runtime"))
        waf_context.create_package_dirs(os.path.join(waf_context.package_root, "data", "manifests"))
        # Print environment.
        env_contents = []
        for k, v in zip(["NX_TOOLS", "NX_COMPILER_TYPE", "NX_COMPILER_VERSION", "NX_BUILD_BITS", "NX_BUILD_MODE",
                         "NX_PLATFORM"],
                        [self.env.tools_root_dir, self.env.compiler_type, self.env.compiler_version,
                         self.env.build_bits, self.env.build_mode, self.env.platform]):
            env_contents.append("export {K}={V}".format(K=k, V=v))
        env_contents.insert(0, "# You can source the file to setup your environment ready for a build")
        env_contents.insert(0, "# ")
        env_contents.insert(0, "# Host was [{HOST}]".format(HOST=platform.node()))
        env_contents.insert(0, "# Built on {DATE}".format(DATE=time.asctime()))
        waf_context.write_ascii_file_by_contents(os.path.join("scripts", "nx_env.sh"), env_contents)
        waf_context.pkg_msg(id="solution", item=os.path.join("scripts", "nx_env.sh"), name=self.name)

    def package_finalize(self, waf_context):
        """
        Construct the archive.
        :param waf_context: Context given by WAF.
        :type waf_context: NX_TestContext.TestContext
        """
        if waf_context.manifest_file:
            waf_context.copy_file(**{"source": waf_context.manifest_file, "item": waf_context.manifest_file,
                                    "dest": waf_context.make_package_file_path("manifest.xml"), "id": "solution",
                                    "name": self.name})
            waf_context.pkg_msg_check()
            # Setting up reader.
            reader = NX_ManifestReader.PackageManifestReader(waf_context.manifest_file)
            # We do not need to extend the reader for checking the integrity. We will simply perform a file check.
            try:
                reader.read()
                manifest_entries = [i.replace("\\", "/") for i in reader.manifest.entries.keys()]

                # List all files under package root, except manifest.xml
                pkg_root_len = len(waf_context.package_root)
                all_files_in_pkg = [item[pkg_root_len + 1:].replace("\\", "/") for item in
                                    common.NX_Utils.list_all_files_in(waf_context.package_root)]
                # we remove the manifest, only flle which is not to be declared.
                all_files_in_pkg.remove(waf_context.manifest_file[pkg_root_len + 1:])
                only_in_package = [item for item in all_files_in_pkg if item not in manifest_entries]
                only_in_manifest = [item for item in manifest_entries if item not in all_files_in_pkg]

                errors = []
                if only_in_package:
                    errors.extend([" + File has not been declared : {F}".format(F=i) for i in only_in_package])
                if only_in_manifest:
                    errors.extend([" - File not found in archive : {F}".format(F=i) for i in only_in_manifest])
                if errors:
                    print os.linesep.join(errors)
                    raise NX_BuilderException.PackageError("Integrity check failed")
            except NX_BuilderException.PackageError, e:
                raise e
        try:
            waf_context.pkg_msg_end()
            fmt = waf_context.env["pkg_algo"]
            if fmt != "none":
                shutil.make_archive(os.path.abspath(self.package_name), fmt, os.path.abspath(waf_context.package_name))
            if waf_context.env["pkg_del"] and os.path.exists(waf_context.package_root):
                shutil.rmtree(waf_context.package_root)
        except Exception, e:
            msg = "Cannot finalize package [{PKG}] : '{R}'".format(PKG=self.package_name, R=e)
            raise NX_BuilderException.PackageError(msg)

    def package(self, waf_context, finalize=True):
        """
        Main entry point for the package command. At solution level, we create the place, package compiler dependencies,
        external dependencies and make requested links for both compilers and externals.
        :param waf_context: waf_context given by WAF for the package command.
        :type waf_context: NX_PackageContext.PackageContext
        :param finalize: If True, the compression will done at the end of the call,
                         otherwise it is the caller's responsibility.
        :type finalize: bool
        """
        common.NX_Logs.init_log_systems(os.path.join(self.build_dir, "NXPy.log"), "build.Project")
        if waf_context.cmd != "package":
            msg = "Call to NX_Project.Solution::Package method without a valid context! Command is [{CMD}]"
            raise NX_BuilderException.ProjectError(msg.format(CMD=waf_context.cmd), self.name)
        if waf_context.fun != "package":
            msg = "Call to NX_Project.Solution::Package from [{}] : cannot be made outside the package function!"
            raise NX_BuilderException.ProjectError(msg.format(waf_context.fun), self.name)
        waf_context.pkg_msg_start()
        self.__package_init(waf_context)
        cmr = NX_ManifestReader.CompilerManifestReader(os.path.join(self.config_dir, "compilers.xml"))
        cmr.extend_mappings_from_context(waf_context)
        cmr.read()
        for entry in cmr.manifest.package:
            cf, cd = cmr.manifest.package[entry]
            waf_context.copy_file(source=cf, dest=cd, id="compiler", item=os.path.basename(os.path.abspath(cf)),
                                  name=self.env.compiler_id)
        for entry in cmr.manifest.package_links:
            lcf, lca, lci = cmr.manifest.package_links[entry]
            waf_context.link_file(source=lcf, link_as=lca, link_in=lci,
                                  id="compiler link", item=os.path.basename(os.path.abspath(lca)),
                                  file=os.path.basename(os.path.abspath(lcf)), name=self.env.CompilerID)
        for e in self.external_keys:
            tmp = e.split(":")
            e_name, e_version = tmp[0], tmp[1]
            manifest_file = os.path.join(self.config_dir, "{BNAME}.xml".format(BNAME=e_name))
            dmr = NX_ManifestReader.ExternalManifestReader(manifest_file, e_name, e_version)
            dmr.extend_mappings_from_context(waf_context)
            dmr.read()
            for entry in dmr.manifest.package:
                cf, cd = dmr.manifest.package[entry]
                waf_context.copy_file(source=cf, dest=cd, id="external", item=os.path.basename(os.path.abspath(cf)),
                                      name=dmr.manifest.name)
            for entry in dmr.manifest.package_links:
                lef, lea, lei = dmr.manifest.package_links[entry]
                waf_context.link_file(source=lef, link_in=lei, link_as=lea,
                                      id="external link", item=os.path.basename(os.path.abspath(lea)),
                                      name=dmr.manifest.name, file=os.path.basename(os.path.abspath(lef)))
        if self.modules:
            waf_context.recurse(self.modules)
        if finalize:
            self.package_finalize(waf_context)


class Project(Abstract):
    def __init__(self, name, **kwargs):
        super(Project, self).__init__(name, kwargs.get("version", ""))
        self.__src_dirs = []
        self.__src_files = []
        self.__inc_dirs = []
        self.__priv_inc_dirs = []
        self.__defines = {}
        self.__priv_defines = {}
        self.__lib = []
        self.__libpath = []
        self.__stlib = []
        self.__stlibpath = []
        self.__linkflags = []
        self.__rpath = []
        self.__cflags = []
        self.__cxxflags = []
        self.__requires = []
        self.__loc_requires = []
        self.__idl_srcs = []
        self.__f_excludes = []
        self.__resource_file = []
        self.__compiler = None
        self.type = kwargs.get("type", PROJECT_SHLIB)
        if self.type not in [PROJECT_SHLIB, PROJECT_EXE, PROJECT_STLIB]:
            raise NX_BuilderException.ProjectError("Invalid project type!", self.name)
        self.add_src_dirs(*(kwargs.get("src", [])))
        self.add_src_dirs("src")
        self.add_public_include_dirs("@include@", *(kwargs.get("public_includes", [])))
        self.add_private_include_dirs("@include@", *(kwargs.get("includes", [])))
        self.add_public_defines(kwargs.get("public_defines", {}))
        self.add_private_defines(kwargs.get("defines", {}))
        self.add_lib(*(kwargs.get("lib", [])))
        self.add_lib_path(*(kwargs.get("libpath", [])))
        self.add_stlib(*(kwargs.get("stlib", [])))
        self.add_stlib_path(*(kwargs.get("stlibpath", [])))
        self.add_link_flags(*(kwargs.get("linkflags", [])))
        self.add_cflags(*(kwargs.get("cflags", [])))
        self.add_cxxflags(*(kwargs.get("cxxflags", [])))
        self.add_rpath(*(kwargs.get("rpath", [])))
        self.add_needs(*(kwargs.get("use", [])))
        self.add_local_needs(*(kwargs.get("local_use", [])))
        self.add_idl_sources(*(kwargs.get("idl", [])))
        self.add_resource_file(*(kwargs.get("rc", [])))

    def is_set_of_project(self):
        return self.type == PROJECT_SET

    def is_executable(self):
        return self.type == PROJECT_EXE

    def is_shared_library(self):
        return self.type == PROJECT_SHLIB

    def is_static_library(self):
        return self.type == PROJECT_STLIB

    def is_header_only(self):
        return self.type == PROJECT_HEADER_ONLY

    @staticmethod
    def solution_dir(waf_context=None):
        if waf_context:
            return waf_context.env["solution_root"]
        return waflib.Context.top_dir

    @property
    def resource_file(self):
        return self.__resource_file

    def add_resource_file(self, *value):
        """
        Adds resouce file. This functionality is available only under Windows. On other systems, adding resources
        has no effect on final build parameter.
        :param value: List of resource files you wish to add.
        :rtype value: list
        """
        for v in value:
            if v not in self.resource_file:
                self.__resource_file.append(v)

    @property
    def idl_sources(self):
        return self.__idl_srcs

    def add_idl_sources(self, *files):
        """
        Adds additional idl file to compile before proceeding to c++ compilation step.
        Each file is given with current project considered as root node.
        :param files: idl files to generates
        :type files: klist of string
        """
        for f in files:
            if f not in self.__idl_srcs:
                self.__idl_srcs.append(f)

    def add_files_to_exclusion_list(self, *files):
        """
        Exclude files from the build.
        :param files: files to exclude
        :type files: klist of string
        """
        for f in files:
            if f not in self.__f_excludes:
                self.__f_excludes.append(f)

    @property
    def exclusion_list(self):
        return self.__f_excludes

    @property
    def project_properties(self):
        prop = {"target": self.name}
        if self.requires:
            prop["use"] = self.requires
        if self.local_needs:
            prop["local_use"] = self.local_needs
            if "use" not in prop:
                prop["use"] = []
            for i in self.local_needs:
                if i not in prop["use"]:
                    prop["use"].append(i)
        for c, k in [(self.compiler.lib, "lib"), (self.compiler.lib_path, "libpath"), (self.compiler.stlib, "stlib"),
                     (self.compiler.stlib_path, "stlibpath"),
                     (self.compiler.cflags, "cflags"), (self.compiler.cxxflags, "cxxflags"),
                     (self.compiler.link_flags, "linkflags"), (self.compiler.rpath, "rpath"),
                     (self.compiler.includes, "includes"), (self.compiler.public_includes, "export_includes"),
                     (self.sources, "source")
                     ]:
            if c:
                prop[k] = c
        if "source" not in prop:
            prop["source"] = []
        if self.resource_file and self.env.os_name == "Win64":
            for r in self.resource_file:
                if r not in prop["source"]:
                    prop["source"].append(r)
        if self.type == PROJECT_SHLIB:
            prop["features"] = "cxx cxxshlib"
        elif self.type == PROJECT_STLIB:
            prop["features"] = "cxx cxxstlib"
        elif self.type == PROJECT_EXE:
            prop["features"] = "cxx cxxprogram"
        t = ["{K}={V}".format(K=i, V=self.public_defines[i]) for i in self.public_defines]
        if t:
            prop["export_defines"] = " ".join(t)
        dfn = {i: self.compiler.defines_dict[i] for i in self.compiler.defines_dict}
        dfn.update(self.public_defines)
        if dfn:
            prop["defines"] = " ".join(["{K}={V}".format(K=i, V=dfn[i]) for i in dfn])
        return prop

    @property
    def sources(self):
        return self.__src_files

    @property
    def requires(self):
        """
        :rtype: list
        """
        return self.__requires

    @property
    def local_needs(self):
        return self.__loc_requires

    @property
    def src_dirs(self):
        return self.__src_dirs

    @property
    def public_include_dirs(self):
        return self.__inc_dirs

    @property
    def private_include_dirs(self):
        return self.__priv_inc_dirs

    @property
    def public_defines(self):
        return self.__defines

    @property
    def private_defines(self):
        return self.__priv_defines

    @property
    def libs(self):
        return self.__lib

    @property
    def lib_path(self):
        return self.__libpath

    @property
    def stlib(self):
        return self.__stlib

    @property
    def stlib_path(self):
        return self.__stlibpath

    @property
    def link_flags(self):
        return self.__linkflags

    @property
    def rpath(self):
        return self.__rpath

    @property
    def cflags(self):
        return self.__cflags

    @property
    def cxxflags(self):
        return self.__cxxflags

    @property
    def compiler(self):
        """
        Retrieve the compiler manifest read in the file.
        :return: The compiler manifest.
        :rtype: NX_ManifestReader.CompilerManifest
        """
        return self.__compiler

    def add_local_needs(self, *local_req):
        """
        Instruct the project that the list of requirements is 'local'. We mean by local that
        if the project is itself a requirement of another project, then its local needs won't be
        added on the command line of such a project.
        :param local_req: List of requirements Keys.
        """
        for lr in local_req:
            if lr.count(":") != 0:
                lr = "".join(lr.split(":"))
            if lr not in self.__loc_requires:
                self.__loc_requires.append(lr)

    def add_needs(self, *req):
        """
        Add a requirement to the list of needs for this project.
        :param req: list of requirement key in the form <NAME>:<VERSION>
        :type req: klist of string
        """
        for r in req:
            if r.count(":") != 0:
                r = "".join(r.split(":"))
            if r not in self.__requires:
                self.__requires.append(r)

    def add_src_dirs(self, *dirs):
        for d in dirs:
            if d not in self.__src_dirs:
                self.__src_dirs.append(d)

    def add_public_include_dirs(self, *dirs):
        for d in dirs:
            if d not in self.__inc_dirs:
                self.__inc_dirs.append(d)

    def add_private_include_dirs(self, *dirs):
        for d in dirs:
            if d not in self.__priv_inc_dirs:
                self.__priv_inc_dirs.append(d)

    def add_public_defines(self, defs):
        self.__defines.update(defs)

    def add_private_defines(self, defs):
        self.__priv_defines.update(defs)

    def add_lib(self, *libs):
        for l in libs:
            if l not in self.__lib:
                self.__lib.append(l)

    def add_lib_path(self, *lib_paths):
        for l in lib_paths:
            if l not in self.__libpath:
                self.__libpath.append(l)

    def add_stlib(self, *libs):
        for l in libs:
            if l not in self.__stlib:
                self.__stlib.append(l)

    def add_stlib_path(self, *lib_path):
        for l in lib_path:
            if l not in self.__stlibpath:
                self.__stlibpath.append(l)

    def add_link_flags(self, *flags):
        for f in flags:
            if f not in self.__linkflags:
                self.__linkflags.append(f)

                self.__linkflags.append(f)

    def add_rpath(self, *paths):
        for p in paths:
            if p not in self.__rpath:
                self.__rpath.append(p)

    def add_cflags(self, *flags):
        for f in flags:
            if f not in self.__cflags:
                self.__cflags.append(f)

    def add_cxxflags(self, *flags):
        for f in flags:
            if f not in self.__cxxflags:
                self.__cxxflags.append(f)

    def add_src_files(self, *files):
        for f in files:
            if f not in self.sources and not f.abspath().endswith(".donotbuild"):
                self.__src_files.append(f)

    def set_options(self, waf_context):
        pass

    def __export_compiler(self, waf_context):
        mr = NX_ManifestReader.CompilerManifestReader(os.path.join(waf_context.env["config_dir"], "compilers.xml"))
        mr.extend_mappings_from_context(waf_context)
        module_name = waf_context.path.get_bld().abspath()
        solution_build = waf_context.env["solution_bld"]
        module_name = module_name[module_name.find(solution_build) + len(solution_build) + 1:]
        mr.extend_mapping(*self.extends_manifest_mapping(module_name))
        mr.read()
        self.__compiler = mr.manifest
        # We need to merge it with local setup
        self.compiler.add_lib(self.libs)
        self.compiler.add_lib_path(self.lib_path)
        self.compiler.add_stlib(self.stlib)
        self.compiler.add_stlib_path(self.stlib_path)
        self.compiler.add_cflags(self.cflags)
        self.compiler.add_cxxflags(self.cxxflags)
        self.compiler.add_link_flags(self.link_flags)
        self.compiler.add_rpath(self.rpath)
        self.compiler.add_includes(self.private_include_dirs)
        self.compiler.add_public_includes(self.public_include_dirs)
        self.compiler.update_public_defines(["{}={}".format(i, self.public_defines[i]) for i in self.public_defines])
        self.compiler.update_defines(["{}={}".format(i, self.private_defines[i]) for i in self.private_defines])
        if self.local_needs:
            self.compiler.link_deps = self.local_needs
        waf_context.to_log("At project [{P}] level : Compiler information dump".format(P=self.name))
        waf_context.to_log(self.compiler)
        self.export_compiler(waf_context, self.compiler)

    def get_absolute_idl_paths(self, waf_context):
        paths = []
        mod = waf_context.path.abspath()
        for f in self.idl_sources:
            final_path = NX_SetupContext.compute_idl_out_path_of(mod, f)
            if final_path not in paths:
                paths.append(final_path)
        return paths

    def configure(self, waf_context):
        path = os.path.join(waf_context.path.abspath(), "include")
        for i, v in enumerate(self.public_include_dirs):
            if v.startswith("@include@"):
                self.public_include_dirs[i] = self.public_include_dirs[i].replace("@include@", path)
        for i, v in enumerate(self.private_include_dirs):
            if v.startswith("@include@"):
                self.private_include_dirs[i] = self.private_include_dirs[i].replace("@include@", path)
        paths = self.get_absolute_idl_paths(waf_context)
        paths = [p[len(waf_context.path.abspath()):].lstrip(os.sep) for p in paths]
        self.add_private_include_dirs(*paths)
        self.__export_compiler(waf_context)

    def setup(self, waf_context):
        waf_context.generate_idl(self.idl_sources)

    def read_local_needs(self, waf_context):
        """
        :param waf_context: build context from WAF
        :type waf_context: waflib.Build.BuildContext
        :return:
        """
        # The idea is to merge information from these requirements in a way that do not export these information
        # on target depending on this project. Hence, a call to this method should occur once the configure
        # step is done. We will then be able to rely on the cache to extract the required information.
        for loc in self.local_needs:
            logging.getLogger(LOGGER_PRJ).info(
                "Processing local need [{L}] for project [{P}]".format(P=self.name, L=loc))
            if "{L}_target".format(L=loc) not in waf_context.env:
                values = {"{K}_{L}".format(K=key, L=loc): [] for key in
                          ["INCLUDES", "DEFINES", "LINKFLAGS", "CXXFLAGS", "CFLAGS", "STLIB", "STLIBPATH", "LIB",
                           "LIBPATH"]}
                b_loc_is_external = True
            else:
                values = {"{L}_{K}".format(K=key, L=loc): [] for key in
                          ["INCLUDES", "DEFINES", "LINKFLAGS", "CXXFLAGS", "CFLAGS", "STLIB", "STLIBPATH", "LIB",
                           "LIBPATH"]}
                b_loc_is_external = False
                for mandatory_info in ["type", "target", "bld_path"]:
                    logging.getLogger(LOGGER_PRJ).info("Trying to access to {L}_{M}".format(L=loc, M=mandatory_info))
                    if "{L}_{M}".format(L=loc, M=mandatory_info) not in waf_context.env:
                        msg = "Nothing known about local dependency [{L}], missing mandatory {M} information"
                        raise NX_BuilderException.ProjectError(self.name, msg.format(L=loc, M=mandatory_info))
                    else:
                        values[mandatory_info] = waf_context.env["{L}_{M}".format(L=loc, M=mandatory_info)]
            for idx, k in enumerate(values):
                if k in waf_context.env:
                    values[k] = waf_context.env[k]
            msg = "Local dependency [{L}] is {P}external".format(L=loc, P="not " if not b_loc_is_external else "")
            logging.getLogger(LOGGER_PRJ).info(msg)
            logging.getLogger(LOGGER_PRJ).info("Local dependency [{L}] property set: {D}".format(L=loc, D=values))
            if not b_loc_is_external:
                if values["type"] == PROJECT_STLIB:
                    self.compiler.add_stlib([values["target"]])
                    self.compiler.add_stlib_path([values["bld_path"]])
                elif values["type"] == PROJECT_SHLIB:
                    self.compiler.add_lib([values["target"]])
                    self.compiler.add_lib_path([values["bld_path"]])
                self.compiler.add_includes(values["{L}_INCLUDES".format(L=loc)])
                self.compiler.update_defines(values["{L}_DEFINES".format(L=loc)])
                self.compiler.add_link_flags(values["{L}_LINKFLAGS".format(L=loc)])
                self.compiler.add_cxxflags(values["{L}_CXXFLAGS".format(L=loc)])
                self.compiler.add_cflags(values["{L}_CFLAGS".format(L=loc)])
                self.compiler.add_stlib(values["{L}_STLIB".format(L=loc)])
                self.compiler.add_stlib_path(values["{L}_STLIBPATH".format(L=loc)])
                self.compiler.add_lib_path(values["{L}_LIBPATH".format(L=loc)])
                self.compiler.add_lib(values["{L}_LIB".format(L=loc)])
            else:
                self.compiler.add_includes(values["INCLUDES_{L}".format(L=loc)])
                self.compiler.update_defines(values["DEFINES_{L}".format(L=loc)])
                self.compiler.add_link_flags(values["LINKFLAGS_{L}".format(L=loc)])
                self.compiler.add_cxxflags(values["CXXFLAGS_{L}".format(L=loc)])
                self.compiler.add_cflags(values["CFLAGS_{L}".format(L=loc)])
                self.compiler.add_stlib(values["STLIB_{L}".format(L=loc)])
                self.compiler.add_stlib_path(values["STLIBPATH_{L}".format(L=loc)])
                self.compiler.add_lib(values["LIB_{L}".format(L=loc)])
                self.compiler.add_lib_path(values["LIBPATH_{L}".format(L=loc)])

    def load_context(self, waf_context):
        # We reload it in order to have some checks in case the developer might have
        # change its environment since configure.
        mr = NX_ManifestReader.CompilerManifestReader(os.path.join(waf_context.env["config_dir"], "compilers.xml"))
        mr.extend_mappings_from_context(waf_context)
        module_name = waf_context.path.get_bld().abspath()
        module_name = module_name[
                      module_name.find(waf_context.env["solution_bld"]) + len(waf_context.env["solution_bld"]) + 1:]
        mr.extend_mapping(*self.extends_manifest_mapping(module_name))
        logging.getLogger(LOGGER_PRJ).info("Loading context of project [{P}]".format(P=self.name))
        mr.read()
        self.__compiler = mr.manifest
        self.read_compiler_manifest_from_env(waf_context, self.compiler)
        # logging.getLogger(LOGGER_PRJ).info("Analyzing local dependencies of project [{P}]".format(P=self.Name))
        # self.ReadLocalNeeds(waf_context)
        logging.getLogger(LOGGER_PRJ).info("Processing IDL for project [{P}]".format(P=self.name))
        for i in self.idl_sources:
            # IDL files are expected to be given as relative. So here, we compute the path in which
            # generated files are put. It will end with "idl_build/<idlfile>.out". We then add it back
            # to the src dir and let the magic operate.
            idl = os.path.join(i[:i.find(os.path.basename(i)) - 1], "idl_build", os.path.basename(i) + ".out")
            self.add_src_dirs(idl)
        logging.getLogger(LOGGER_PRJ).info("Computing source files for project [{P}]".format(P=self.name))
        for i in self.src_dirs:
            self.add_src_files(*(waf_context.path.ant_glob(os.path.join(i, "*.c*"))))
        logging.getLogger(LOGGER_PRJ).info(
            "Project [{P}] has {L} identified source files".format(P=self.name, L=len(self.__src_files)))
        logging.getLogger(LOGGER_PRJ).info("Processing exclusion list for project [{P}]".format(P=self.name))
        for f in self.exclusion_list:
            for i in [s for s in self.__src_files if s.abspath().endswith(f)]:
                logging.getLogger(LOGGER_PRJ).info(
                    "Removing [{F}] from source file of project [{P}]".format(P=self.name, F=i))
                self.__src_files.remove(i)
        logging.getLogger(LOGGER_PRJ).info(
            "Project [{P}] has {L} source files to compile".format(P=self.name, L=len(self.__src_files)))
        logging.getLogger(LOGGER_PRJ).info(
            "Final project properties for [{P}] : {D}".format(P=self.name, D=self.project_properties))
        logging.getLogger(LOGGER_PRJ).info("-" * 80)

    def build(self, waf_context):
        self.load_context(waf_context)
        if self.type == PROJECT_STLIB:
            waf_context.stlib(**self.project_properties)
        elif self.type == PROJECT_SHLIB:
            waf_context.shlib(**self.project_properties)
        elif self.type == PROJECT_EXE:
            waf_context.program(**self.project_properties)
        else:
            raise NX_BuilderException.ProjectError(self.name,
                                                   "Invalid project type encountered while proceeding to build.")

    def package(self, waf_context):
        pass

    def test(self, waf_context):
        pass


class SetOfProject(object):
    def __init__(self, modules):
        self.__modules = modules

    @property
    def modules(self):
        return self.__modules

    def set_options(self, waf_context):
        pass

    def configure(self, waf_context):
        if self.modules:
            waf_context.recurse(" ".join(self.modules))

    def build(self, waf_context):
        if self.modules:
            waf_context.recurse(self.modules)

    def setup(self, waf_context):
        if self.modules:
            waf_context.recurse(" ".join(self.modules))

    def package(self, waf_context):
        if self.modules:
            waf_context.recurse(" ".join(self.modules))

    def test(self, waf_context):
        if self.modules:
            waf_context.recurse(" ".join(self.modules))


class HeaderOnly(Abstract):
    def __init__(self, name, **kwargs):
        super(HeaderOnly, self).__init__(name, kwargs.get("version", ""))
        self.__inc_dirs = []
        self.__priv_inc_dirs = []
        self.__defines = {}
        self.__priv_defines = {}
        self.__requires = []
        self.__compiler = None
        self.__kind = PROJECT_HEADER_ONLY
        self.add_public_include_dirs("@include@", *(kwargs.get("public_includes", [])))
        self.add_private_include_dirs("@include@", *(kwargs.get("includes", [])))
        self.add_public_defines(kwargs.get("public_defines", {}))
        self.add_private_defines(kwargs.get("defines", {}))
        self.add_needs(*(kwargs.get("use", [])))

    @property
    def project_properties(self):
        prop = {"target": self.name}
        if self.requires:
            prop["use"] = self.requires
        for c, k in [(self.compiler.includes, "includes"), (self.compiler.public_includes, "export_includes"),
                     (self.public_defines, "export_defines")]:
            if c:
                prop[k] = c
        dfn = {}
        for i in self.compiler.defines.split(" "):
            if i:
                s = i.split("=")
                dfn[s[0]] = s[1]
        dfn.update(self.__priv_defines)
        if dfn:
            prop["defines"] = " ".join(["{K}={V}".format(K=i, V=dfn[i]) for i in dfn])
        return prop

    @property
    def requires(self):
        return self.__requires

    @property
    def public_include_dirs(self):
        return self.__inc_dirs

    @property
    def private_include_dirs(self):
        return self.__priv_inc_dirs

    @property
    def public_defines(self):
        return self.__defines

    @property
    def private_defines(self):
        return self.__priv_defines

    @property
    def compiler(self):
        """
        Retrieve the compiler manifest read in the file.
        :return: The compiler manifest.
        :rtype: NX_ManifestReader.CompilerManifest
        """
        return self.__compiler

    def add_needs(self, *req):
        for r in req:
            if r.count(":") != 0:
                r = "".join(r.split(":"))
            if r not in self.__requires:
                self.__requires.append(r)

    def add_public_include_dirs(self, *dirs):
        for d in dirs:
            if d not in self.__inc_dirs:
                self.__inc_dirs.append(d)

    def add_private_include_dirs(self, *dirs):
        for d in dirs:
            if d not in self.__priv_inc_dirs:
                self.__priv_inc_dirs.append(d)

    def add_public_defines(self, defs):
        self.__defines.update(defs)

    def add_private_defines(self, defs):
        self.__priv_defines.update(defs)

    def set_options(self, waf_context):
        pass

    def __export_compiler(self, waf_context):
        self.__compiler = NX_ManifestReader.CompilerManifest("hlib", self.env.compiler_version)
        # We need to merge it with local setup
        self.compiler.add_includes(self.private_include_dirs)
        self.compiler.add_public_includes(self.public_include_dirs)
        self.compiler.update_public_defines(["{}={}".format(i, self.public_defines[i]) for i in self.public_defines])
        self.compiler.update_defines(["{}={}".format(i, self.private_defines[i]) for i in self.private_defines])

        waf_context.to_log("At project [{P}] level : Compiler information dump".format(P=self.name))
        waf_context.to_log(self.compiler)
        self.export_compiler(waf_context, self.compiler)

    def configure(self, waf_context):
        for i, v in enumerate(self.public_include_dirs):
            if v.startswith("@include@"):
                self.public_include_dirs[i] = self.public_include_dirs[i].replace("@include@", os.path.join(
                    waf_context.path.abspath(), "include"))
        for i, v in enumerate(self.private_include_dirs):
            if v.startswith("@include@"):
                self.private_include_dirs[i] = self.private_include_dirs[i].replace("@include@", os.path.join(
                    waf_context.path.abspath(), "include"))
        self.__export_compiler(waf_context)

    def setup(self, waf_context):
        pass

    def load_context(self, waf_context):
        # We reload it in order to have some checks in case the developer might
        # have changed its environment since configure.
        mr = NX_ManifestReader.CompilerManifestReader(os.path.join(waf_context.env["config_dir"], "compilers.xml"))
        mr.extend_mappings_from_context(waf_context)
        module_name = waf_context.path.get_bld().abspath()
        solution_build = waf_context.env["solution_bld"]
        module_name = module_name[module_name.find(solution_build) + len(solution_build) + 1:]
        mr.extend_mapping(*self.extends_manifest_mapping(module_name))
        mr.read()
        self.__compiler = mr.manifest
        self.read_compiler_manifest_from_env(waf_context, self.compiler)

    def build(self, waf_context):
        self.load_context(waf_context)
        waf_context(**self.project_properties)

    def package(self, waf_context):
        pass

    def test(self, waf_context):
        pass


class BunchOfObjects(Abstract):
    def __init__(self, name, **kwargs):
        super(BunchOfObjects, self).__init__(name, kwargs.get("version", ""))
        self.__src_dirs = []
        self.__src_files = []
        self.__f_excludes = []
        self.__idl_srcs = []
        self.__cflags = []
        self.__cxxflags = []
        self.__inc_dirs = []
        self.__priv_inc_dirs = []
        self.__defines = {}
        self.__priv_defines = {}
        self.__requires = []
        self.__compiler = None
        self.type = PROJECT_BUNCH_OBJECTS
        self.add_public_include_dirs("@include@", *(kwargs.get("public_includes", [])))
        self.add_private_include_dirs("@include@", *(kwargs.get("includes", [])))
        self.add_public_defines(kwargs.get("public_defines", {}))
        self.add_private_defines(kwargs.get("defines", {}))
        self.add_public_include_dirs(*(kwargs.get("public_includes", [])))
        self.add_public_include_dirs("include")
        self.add_public_defines(kwargs.get("public_defines", {}))
        self.add_private_defines(kwargs.get("defines", {}))
        self.add_needs(*(kwargs.get("use", [])))
        self.add_cflags(*(kwargs.get("cflags", [])))
        self.add_cxxflags(*(kwargs.get("cxxflags", [])))
        self.add_idl_sources(*(kwargs.get("idl", [])))

    @property
    def project_properties(self):
        prop = {"target": self.name}
        if self.requires:
            prop["use"] = self.requires
        for c, k in [(self.compiler.cflags, "cflags"), (self.compiler.cxxflags, "cxxflags"),
                     (self.sources, "source"),
                     (self.compiler.includes, "includes"), (self.compiler.public_includes, "export_includes")]:
            if c:
                prop[k] = c
        prop["features"] = "cxx"
        t = ["{K}={V}".format(K=i, V=self.public_defines[i]) for i in self.public_defines]
        if t:
            prop["export_defines"] = " ".join(t)
        dfn = {i: self.compiler.defines_dict[i] for i in self.compiler.defines_dict}
        dfn.update(self.public_defines)
        if dfn:
            prop["defines"] = " ".join(["{K}={V}".format(K=i, V=dfn[i]) for i in dfn])
        return prop

    @property
    def cflags(self):
        return self.__cflags

    @property
    def cxxflags(self):
        return self.__cxxflags

    @property
    def idl_sources(self):
        return self.__idl_srcs

    def add_idl_sources(self, *files):
        """
        Adds additional idl file to compile before proceeding to c++ compilation step.
        Each file is given with current project considered as root node.
        :param files: idl files to generates
        :type files: klist of string
        """
        for f in files:
            if f not in self.__idl_srcs:
                self.__idl_srcs.append(f)

    @property
    def requires(self):
        return self.__requires

    @property
    def public_include_dirs(self):
        return self.__inc_dirs

    @property
    def private_include_dirs(self):
        return self.__priv_inc_dirs

    @property
    def public_defines(self):
        return self.__defines

    @property
    def private_defines(self):
        return self.__priv_defines

    @property
    def compiler(self):
        """
        Retrieve the compiler manifest read in the file.
        :return: The compiler manifest.
        :rtype: NX_ManifestReader.CompilerManifest
        """
        return self.__compiler

    @property
    def sources(self):
        return self.__src_files

    @property
    def src_dirs(self):
        return self.__src_dirs

    def add_cflags(self, *flags):
        for f in flags:
            if f not in self.__cflags:
                self.__cflags.append(f)

    def add_cxxflags(self, *flags):
        for f in flags:
            if f not in self.__cxxflags:
                self.__cxxflags.append(f)

    def add_needs(self, *req):
        for r in req:
            if r.count(":") != 0:
                r = "".join(r.split(":"))
            if r not in self.__requires:
                self.__requires.append(r)

    def add_public_include_dirs(self, *dirs):
        for d in dirs:
            if d not in self.__inc_dirs:
                self.__inc_dirs.append(d)

    def add_private_include_dirs(self, *dirs):
        for d in dirs:
            if d not in self.__priv_inc_dirs:
                self.__priv_inc_dirs.append(d)

    def add_public_defines(self, defs):
        self.__defines.update(defs)

    def add_private_defines(self, defs):
        self.__priv_defines.update(defs)

    def add_files_to_exclusion_list(self, *files):
        """
        Exclude files from the build.
        :param files: files to exclude
        :type files: klist of string
        """
        for f in files:
            if f not in self.__f_excludes:
                self.__f_excludes.append(f)

    @property
    def exclusion_list(self):
        return self.__f_excludes

    def __export_compiler(self, waf_context):
        mr = NX_ManifestReader.CompilerManifestReader(os.path.join(waf_context.env["config_dir"], "compilers.xml"))
        mr.extend_mappings_from_context(waf_context)
        module_name = waf_context.path.get_bld().abspath()
        module_name = module_name[
                      module_name.find(waf_context.env["solution_bld"]) + len(waf_context.env["solution_bld"]) + 1:]
        mr.extend_mapping(*self.extends_manifest_mapping(module_name))
        mr.read()
        self.__compiler = mr.manifest
        # We need to merge it with local setup
        self.compiler.add_cflags(self.cflags)
        self.compiler.add_cxxflags(self.cxxflags)
        self.compiler.add_includes(self.private_include_dirs)
        self.compiler.add_public_includes(self.public_include_dirs)
        self.compiler.update_public_defines(
            ["{K}={V}".format(K=i, V=self.public_defines[i]) for i in self.public_defines])
        self.compiler.update_defines(
            ["{K}={V}".format(K=i, V=self.private_defines[i]) for i in self.private_defines])
        waf_context.to_log("At project [{P}] level : Compiler informations dump".format(P=self.name))
        waf_context.to_log(self.compiler)
        self.export_compiler(waf_context, self.compiler)

    def add_src_files(self, *files):
        for f in files:
            if f not in self.sources and not f.abspath().endswith(".donotbuild"):
                self.__src_files.append(f)

    def add_src_dirs(self, *dirs):
        for d in dirs:
            if d not in self.__src_dirs:
                self.__src_dirs.append(d)

    def get_absolute_idl_paths(self, waf_context):
        paths = []
        mod = waf_context.path.abspath()
        for f in self.idl_sources:
            final_path = NX_SetupContext.compute_idl_out_path_of(mod, f)
            if final_path not in paths:
                paths.append(final_path)
        return paths

    def set_options(self, waf_context):
        pass

    def configure(self, waf_context):
        for i, v in enumerate(self.public_include_dirs):
            if v.startswith("@include@"):
                self.public_include_dirs[i] = self.public_include_dirs[i].replace("@include@", os.path.join(
                    waf_context.path.abspath(), "include"))
        for i, v in enumerate(self.private_include_dirs):
            if v.startswith("@include@"):
                self.private_include_dirs[i] = self.private_include_dirs[i].replace("@include@", os.path.join(
                    waf_context.path.abspath(), "include"))
        paths = self.get_absolute_idl_paths(waf_context)
        paths = [p[len(waf_context.path.abspath()):].lstrip(os.sep) for p in paths]
        self.add_private_include_dirs(*paths)
        self.__export_compiler(waf_context)

    def setup(self, waf_context):
        waf_context.generate_idl(self.idl_sources)

    def load_context(self, waf_context):
        # We reload it in order to have some checks in case the developer might
        # have change its environment since configure.
        logging.getLogger(LOGGER_PRJ).info("Loading context of project [{P}]".format(P=self.name))
        mr = NX_ManifestReader.CompilerManifestReader(os.path.join(waf_context.env["config_dir"], "compilers.xml"))
        mr.extend_mappings_from_context(waf_context)
        module_name = waf_context.path.get_bld().abspath()
        module_name = module_name[
                      module_name.find(waf_context.env["solution_bld"]) + len(waf_context.env["solution_bld"]) + 1:]
        mr.extend_mapping(*self.extends_manifest_mapping(module_name))
        mr.read()
        self.__compiler = mr.manifest
        self.read_compiler_manifest_from_env(waf_context, self.compiler)
        logging.getLogger(LOGGER_PRJ).info("Processing IDL for project [{P}]".format(P=self.name))
        for i in self.idl_sources:
            # IDL files are expected to be given as relative. So here, we compute the path in which
            # generated files are put. It will end with "idl_build/<idlfile>.out". We then add it back
            # to the src dir and let the magic operate.
            idl = os.path.join(i[:i.find(os.path.basename(i)) - 1], "idl_build", os.path.basename(i) + ".out")
            self.add_src_dirs(idl)
        logging.getLogger(LOGGER_PRJ).info("Computing source files for project [{P}]".format(P=self.name))
        for i in self.src_dirs:
            self.add_src_files(*(waf_context.path.ant_glob(os.path.join(i, "*.c*"))))
        logging.getLogger(LOGGER_PRJ).info(
            "Project [{P}] has {L} identified source files".format(P=self.name, L=len(self.__src_files)))
        logging.getLogger(LOGGER_PRJ).info("Processing exclusion list for project [{P}]".format(P=self.name))
        for f in self.exclusion_list:
            for i in [s for s in self.__src_files if s.abspath().endswith(f)]:
                logging.getLogger(LOGGER_PRJ).info(
                    "Removing [{F}] from source file of project [{P}]".format(P=self.name, F=i))
                self.__src_files.remove(i)
        logging.getLogger(LOGGER_PRJ).info(
            "Project [{P}] has {L} source files to compile".format(P=self.name, L=len(self.__src_files)))
        logging.getLogger(LOGGER_PRJ).info(
            "Final project properties for [{P}] : {D}".format(P=self.name, D=self.project_properties))
        logging.getLogger(LOGGER_PRJ).info("-" * 80)

    def build(self, waf_context):
        self.load_context(waf_context)
        waf_context.objects(**self.project_properties)

    def package(self, waf_context):
        pass

    def test(self, waf_context):
        pass
