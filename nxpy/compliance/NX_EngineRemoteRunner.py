import argparse
import os

import build.NX_BuilderException
import build.NX_TestRunner
import common.NX_Utils
import common.NX_Colors
import common.NX_Process
import compliance.NX_DB


class RemoteTestRunner(build.NX_TestRunner.AbstractTestRunner):
    def __init__(self):
        super(RemoteTestRunner, self).__init__("Remote Tests Runner")
        self.__Process = None
        self.parser = None
        self.args = None
        self.__xml = ""
        self.__ip = ""
        self.__list = False
        self.__verbose = False
        self.__test_list = []
        self.__logdir = ""
        self.__workspace = ""
        self.__win_dbg = False
        self.__gdb = False

    @property
    def win_dbg(self):
        return self.__win_dbg

    @win_dbg.setter
    def win_dbg(self, value):
        self.__win_dbg = value

    @property
    def gdb(self):
        return self.__gdb

    @gdb.setter
    def gdb(self, value):
        self.__gdb = value

    @property
    def log_dir(self):
        return self.__logdir

    @log_dir.setter
    def log_dir(self, value):
        if value:
            if os.path.exists(value):
                self.__logdir = os.path.abspath(value)
            else:
                msg = "Cannot set [{D}] as logdir : directory does not exists!".format(D=value)
                raise build.NX_BuilderException.TestError(self.name, msg)
        else:
            self.__logdir = os.path.join(self.workspace, "playground", "log", self.env.platform_id_ext)

    @property
    def workspace(self):
        return self.__workspace

    @workspace.setter
    def workspace(self, value):
        if not os.path.exists(value):
            msg = "Cannot set [{D}] as workspace : directory does not exists!".format(D=value)
            raise build.NX_BuilderException.TestError(self.name, msg)
        if not os.path.isdir(value):
            msg = "Cannot set [{D}] as workspace : this is not a directory!".format(D=value)
            raise build.NX_BuilderException.TestError(self.name, msg)
        self.__workspace = os.path.abspath(value)

    @property
    def test_list(self):
        return self.__test_list

    @test_list.setter
    def test_list(self, value):
        self.__test_list = common.NX_Utils.extend_unique(self.__test_list, value)

    @property
    def verbose(self):
        return self.__verbose

    @verbose.setter
    def verbose(self, value):
        self.__verbose = value

    @property
    def xml_output_path(self):
        return self.__xml

    @xml_output_path.setter
    def xml_output_path(self, value):
        if os.path.exists(value):
            msg = "Cannot use [{F}] as xml output file : file exists!".format(F=value)
            raise build.NX_BuilderException.TestError(self.name, msg)
        self.__xml = value

    @property
    def list(self):
        return self.__list

    @list.setter
    def list(self, value):
        self.__list = value

    @property
    def ip(self):
        return self.__ip

    @ip.setter
    def ip(self, value):
        self.__ip = value

    @property
    def program(self):
        return "remote"

    @property
    def version(self):
        return "1.0"

    def set_options(self, waf_context=None):
        if waf_context is None:
            self.parser = argparse.ArgumentParser("Remote Tests Runner")
            with self.parser.add_argument as fun:
                fun("--xml", action="store", default=self.xml_output_path,
                    help="This option force outputing results in a XML file.")
                fun("--tests", nargs="+",
                    help="If this option is set, only run the provided tests, otherwise all tests are run.")
                fun("--ip", action="store", default=self.ip,
                    help="IP address of the remote server using format IPV4:SERVICE.")
                fun("--logdir", action="store", default=self.log_dir,
                    help="Set the log directory at the specified path.")
                fun("--list", action="store_true", default=self.list,
                    help="List all tests available and make the script returns.")
                fun("--workspace", action="store", default=self.workspace,
                    help="Allow you to override the workspace (root dir) of the source.")
                fun("--dbg", action="store_true", default=self.win_dbg,
                    help="Attach a debugger just after the process has started (Windows only).")
                fun("--gdb", action="store_true", default=self.gdb,
                    help="Run executable with GDB.")
                fun("-v", "--verbose", action="store_true", default=self.verbose,
                    help="Print additional information on stdout, usefull for debugging.")
                fun("--version", action="version",
                    version="%(prog) version {V}".format(V=self.version), help="Print version information.")
            args = self.parser.parse_args()
            self.test_list = args.tests
            self.log_dir = args.logdir
            self.workspace = args.workspace
            self.win_dbg = args.dbg
            self.gdb = args.gdb
            self.verbose = args.verbose
            self.list = args.list
            self.ip = args.ip
            self.xml_output_path = args.xml
        else:
            grp = waf_context.add_option_group("{N} options".format(N=self.name))
            fun = grp.add_option
            fun("--xml", action="store", default=self.xml_output_path,
                help="This option force outputing results in a XML file.")
            fun("--tests", nargs="+",
                help="If this option is set, only run the provided tests, otherwise all tests are run.")
            fun("--ip", action="store", default=self.ip,
                help="IP address of the remote server using format IPV4:SERVICE.")
            fun("--logdir", action="store", default=self.log_dir,
                help="Set the log directory at the specified path.")
            fun("--list", action="store_true", default=self.list,
                help="List all tests available and make the script returns.")
            fun("--workspace", action="store", default=self.workspace,
                help="Allow you to override the workspace (root dir) of the source.")
            fun("--dbg", action="store_true", default=self.win_dbg,
                help="Attach a debugger just after the process has started (Windows only).")
            fun("--gdb", action="store_true", default=self.gdb,
                help="Run executable with GDB.")
            fun("-v", "--verbose", action="store_true", default=self.verbose,
                help="Print additional information on stdout, usefull for debugging.")

    def configure(self, waf_context):
        waf_context.env.tests = waf_context.options.tests
        waf_context.env.logdir = waf_context.options.logdir
        waf_context.env.workspace = waf_context.options.workspace
        waf_context.env.dbg = waf_context.options.dbg
        waf_context.env.gdb = waf_context.options.gdb
        waf_context.env.verbose = waf_context.options.verbose
        waf_context.env.list = waf_context.options.list
        waf_context.env.ip = waf_context.options.ip
        waf_context.env.xml = waf_context.options.xml
        self.__read_option_from_context(waf_context)

    def __read_option_from_context(self, waf_context):
        self.test_list = waf_context.env.tests
        self.workspace = waf_context.env.workspace
        self.log_dir = waf_context.env.logdir
        self.win_dbg = waf_context.env.dbg
        self.gdb = waf_context.env.gdb
        self.verbose = waf_context.env.verbose
        self.list = waf_context.env.list
        self.ip = waf_context.env.ip
        self.xml_output_path = waf_context.env.xml

    def setup(self, waf_context=None):
        if waf_context is not None:
            self.__read_option_from_context(waf_context)
        db_env_info = compliance.NX_DB.DBSetup()
        db_env_info.setup()
        # Log directory customization
        if self.log_dir:
            os.environ["LOGDIR"] = self.log_dir
        else:
            os.environ["LOGDIR"] = os.path.join(os.getcwd(), "build", self.env.platform_id_ext)
        if self.workspace:
            self.env.workspace = self.workspace
        # Adding playground lib dir to loader search path.
        if self.env.os_name in ["linux", "sol", "aix"]:
            lib_compiler_path = os.path.join(self.env.tools_root_dir, "compilers", self.env.platform,
                                             self.env.compiler_type, self.env.compiler_version,
                                             "lib{B}".format(B=self.env.build_bits))
            self.env.extend_lib_loader_path([self.env.playground_lib_dir, lib_compiler_path], True)
        else:
            self.env.extend_lib_loader_path([self.env.playground_lib_dir], True)
        if self.win_dbg:
            os.environ["DEBUGBREAK"] = "1"
        # Adjusting path of program
        if self.gdb:
            self.__Process = common.NX_Process.Process("gdb")
            self.__Process.program_path = os.path.join(self.env.tools_root_dir, "utils", "gdb", "64bits", "last", "bin")
        else:
            self.__Process = common.NX_Process.Process(self.program)
            self.__Process.program_path = self.env.playground_bin_dir
        self.__Process.cwd = os.path.join(self.env.workspace, "RemoteTests")
        # Cannot have a portable configuration without that...
        os.environ["DLL_PFX"] = "" if self.env.os_name == "Win64" else "lib"
        os.environ["DLL_EXT"] = ".dll" if self.env.os_name == "Win64" else ".so"
        os.environ["NOMEMOPTIMIZE"] = "1"
        os.environ["NX_BUILD_PLATFORM_EXT"] = self.env.platform_id_ext
        # Properties...
        file_path = os.path.join(self.env.workspace, "RemoteTests", "etc", "env.properties")
        try:
            with open(file_path, "r") as f:
                for i in f:
                    line = i.strip().split("=")
                    os.environ[line[0]] = line[1]
        except Exception:
            raise Exception("Cannot find/open file '{F}', please check your environment!".format(F=file_path))
        self.print_infos()

    def print_infos(self):
        """
        Dump basic information about the incoming run.
        """
        print """
{SEP}
      Workspace set to [{WSDIR}]
      Build [{BUILD}] on platform [{PLATFORM}]
      Logdir positioned at [{LOGDIR}]
      Playground lib directory located under [{LIBDIR}]
{SEP}
      {Key} has been set to {Value}
{SEP}
""".format(WSDIR=common.NX_Colors.colored_text("GREEN", self.env.workspace),
           BUILD=common.NX_Colors.colored_text("GREEN", self.env.build_mode),
           PLATFORM=common.NX_Colors.colored_text("GREEN", self.env.platform_id_ext),
           LOGDIR=common.NX_Colors.colored_text("GREEN", os.environ["LOGDIR"]),
           LIBDIR=common.NX_Colors.colored_text("GREEN", self.env.playground_lib_dir),
           SEP="-" * 80,
           Key=self.env.get_lib_loader_env_key(),
           Value=os.getenv(self.env.get_lib_loader_env_key())
           )

    def __run_program(self, **kwargs):
        if self.verbose:
            print "-" * 80
            print "Running command '{CMD} {ARGS}'.".format(CMD=self.__Process.full_path,
                                                           ARGS=" ".join(self.__Process.args))
        if kwargs.get("active_polling", True):
            self.__Process.run_and_poll(**kwargs)
        else:
            self.__Process.run(**kwargs)
        if self.verbose:
            print "Execution complete, rc [%d]." % self.__Process.last_return_code

    def __extract_test_list(self):
        if self.verbose:
            print "Extracting list of tests..."
        self.__Process.args = ["-l"]
        self.__run_program(out_prefix="", err_prefix="STDERR: ", active_polling=False, keep_stdout=False,
                           keep_stderr=False)
        test_list = []
        if 0 == self.__Process.last_return_code:
            with open("{PP}.list.txt".format(PP=self.__Process.full_path), "r") as f:
                for i in f:
                    tmp = i.split(":")[0]
                    if tmp not in test_list:
                        test_list.append(tmp)
        return test_list

    def execute(self):
        original_workspace = self.env.workspace
        if self.list:
            self.__Process.args = ["-l"]
            self.__run_program(out_prefix="", err_prefix="STDERR: ", active_polling=True, keep_stdout=False,
                               keep_stderr=False)
            self.return_code = self.__Process.last_return_code
        else:
            cmd = []
            if self.gdb:
                cmd.extend([os.path.join(self.env.playground_bin_dir, self.program)])
            if self.xml_output_path:
                cmd.extend(["-o", self.xml_output_path])
            if self.ip:
                cmd.extend(["-i", self.ip])
            if self.test_list:
                cmd.extend(["-d", ",".join(self.test_list)])
            cmd.extend([self.env.build_mode, self.env.os_name, self.env.workspace])
            if self.env.platform == "Win64":
                self.env.workspace = self.env.uworkspace
            self.__Process.args = cmd
            self.__run_program(out_prefix="", err_prefix="STDERR : ", active_polling=True)
            self.return_code = self.__Process.last_return_code
            if 0 != self.return_code:
                raise build.NX_BuilderException.TestError(self.name, "At least one unit test failed.")
        if self.env.platform == "Win64":
            self.env.workspace = original_workspace
        return self.return_code

    def post_process(self):
        pass
