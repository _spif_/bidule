class VersionError(Exception):
    def __init__(self, msg):
        super(VersionError, self).__init__(msg)


class PMVersion(object):
    def __init__(self, version):
        self.__version = version
        self.__version_comp = version.split(".")
        self.__flag = self.__compute_version_flag()
        last = self.__version_comp[-1]
        self.__version_flag_info = ""
        if last.find("-") >= 0:
            tmp = last.split("-")
            self.__version_comp[-1] = tmp[0]
            self.__version_flag_info = tmp[1]

    def __compute_version_flag(self):
        if self.full_version.find("-bld") >= 0:
            return "build"
        elif self.full_version.find("-snapshot") >= 0:
            return "snapshot"
        else:
            return "version"

    @property
    def full_version(self):
        return self.__version

    @property
    def version(self):
        return ".".join(self.__version_comp[:2])

    @property
    def version_ext(self):
        return ".".join(self.__version_comp[:3])

    @property
    def version_flag_info(self):
        return self.__version_flag_info

    @property
    def version_flag(self):
        return self.__flag

    @property
    def major(self):
        if len(self.__version_comp) < 2:
            raise VersionError("Invalid version string!")
        return self.__version_comp[0]

    @property
    def minor(self):
        if len(self.__version_comp) < 2:
            raise VersionError("Invalid version string!")
        return self.__version_comp[1]

    @property
    def revision(self):
        if len(self.__version_comp) < 3:
            raise VersionError("Invalid version string!")
        return self.__version_comp[2]

    @property
    def hotfix(self):
        if len(self.__version_comp) >= 4:
            return self.__version_comp[3]
        return "0"
