import os
from common.NX_Colors import warning as cmn_color_warn
import common.NX_Env


class DBSetup:
    def __init__(self):
        self.__env = common.NX_Env.Env()
        self.__syb15_win = "C:\\sybase\\OCS-15_0\\dll;C:\\sybase\\OCS-15_0\\lib;C:\\sybase\\OCS-15_0\\lib3p"
        self.__syb_win = "C:\\sybase12.5\\OCS-12_5\\dll;C:\\sybase12.5\\OCS-12_5\\lib;C:\\sybase12.5\\OCS-12_5\\lib3p"
        self.__oracle_win = os.path.join("C:\\", "oracle", "product", "10.2.0", "client_1")

    @property
    def sybase_15_win_default_path(self):
        return self.__syb15_win

    @property
    def sybase_win_default_path(self):
        return self.__syb_win

    @property
    def oracle_win_default_path(self):
        return self.__oracle_win

    @property
    def env(self):
        return self.__env

    def __win_64_setup(self):
        if "ORACLE_CLIENT_PATH" in os.environ:
            ora_cli_path = os.environ["ORACLE_CLIENT_PATH"]
            if not os.path.exists(os.path.join(ora_cli_path, "OCI.dll")):
                cmn_color_warn("Cannot find OCI.dll in [{}]! Trying default path...".format(ora_cli_path))
                if not os.path.exists(self.oracle_win_default_path):
                    cmn_color_warn("Cannot find OCI.dll in [{}]!".format(self.oracle_win_default_path))
                    raise Exception("Invalid ORACLE_CLIENT_PATH environment variable set!")
                else:
                    os.environ["ORACLE_CLIENT_PATH"] = self.oracle_win_default_path
        else:
            if not os.path.exists(os.path.join(self.oracle_win_default_path, "oci.dll")):
                cmn_color_warn("Cannot find OCI.dll in [{}]!".format(self.oracle_win_default_path))
                raise Exception("Invalid ORACLE_CLIENT_PATH environment variable set!")
            else:
                os.environ["ORACLE_CLIENT_PATH"] = self.oracle_win_default_path

        if "SYBASE15_CLIENT_PATH" in os.environ:
            found = False
            syb_cli_path = os.environ["SYBASE15_CLIENT_PATH"]
            for p in syb_cli_path.split(os.pathsep):
                found = found or os.path.exists(os.path.join(p, "LIBSYBCS.dll"))
            if not found:
                cmn_color_warn("Cannot find LIBSYBCS.dll in [{}]! Trying default path...".format(syb_cli_path))
                for p in self.sybase_15_win_default_path.split(os.pathsep):
                    found = found or os.path.exists(os.path.join(p, "LIBSYBCS.dll"))
                if not found:
                    cmn_color_warn("Cannot find LIBSYBCS.dll in [{}]!".format(self.sybase_15_win_default_path))
                    raise Exception("Invalid SYBASE15_CLIENT_PATH environment variable set!")
                else:
                    os.environ["SYBASE15_CLIENT_PATH"] = self.sybase_15_win_default_path
        else:
            os.environ["SYBASE15_CLIENT_PATH"] = self.sybase_15_win_default_path

        if os.environ.has_key("SYBASE_CLIENT_PATH"):
            found = False
            syb_cli_path = os.environ["SYBASE_CLIENT_PATH"]
            for p in syb_cli_path.split(os.pathsep):
                found = found or os.path.exists(os.path.join(p, "LIBCS.dll"))
            if not found:
                cmn_color_warn("Cannot find LIBCS.dll in [{}]! Trying default path...".format(syb_cli_path))
                for p in self.sybase_win_default_path.split(os.pathsep):
                    found = found or os.path.exists(os.path.join(p, "LIBCS.dll"))
                if not found:
                    cmn_color_warn("Cannot find LIBCS.dll in [{}]!".format(self.sybase_win_default_path))
                    raise Exception("Invalid SYBASE_CLIENT_PATH environment variable set!")
                else:
                    os.environ["SYBASE_CLIENT_PATH"] = self.sybase_win_default_path
        else:
            os.environ["SYBASE_CLIENT_PATH"] = self.sybase_win_default_path

    def setup(self):
        if self.env.os_name == "Win64":
            self.__win_64_setup()
            env_values = [os.environ[i] for i in ["ORACLE_CLIENT_PATH", "SYBASE15_CLIENT_PATH", "SYBASE_CLIENT_PATH"]]
            self.env.extend_lib_loader_path(env_values)
