import os
import common.NX_Colors
import common.NX_Env
import common.NX_Process


class Sonar(object):
    def __init__(self):
        self.__env = common.NX_Env.Env()
        self._rc = 0
        self.__java_home = ""
        self.__sonar_version = "last"
        self.__project_name = ""
        self.__branch_name = ""
        self.__verbose = False
        self.__push_only = False

    @property
    def push_only(self):
        return self.__push_only

    @push_only.setter
    def push_only(self, value):
        self.__push_only = value

    @property
    def verbose(self):
        return self.__verbose

    @verbose.setter
    def verbose(self, value):
        self.__verbose = value

    @property
    def branch_name(self):
        return self.__branch_name

    @property
    def project_name(self):
        return self.__project_name

    @branch_name.setter
    def branch_name(self, name):
        self.__branch_name = name

    @project_name.setter
    def project_name(self, name):
        self.__project_name = name

    @property
    def sonar_version(self):
        return self.__sonar_version

    @sonar_version.setter
    def sonar_version(self, version):
        self.__sonar_version = version

    @property
    def java_home(self):
        return self.__java_home

    @java_home.setter
    def java_home(self, path):
        self.__java_home = path

    @property
    def return_code(self):
        return self._rc

    @property
    def env(self):
        return self.__env

    def do_sonar(self):
        if self.verbose:
            common.NX_Colors.print_info("  - Checking information ...")
        if not self.env.valid():
            raise Exception("NX Environment is not valid.")
        if self.java_home and not os.path.exists(self.java_home):
            raise Exception("Invalid JAVA_HOME directory provided.")
        if self.java_home:
            self.env.extend_environment("JAVA_HOME", [self.java_home], True)
        if not self.branch_name and not self.project_name:
            raise Exception("Cannot perform a Sonar analysis without branch name and project name!")
        if self.verbose:
            common.NX_Colors.print_info("  -> Everything fine!")


class ComplianceEngine(Sonar):
    def __init__(self):
        super(ComplianceEngine, self).__init__()
        self.__cppchk_output = ""
        self.__cppchk_version = ""

    @property
    def cpp_check_output_file(self):
        return self.__cppchk_output

    @cpp_check_output_file.setter
    def cpp_check_output_file(self, output):
        self.__cppchk_output = output

    @property
    def cpp_check_version(self):
        return self.__cppchk_version

    @cpp_check_version.setter
    def cpp_check_version(self, version):
        self.__cppchk_version = version

    def __run_cpp_check(self):
        if not self.push_only:
            self.env.extend_lib_loader_path(
                [os.path.join(self.env.tools_root_dir, "utils", "cppcheck", self.cpp_check_version, "lib")], True)
            chkr = common.NX_Process.Process("cppcheck")
            chkr.program_path = os.path.join(self.env.tools_root_dir, "utils", "cppcheck",
                                             self.cpp_check_version, "bin")
            chkr.args = [".", "--enable=all", "--xml", "--xml-version=1"]
            chkr.cwd = self.env.workspace
            if self.verbose:
                common.NX_Colors.print_info("    - Launching cppcheck from [%s]" % chkr.full_path)
            chkr.run_and_poll(keep_stdout=False, keep_stderr=False, stderr_file_path=self.cpp_check_output_file,
                              err_prefix=None, out_prefix=None)
            return chkr.last_return_code
        return 0

    def __run_sonar(self):
        my_sonar = common.NX_Process.Process("sonar-runner")
        my_sonar.program_path = os.path.join(self.env.tools_root_dir, "utils", "sonar", self.sonar_version, "bin")
        my_sonar.cwd = self.env.workspace
        my_sonar.run_and_poll(keep_stderr=False, keep_stdout=False, err_prefix="SONAR ERROR: ", out_prefix="SONAR: ")
        return my_sonar.last_return_code

    def do_sonar(self):
        super(ComplianceEngine, self).do_sonar()
        if self.verbose:
            common.NX_Colors.print_info("Starting pre-Sonar analysis ...")
            common.NX_Colors.print_info("  - Trying to launch cppcheck analysis.")
        if self.cpp_check_output_file:
            rc = self.__run_cpp_check()
            color = "GREEN:BOLD"
            if 0 != rc:
                self._rc = 1
                color = "RED:BOLD"
            if self.verbose:
                print "%s%s" % (common.NX_Colors.colored_text("BOLD", "  - CPPCheck analysis ended with rc "),
                                common.NX_Colors.colored_text(color, "[%d]" % rc))
        else:
            common.NX_Colors.print_warning("  - No output file given for CPPCheck. Skipping this step.")
        if self.verbose:
            common.NX_Colors.print_info("Trying to run Sonar.")
        if 0 != self.__run_sonar():
            self._rc += 100
