import os
import argparse
import common.NX_ArtifactoryDialect
import common.NX_RestClient
import compliance.NX_PMVersion
import common.NX_Colors

ARTIFACT_REPOSITORY = "compliance-deployer:compliance123@http://nx-artifacts:8085/artifactory"

BAD_FILE = 10
RELEASE_PROMOTE_ERR = 20
RELEASE_PROMOTE_BAD_VERSION = 21
RELEASE_RM_BAD_VERSION = 22
BUILD_PUSH_ERR = 40
BUILD_PROMOTE_BAD_EXTENSION = 41
BUILD_PROPERTIES_ERR = 42
BUILD_RM_BAD_VERSION = 43
PROPERTY_ERROR_INTERNAL = 60
SNAPSHOT_PUSH_ERR = 80
SNAPSHOT_RM_ERR = 81
SNAPSHOT_PROPERTIES_ERR = 82
SNAPSHOT_RM_BAD_VERSION = 83


class PMError(Exception):
    def __init__(self, msg, rc):
        super(PMError, self).__init__(msg)
        self.__rc = rc

    @property
    def rc(self):
        return self.__rc


class PMVersionManager:
    def __init__(self):
        self.__args = None
        self.__rest_client = None
        self.__properties = {}

    def set_url_info(self):
        try:
            self.__rest_client = common.NX_ArtifactoryDialect.ArtifactoryInterpret(settings=self.args.settings)
            self.__rest_client.translator = common.NX_ArtifactoryDialect.JSonTranslator()
            self.__rest_client.group_id = self.args.group_id
        except common.NX_RestClient:
            raise

    @property
    def properties(self):
        return self.__properties

    @property
    def rest_client(self):
        return self.__rest_client

    @property
    def args(self):
        if self.__args is None:
            raise PMError("Internal Error : a call to 'set_options' must be operated first.", 10)
        return self.__args

    def __get_all_properties(self):
        try:
            my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
            repo_path = "/".join([my_version.version, "build", my_version.full_version, self.args.artifact])
            auto = self.rest_client.get_item_properties(repo_path, all=True)
            if auto.ok:
                props = []
                for p in auto.content["properties"]:
                    props.append(p)
                return props
            else:
                raise PMError("Internal Error : unable to collect all existing properties on {}".format(repo_path),
                              PROPERTY_ERROR_INTERNAL)
        except common.NX_RestClient.RestException as rest_error:
                msg = "Cannot collect all artifact's properties: {}".format(rest_error.message)
                raise PMError(msg, PROPERTY_ERROR_INTERNAL)

    def __do_property(self):
        if self.args.operation == "add":
            self.__do_property_add()
        elif self.args.operation == "rm":
            self.__do_property_rm()
        elif self.args.operation == "set":
            property_keys = self.__get_all_properties()
            self.__properties, property_keys = {i: i for i in property_keys}, self.__properties
            self.__do_property_rm()
            self.__properties, property_keys = property_keys, self.__properties
            self.__do_property_add()
        else:
            print common.NX_Colors.warning("Unknown operation!")

    def __do_property_rm(self):
        my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
        repo_path = "/".join([my_version.version, "build", my_version.full_version, self.args.artifact])
        try:
            if not self.rest_client.delete_item_properties(repo_path, self.properties.keys()):
                msg = "Cannot remove property on {} : check that they are valid"
                print common.NX_Colors.warning(msg.format(self.args.artifact))
        except common.NX_RestClient.RestException as rest_error:
            msg = "Cannot update artifact's properties: {}".format(rest_error.message)
            raise PMError(msg, BUILD_PROPERTIES_ERR)

    def __do_property_add(self):
        if self.properties:
            my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
            repo_path = "/".join([my_version.version, "build", my_version.full_version, self.args.artifact])
            try:
                rc, msg = self.rest_client.set_item_properties(repo_path, self.properties)
                if not rc:
                    print common.NX_Colors.warning("Cannot add property on {} : {}".format(self.args.artifact, msg))
            except common.NX_RestClient.RestException as rest_error:
                msg = "Cannot update artifact's properties: {}".format(rest_error.message)
                raise PMError(msg, BUILD_PROPERTIES_ERR)
        else:
            print common.NX_Colors.warning("Nothing to add")

    def __do_snapshot(self):
        if self.args.operation == "push":
            self.__do_snapshot_push()
        if self.args.operation == "rm":
            self.__do_snapshot_rm()

    def __do_snapshot_rm(self):
        my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
        if my_version.version_flag != "snapshot":
            raise PMError("Cannot remove a non snapshot artifact. Review your version.", SNAPSHOT_RM_BAD_VERSION)
        repo_file = self.args.artifact
        repo_path = "/".join([my_version.version, "snapshot", my_version.full_version, repo_file])
        try:
            self.rest_client.delete_artifact(repo_path)
        except common.NX_RestClient.RestException as rest_error:
            msg = "Cannot delete artifact {} : {}".format(self.args.artifact, rest_error.message)
            raise PMError(msg, SNAPSHOT_PUSH_ERR)
        else:
            repo_path = "/".join([my_version.version, "snapshot", my_version.full_version])
            children = self.rest_client.get_item_children(repo_path, ["uri", "children"])
            if len(children) == 0:
                try:
                    self.rest_client.delete_artifact(repo_path)
                except common.NX_RestClient.RestException as rest_error:
                    msg = "Cannot delete folder {} : {}".format(repo_path, rest_error.message)
                    raise PMError(msg, SNAPSHOT_PUSH_ERR)

    def __do_snapshot_push(self):
        my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
        ext = ".zip" if self.args.file.endswith(".zip") else "unknown"
        if ext == "unknown":
            ext = ".tar.gz" if self.args.file.endswith(".tar.gz") else "unknown"
        if ext == "unknown":
            ext = ".tar.bz2" if self.args.file.endswith(".tar.bz2") else "unknown"
        if ext == "unknown":
            msg = "Invalid artifact extension. Supported extensions are zip, tar.bz2 and tar.gz"
            raise PMError(msg, BUILD_PROMOTE_BAD_EXTENSION)
        repo_file = self.args.artifact + ext
        repo_path = "/".join([my_version.version, "snapshot", my_version.full_version, repo_file])
        try:
            self.rest_client.deploy_artifact(self.args.file, self.args.group_id, repo_path)
        except common.NX_RestClient.RestException as rest_error:
            msg = "Cannot push artifact {} : {}".format(self.args.artifact, rest_error.message)
            raise PMError(msg, SNAPSHOT_PUSH_ERR)
        if self.properties:
            try:
                self.rest_client.set_item_properties(repo_path, self.properties)
            except common.NX_RestClient.RestException as rest_error:
                msg = "Cannot update artifact's properties: {}".format(rest_error.message)
                raise PMError(msg, SNAPSHOT_PROPERTIES_ERR)

    def __do_build(self):
        if self.args.operation == "push":
            self.__do_build_push()
        if self.args.operation == "rm":
            self.__do_build_rm()

    def __do_build_rm(self):
        my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
        if my_version.version_flag != "build":
            raise PMError("Cannot remove a non build artifact. Review your version", BUILD_RM_BAD_VERSION)
        repo_file = self.args.artifact
        repo_path = "/".join([my_version.version, "build", my_version.full_version, repo_file])
        try:
            self.rest_client.delete_artifact(repo_path)
        except common.NX_RestClient.RestException as rest_error:
            msg = "Cannot delete artifact {} : {}".format(self.args.artifact, rest_error.message)
            raise PMError(msg, BUILD_PUSH_ERR)
        else:
            repo_path = "/".join([my_version.version, "build", my_version.full_version])
            children = self.rest_client.get_item_children(repo_path, ["uri", "children"])
            if len(children) == 0:
                try:
                    self.rest_client.delete_artifact(repo_path)
                except common.NX_RestClient.RestException as rest_error:
                    msg = "Cannot delete folder {} : {}".format(repo_path, rest_error.message)
                    raise PMError(msg, BUILD_PUSH_ERR)

    def __do_build_push(self):
        my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
        ext = ".zip" if self.args.file.endswith(".zip") else "unknown"
        if ext == "unknown":
            ext = ".tar.gz" if self.args.file.endswith(".tar.gz") else "unknown"
        if ext == "unknown":
            ext = ".tar.bz2" if self.args.file.endswith(".tar.bz2") else "unknown"
        if ext == "unknown":
            msg = "Invalid artifact extension. Supported extensions are zip, tar.bz2 and tar.gz"
            raise PMError(msg, BUILD_PROMOTE_BAD_EXTENSION)
        repo_file = self.args.artifact + ext
        repo_path = "/".join([my_version.version, "build", my_version.full_version, repo_file])
        try:
            self.rest_client.deploy_artifact(self.args.file, self.args.group_id, repo_path)
        except common.NX_RestClient.RestException as rest_error:
            msg = "Cannot push artifact {} : {}".format(self.args.artifact, rest_error.message)
            raise PMError(msg, BUILD_PUSH_ERR)
        if self.properties:
            try:
                self.rest_client.set_item_properties(repo_path, self.properties)
            except common.NX_RestClient.RestException as rest_error:
                msg = "Cannot update artifact's properties: {}".format(rest_error.message)
                raise PMError(msg, BUILD_PROPERTIES_ERR)

    def __do_release(self):
        if self.args.operation == "promote":
            self.__do_release_promote()
        elif self.args.operation == "deprecate":
            self.__do_release_deprecate()
        elif self.args.operation == "rm":
            self.__do_release_rm()
        elif self.args.operation == "maintain":
            self.__do_release_maintain()

    def __do_release_maintain(self):
        my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
        repo_path = "/".join([my_version.version, "version", my_version.full_version])
        try:
            prop = {"is_version": "1", "deprecate": "0"}
            rc, msg = self.rest_client.set_item_properties(repo_path, prop, False)
            if not rc:
                print common.NX_Colors.warning("Cannot add property on {} : {}".format(self.args.artifact, msg))
        except common.NX_RestClient.RestException as rest_error:
            msg = "Cannot update artifact's properties: {}".format(rest_error.message)
            raise PMError(msg, BUILD_PROPERTIES_ERR)

    def __do_release_deprecate(self):
        my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
        repo_path = "/".join([my_version.version, "version", my_version.full_version])
        try:
            prop = {"is_version": "1", "deprecate": "1"}
            rc, msg = self.rest_client.set_item_properties(repo_path, prop, False)
            if not rc:
                print common.NX_Colors.warning("Cannot add property on {} : {}".format(self.args.artifact, msg))
        except common.NX_RestClient.RestException as rest_error:
            msg = "Cannot update artifact's properties: {}".format(rest_error.message)
            raise PMError(msg, BUILD_PROPERTIES_ERR)

    def __do_release_rm(self):
        my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
        if my_version.version_flag != "version":
            raise PMError("Cannot remove a non version artifact. Review your version.", RELEASE_RM_BAD_VERSION)
        repo_file = self.args.artifact
        repo_path = "/".join([my_version.version, "version", my_version.full_version, repo_file])
        try:
            self.rest_client.delete_artifact(repo_path)
        except common.NX_RestClient.RestException as rest_error:
            msg = "Cannot delete artifact {} : {}".format(self.args.artifact, rest_error.message)
            raise PMError(msg, BUILD_PUSH_ERR)
        else:
            repo_path = "/".join([my_version.version, "version", my_version.full_version])
            children = self.rest_client.get_item_children(repo_path, ["uri", "children"])
            if len(children) == 0:
                try:
                    self.rest_client.delete_artifact(repo_path)
                except common.NX_RestClient.RestException as rest_error:
                    msg = "Cannot delete folder {} : {}".format(repo_path, rest_error.message)
                    raise PMError(msg, BUILD_PUSH_ERR)

    def __do_release_promote(self):
        try:
            args = {"src_repo": self.args.group_id, "tgt_repo": self.args.group_id}
            my_version = compliance.NX_PMVersion.PMVersion(self.args.version)
            if my_version.version_flag != "build":
                raise PMError("Only build artifact can be promoted as release. Review your version.",
                              RELEASE_PROMOTE_BAD_VERSION)
            src_file = "/".join([my_version.version, my_version.version_flag, my_version.full_version])
            definite_version = "{}.{}".format(my_version.version_ext, my_version.hotfix)
            tgt_file = "/".join([my_version.version, "version", definite_version])
            args["src_file_path"] = src_file
            args["tgt_file_path"] = tgt_file
            self.rest_client.copy_artifact(**args)
        except common.NX_RestClient.RestException as rest_error:
            raise PMError("Cannot promote artifact {} : {}".format(self.args.artifact, rest_error), RELEASE_PROMOTE_ERR)

    def set_options(self):
        parser = argparse.ArgumentParser("version manager", version="%(prog)s, version 1.0")
        parser.description = """This program will help you managing the different version, using an artifact approach.
It provides the ability to push, remove, overwrite an artifact using the NX-PM version schema.
"""
        parser.add_argument("--settings", "-s", dest="settings", required=True,
                            help="Path to the settings to use for communicating/authenticating with Artifactory.")

        sub_commands = parser.add_subparsers(title="Commands", dest="command")

        release = sub_commands.add_parser("release")
        release.description = """This commands aims at managing all 'official' release aspects. It allows you 
to publish a new version, to deprecate an existing version or to remove a version. Each operation performed by this
command cannot be undone."""
        fun = release.add_argument
        fun("operation", choices=["promote", "deprecate", "rm", "maintain"],
            help="Operation to perform on the given artifact/version.")
        fun("--version", action="store", required=True, dest="version",
            help="Version Id on which the operation will operate.")
        fun("--group-id", action="store", required=True, dest="group_id",
            help="Version Id on which the operation will operate.")
        fun("--artifact", action="store", required=True, dest="artifact",
            help="Name of the artifact the operation will work on.")

        build = sub_commands.add_parser("build")
        fun = build.add_argument
        fun("operation", choices=["push", "rm"],
            help="Operation to perform on the given artifact/version.")
        fun("--version", action="store", required=True,
            help="Version Id on which the operation will operate.")
        fun("--group-id", action="store", required=True,
            help="Version Id on which the operation will operate.")
        fun("--artifact", action="store",
            help="Name of the artifact once pushed.")
        fun("--file", action="store",
            help="Name of the artifact. Under rm operation, specify the whole path within group-id.")
        fun("--property", action="append", default=None, nargs="*",
            help="""Properties to set on an artifact, using <KEY>:<VALUE> format. You can accumulate different values
on a key by specifying multiple entries using the same key. By default, no properties are set.""")

        snapshot = sub_commands.add_parser("snapshot")
        fun = snapshot.add_argument
        fun("operation", choices=["push", "rm"],
            help="Operation to perform on the given artifact/version.")
        fun("--version", action="store", required=True,
            help="Version Id on which the operation will operate.")
        fun("--group-id", action="store", required=True,
            help="Version Id on which the operation will operate.")
        fun("--artifact", action="store",
            help="Name of the artifact once pushed.")
        fun("--file", action="store",
            help="Name of the artifact on local fs. Under rm operation, specify the whole path within group-id.")
        fun("--property", action="append", default=None, nargs="*",
            help="""Properties to set on an artifact, using <KEY>:<VALUE> format. You can accumulate different values
        on a key by specifying multiple entries using the same key. By default, no properties are set.""")

        properties = sub_commands.add_parser("property")
        fun = properties.add_argument
        fun("operation", choices=["add", "rm", "set"],
            help="""Add, remove or override existing properties.
The add operation overwrite all values for existing keys. The set operation deletes all existing properties and 
replace them by the provided one.""")
        fun("--version", action="store", required=True,
            help="Version Id on which the operation will operate.")
        fun("--group-id", action="store", required=True,
            help="Version Id on which the operation will operate.")
        fun("--artifact", action="store",
            help="Full name of the artifact once pushed.")
        fun("--property", action="append", default=None, nargs="*",
            help="""Properties to set on an artifact, using <KEY>:<VALUE> format. You can accumulate different values
on a key by specifying multiple entries using the same key. By default, no properties are set.""")

        self.__args = parser.parse_args()

    def setup(self):
        if self.args.command in ["release", "build", "property", "snapshot"]:
            self.set_url_info()
        if self.args.command in ["snapshot", "build"] and self.args.operation == 'push':
            if not os.path.exists(self.args.file):
                raise PMError("Invalid file: to be pushed, the file value must point to a valid file!", BAD_FILE)
        if self.args.command not in ["release"]:
            if self.args.property:
                for cmd_line_entry in self.args.property:
                    for entry in cmd_line_entry:
                        idx = entry.find(":")
                        if idx == -1:
                            msg = "Invalid property : {} does not fulfill <KEY>:<VALUE> format.".format(entry)
                            raise PMError(msg, BUILD_PROPERTIES_ERR)
                        key = entry[:idx]
                        value = entry[idx+1:]
                        if key in self.properties:
                            self.properties[key].append(value)
                        else:
                            self.properties[key] = [value]

    def process(self):
        if self.args.command == "release":
            self.__do_release()
        elif self.args.command == "build":
            self.__do_build()
        elif self.args.command == "property":
            self.__do_property()
        elif self.args.command == "snapshot":
            self.__do_snapshot()
