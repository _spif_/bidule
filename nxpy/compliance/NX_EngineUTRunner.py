import os
import copy
import argparse
import glob
import re

import common.NX_Process
import common.NX_Utils
import common.NX_Colors

import compliance.NX_ReportConverter
import compliance.NX_DB

import build.NX_TestRunner
import build.NX_BuilderException

import waflib.Configure


def read_test_options(obj, opt, value, parser, *args, **kwargs):
    assert value is None
    value = []

    def floatable(str_val):
        try:
            float(str_val)
            return True
        except ValueError:
            return False

    for arg in parser.rargs:
        # break if something like a --foo option is encountered
        if arg[:2] == "--" and len(arg) > 2:
            break
        if arg[:1] == "-" and len(arg) > 1 and not floatable(arg):
            break
        if arg == "--":
            break
        value.append(arg)

    del parser.rargs[:len(value)]
    setattr(parser.values, "tests", value)


class EngineUTRunner(build.NX_TestRunner.AbstractTestRunner):
    def __init__(self):
        super(EngineUTRunner, self).__init__("Engine Unitary Test Runner")
        self.__Process = []
        self.__Summary = {}
        self.__xml = ""
        self.__junit = ""
        self.__repeat = 0
        self.__tests = []
        self.__fork_mode = False
        self.__ip = ""
        self.__logdir = ""
        self.__list = False
        self.__workspace = ""
        self.__dbg = False
        self.__gdb = False
        self.__valgrind = False
        self.__valgrind_gen = False
        self.__valgrind_xml = ""
        self.__verbose = False
        self.__cmd_parser = None
        self.__modules = ""
        self.__test_framework = ""
        self.__asan_options = ""
        self.__tsan_options = ""

    @property
    def test_modules(self):
        return self.__modules

    @property
    def test_framework(self):
        return self.__test_framework

    @property
    def quality_gate(self):
        return os.path.join(self.workspace, "QualityGate")

    def xml_out_file(self, program_name):
        if self.__xml:
            dir_parent = os.path.dirname(self.__xml)
            basename = os.path.basename(self.__xml)
            basename = "{BASE}.{PROG}.xml".format(BASE=basename, PROG=program_name)
            return os.path.join(dir_parent, basename)
        return ""

    @property
    def junit_out_file(self):
        return self.__junit

    @property
    def repeat(self):
        return self.__repeat

    @property
    def test_list(self):
        return self.__tests

    @test_list.setter
    def test_list(self, tests):
        if tests is not None:
            self.__tests = common.NX_Utils.extend_unique(self.__tests, tests)

    @property
    def verbose(self):
        return self.__verbose

    @property
    def unix_valgrind_out_file(self):
        if self.env.os_name == "Win64":
            return ""
        return self.__valgrind_xml

    @property
    def unix_valgrind(self):
        if self.env.os_name == "Win64":
            return False
        return self.__valgrind or self.__valgrind_gen

    @property
    def unix_valgrind_analyzer(self):
        if self.unix_valgrind:
            return self.__valgrind
        return False

    @property
    def unix_gdb(self):
        if self.env.os_name == "Win64":
            return False
        return self.__gdb

    @property
    def win_dbg(self):
        if self.env.os_name == "Win64":
            return self.__dbg
        return False

    @property
    def workspace(self):
        return self.__workspace

    @property
    def fork_mode(self):
        return self.__fork_mode

    @property
    def ip(self):
        return self.__ip

    @property
    def log_dir(self):
        return self.__logdir

    @property
    def list(self):
        return self.__list

    @property
    def version(self):
        return "2.0"

    @property
    def use_asan_filter(self):
        return self.__asan_options

    @property
    def use_tsan_filter(self):
        return self.__tsan_options

    def set_options(self, waf_context=None):
        if waf_context is None:
            self.__cmd_parser = argparse.ArgumentParser("Engine Unit Test Launcher")
            fun = self.__cmd_parser.add_argument
            fun("--xml", action="store", default="",
                help="This option force outputing results in a XML file.")
            fun("--junit", action="store", default="",
                help="This option force ouputing results in a XML (Junit format) file. Implies the XML option.")
            fun("--repeat", action="store", default=0,
                help="Number of times the series of provided tests must be repeated. Default to a single run.")
            fun("--tests", nargs="+",
                help="If this option is set, only run the provided tests, otherwise all tests are run.")
            fun("--fork-mode", action="store_true", default=False, dest="fork_mode",
                help="Use this option to run each given test separately.")
            fun("--ip", action="store", default="",
                help="IP address of the remote server using format IPV4:SERVICE.")
            fun("--logdir", action="store", default="",
                help="Set the log directory at the specified path.")
            fun("--list", action="store_true", default=False,
                help="List all tests available and make the script returns.")
            fun("--workspace", action="store", default=".",
                help="Allow you to override the workspace (root dir) of the source.")
            fun("--dbg", action="store_true", default=False,
                help="Attach a debugger just after the process has started (Windows only).")
            fun("--gdb", action="store_true", default=False,
                help="Run executable with GDB.")
            fun("--valgrind", action="store_true", default=False,
                help="Run executable with valgrind.")
            fun("--valgrind-error-gen", action="store_true", default=False, dest="valgrind_error_gen",
                help="Run executable with valgrind to generate error suppressions.")
            fun("--valgrind-xml", action="store", default="", dest="valgrind_xml",
                help="Asks valgrind to dump an xml file at the given location. Must not exists.")
            fun("-v", "--verbose", action="store_true", default=False,
                help="Print additional information on stdout, usefull for debugging.")
            fun("--version", action="version", version="%(prog) version {V}".format(V=self.version),
                help="Print version information.")
            fun("--modules", "-m", action="store", default="*", dest="modules",
                help="Choose the test module to run. Can be a wildcard expression.")
            fun("--test-framework", "-t", choices=["gtest", "cpptest", "*"], default="*", dest="test_framework",
                help="Restrict test framework to run. Can be a wildcard expression.")
            fun("--use-asan-filter", default=False, action="store_true",
                dest="use_asan_filter", help="Activate asan suppression filter")
            fun("--use-tsan-filter", default=False, action="store_true",
                dest="use_tsan_filter", help="Activate tsan suppression filter")

            args = self.__cmd_parser.parse_args()
            self.__read_info_from_context(args)
        else:
            grp = waf_context.add_option_group("{N} options".format(N=self.name))
            fun = grp.add_option
            fun("--xml", action="store", default="",
                help="This option force outputing results in a XML file.")
            fun("--junit", action="store", default="",
                help="This option force ouputing results in a XML (Junit format) file. Implies the XML option.")
            fun("--repeat", action="store", default=0,
                help="Number of times the series of provided tests must be repeated. Default to a single run.")
            fun("--tests", callback=read_test_options, action="callback", dest="tests",
                help="If this option is set, only run the provided tests, otherwise all tests are run.")
            fun("--fork-mode", action="store_true", default=False, dest="fork_mode",
                help="Use this option to run each given test separately.")
            fun("--ip", action="store", default="",
                help="IP address of the remote server using format IPV4:SERVICE.")
            fun("--logdir", action="store", default="",
                help="Set the log directory at the specified path.")
            fun("--list", action="store_true", default=False,
                help="List all tests available and make the script returns.")
            fun("--workspace", action="store", default=".",
                help="Allow you to override the workspace (root dir) of the source.")
            fun("--dbg", action="store_true", default=False,
                help="Attach a debugger just after the process has started (Windows only).")
            fun("--gdb", action="store_true", default=False,
                help="Run executable with GDB.")
            fun("--valgrind", action="store_true", default=False,
                help="Run executable with valgrind.")
            fun("--valgrind-error-gen", action="store_true", default=False, dest="valgrind_error_gen",
                help="Run executable with valgrind to generate error suppressions.")
            fun("--valgrind-xml", action="store", default="", dest="valgrind_xml",
                help="Asks valgrind to dump an xml file at the given location. Must not exists.")
            fun("--modules", "-m", action="store", default="*", dest="modules",
                help="Choose the test module to run. Can be a wildcard expression.")
            fun("--test-framework", "-t", choices=["gtest", "cpptest", "*"], default="*", dest="test_framework",
                help="Restrict test framework to run. Can be a wildcard expression.")
            fun("--use-asan-filter", default=False, action="store_true", dest="use_asan_filter",
                help="Activate asan suppression filter")
            fun("--use-tsan-filter", default=False, action="store_true", dest="use_tsan_filter",
                help="Activate tsan suppression filter")

    def __read_info_from_context(self, context):
        self.test_list = context.tests
        self.__workspace = os.path.abspath(context.workspace)
        # To avoid defaulting to ${HOME}...
        if context.logdir:
            self.__logdir = os.path.abspath(context.logdir)
        else:
            self.__logdir = os.path.join(self.workspace, "playground", "log", self.env.platform_id_ext)
        if not os.path.exists(context.workspace):
            raise build.NX_BuilderException.TestError(self.name, "Invalid workspace!")
        self.__dbg = context.dbg
        self.__gdb = context.gdb
        self.__valgrind = context.valgrind
        self.__valgrind_gen = context.valgrind_error_gen
        self.__valgrind_xml = context.valgrind_xml
        self.__verbose = context.verbose
        self.__list = context.list
        self.__ip = context.ip
        self.__fork_mode = context.fork_mode
        self.__repeat = context.repeat
        self.__junit = context.junit
        self.__asan_options = context.use_asan_filter
        self.__tsan_options = context.use_tsan_filter
        if self.__junit:
            self.__junit = os.path.abspath(self.__junit)
            if not os.path.exists(os.path.dirname(self.junit_out_file)):
                raise build.NX_BuilderException.TestError(self.name,
                                                          "Invalid path provided to store Junit output file!")
        self.__xml = context.xml
        if self.__xml and not os.path.isabs(self.__xml):
            self.__xml = os.path.abspath(self.__xml)
        self.__modules = context.modules.split(",")
        self.__test_framework = context.test_framework
        # Sanity check
        if self.unix_valgrind and self.unix_gdb:
            raise Exception("Cannot run tests with both Valgrind and GDB. Please choose one program only.")
        if not self.unix_valgrind and self.unix_valgrind_out_file:
            raise Exception("An xml report from valgrind is requested, but valgrind has not been turned on.")
        if os.path.exists(self.unix_valgrind_out_file):
            raise Exception("The path to the xml report from valgrind already exists!")

    def configure(self, waf_context):
        if waf_context is None:
            raise build.NX_BuilderException.TestError(self.name,
                                                      "Cannot configure this test runner without a valid WAF-context!")
        if not isinstance(waf_context, waflib.Configure.ConfigurationContext):
            raise build.NX_BuilderException.TestError(self.name,
                                                      "Cannot configure this test runner : bad WAF-context type given!")
        waf_context.env.tests = waf_context.options.tests
        waf_context.env.logdir = waf_context.options.logdir
        waf_context.env.workspace = waf_context.options.workspace
        waf_context.env.dbg = waf_context.options.dbg
        waf_context.env.gdb = waf_context.options.gdb
        waf_context.env.valgrind = waf_context.options.valgrind
        waf_context.env.valgrind_gen = waf_context.options.valgrind_error_gen
        waf_context.env.valgrind_xml = waf_context.options.valgrind_xml
        waf_context.env.verbose = waf_context.options.verbose
        waf_context.env.list = waf_context.options.list
        waf_context.env.ip = waf_context.options.ip
        waf_context.env.fork_mode = waf_context.options.fork_mode
        waf_context.env.repeat = waf_context.options.repeat
        waf_context.env.junit = waf_context.options.junit
        waf_context.env.xml = waf_context.options.xml
        waf_context.env.modules = waf_context.options.modules
        waf_context.env.test_framework = waf_context.options.test_framework
        waf_context.env.use_tsan_filter = waf_context.options.use_tsan_filter
        waf_context.env.use_asan_filter = waf_context.options.use_asan_filter
        self.__read_info_from_context(waf_context.env)

    def print_infos(self):
        """
        Dump basic information about the incoming run.
        """
        print "-" * 80
        print """
      Workspace set to [{WSDIR}]
      Build [{BUILD}] on platform [{PLATFORM}]
      Logdir positioned at [{LOGDIR}]
      Playground lib directory located under [{LIBDIR}]
""".format(WSDIR=common.NX_Colors.colored_text("GREEN", self.env.workspace),
           BUILD=common.NX_Colors.colored_text("GREEN", self.env.build_mode),
           PLATFORM=common.NX_Colors.colored_text("GREEN", self.env.platform_id_ext),
           LOGDIR=common.NX_Colors.colored_text("GREEN", os.environ["LOGDIR"]),
           LIBDIR=common.NX_Colors.colored_text("GREEN", self.env.playground_lib_dir))
        print "-" * 80
        print "{Key} has been set to {Value}".format(Key=self.env.get_lib_loader_env_key(),
                                                     Value=os.getenv(self.env.get_lib_loader_env_key()))
        print "-" * 80

    @staticmethod
    def extend_environment_from_file(file_path):
        try:
            with open(file_path, "r") as f:
                for i in f:
                    line = i.strip().split("=")
                    os.environ[line[0]] = line[1]
        except Exception:
            raise Exception("Cannot find/open file '%s', please check your environment!" % file_path)

    def __find_all_programs_to_run(self):
        final_lst = set()
        tstfw = "*test" if self.test_framework == "*" else self.test_framework
        for mod in self.test_modules:
            pattern = common.NX_Utils.make_bin_name("{MODULE}_{TSTFW}".format(MODULE=mod, TSTFW=tstfw))
            for prg in [i for i in glob.glob(os.path.join(self.env.playground_bin_dir, pattern)) if os.path.isfile(i)]:
                if prg.endswith(".exe"):
                    final_lst.add(os.path.splitext(prg)[0])
                else:
                    final_lst.add(prg)
        return [i for i in final_lst]

    def __add_program_to_run_list(self):
        if self.unix_valgrind:
            for applicable in self.__find_all_programs_to_run():
                proc = common.NX_Process.Process("valgrind")
                # Warning : ugly trick ...
                setattr(proc, "apply_on", applicable)
                proc.program_path = os.path.join(self.env.tools_root_dir, "utils", "valgrind", "last", "bin")
                self.__Process.append(proc)
        elif self.unix_gdb:
            for applyable in self.__find_all_programs_to_run():
                proc = common.NX_Process.Process("gdb")
                # Warning : ugly trick ...
                proc.apply_on = applyable
                proc.program_path = os.path.join(self.env.tools_root_dir, "utils", "gdb", "64bits", "last", "bin")
                self.__Process.append(proc)
        else:
            progs = self.__find_all_programs_to_run()
            if not progs:
                raise build.NX_BuilderException.TestError(self.name, "There are no program to run.")
            self.__Process.extend([common.NX_Process.Process(i) for i in progs])
            for process in self.__Process:
                process.program_path = self.env.playground_bin_dir
        for process in self.__Process:
            process.cwd = os.path.join(self.env.workspace, "UnitTests")

    def setup(self, context=None):
        if context is not None:
            self.__read_info_from_context(context.env)
        db_env_info = compliance.NX_DB.DBSetup()
        db_env_info.setup()
        self.env.workspace = self.workspace
        # Log directory customization
        os.environ["LOGDIR"] = self.log_dir
        # Adding playground lib dir to loader search path.
        paths = [self.env.playground_lib_dir]
        if self.env.os_name in ["linux", "sol", "aix"]:
            paths.append(os.path.join(self.env.tools_root_dir, "compilers", self.env.platform, self.env.compiler_type,
                                      self.env.compiler_version, "lib%s" % self.env.build_bits))
        self.env.extend_lib_loader_path(paths, True)
        # debug environment.
        if self.win_dbg:
            os.environ["DEBUGBREAK"] = "1"
        # Cannot have a portable configuration without that...
        os.environ["DLL_PFX"] = "" if self.env.os_name == "Win64" else "lib"
        os.environ["DLL_EXT"] = ".dll" if self.env.os_name == "Win64" else ".so"
        os.environ["NOMEMOPTIMIZE"] = "1"
        os.environ["NX_BUILD_PLATFORM_EXT"] = self.env.platform_id_ext
        # Adjusting path of program
        self.__add_program_to_run_list()
        self.extend_environment_from_file(os.path.join(self.env.workspace, "UnitTests", "etc", "env.properties"))
        self.print_infos()

    def __run_program(self, process, **kwargs):
        if self.verbose:
            print "-" * 80
            print "Running command '{CMD} {ARGS}'.".format(CMD=process.full_path, ARGS=" ".join(process.args))
        if kwargs.get("active_polling", True):
            process.run_and_poll(**kwargs)
        else:
            process.run(**kwargs)
        if self.verbose:
            print "Execution complete, rc [{RC}].".format(RC=process.last_return_code)
        return process.last_return_code

    def __extract_test_list(self, process):
        if self.verbose:
            print "Extracting list of tests..."
        process.Args = ["-l"]
        return_code = self.__run_program(process, out_prefix="", err_prefix="STDERR: ", active_polling=False,
                                         keep_stdout=False, keep_stderr=False)
        test_list = []
        if 0 == return_code:
            f = open("%s.list.txt" % process.full_path, "r")
            for i in f:
                tmp = i[:i.rfind("::")]
                if tmp not in test_list:
                    test_list.append(tmp)
        return test_list

    def add_program_cpp_unit_options(self, process, cmd):
        process.args = ["-l", self.env.build_mode, self.env.os_name, self.env.workspace]
        xml = self.xml_out_file(process.program)
        if self.ip:
            cmd.extend(["-i", self.ip])
        if xml:
            cmd.extend(["-o", xml])
        if self.junit_out_file and not xml:
            cmd.extend(["-o", self.junit_out_file + "_tmp.xml"])
        if self.repeat:
            cmd.extend(["-r", self.repeat])
        if not self.fork_mode:
            if self.test_list:
                cmd.extend(["-d", ",".join(self.test_list)])

    def add_program_gtest_options(self, process, cmd):
        process.args = ["--gtest_list_tests", self.env.build_mode, self.env.os_name, self.env.workspace]
        xml = self.xml_out_file(process.program)
        if xml:
            cmd.extend(
                ["--gtest_output=xml:{P}".format(P=os.path.dirname(self.xml_out_file(process.program)) + os.path.sep)])
        if self.junit_out_file and not xml:
            cmd.extend(["--gtest_output=xml:{P}".format(P=os.path.dirname(self.junit_out_file) + os.path.sep)])
        if self.repeat:
            cmd.extend(["--gtest_repeat={R}".format(R=self.repeat)])
        if not self.fork_mode:
            if self.test_list:
                cmd.extend(["--gtest_filter=%s" % "".join(self.test_list)])

    def add_program_options(self, process, cmd):
        process_name = process.program_no_ext
        if hasattr(process, "apply_on"):
            process_name = os.path.splitext(process.apply_on)[0]
        unit_test_model = re.compile(".*_gtest")
        if self.list:
            if re.match(unit_test_model, process_name) is None:  # Not dealing with gtest
                process.args = ["-l", self.env.build_mode, self.env.os_name, self.env.workspace]
            else:
                process.args = ["--gtest_list_tests", self.env.build_mode, self.env.os_name, self.env.workspace]
        else:
            if re.match(unit_test_model, process_name) is None:  # Not dealing with gtest
                self.add_program_cpp_unit_options(process, cmd)
            else:
                self.add_program_gtest_options(process, cmd)

    def __execute_single(self, process):
        original_workspace = self.env.workspace
        cmd = []
        if self.list:
            self.add_program_options(process, cmd)
            if self.env.platform == "Win64":
                self.env.workspace = self.env.uworkspace
            self.__run_program(process, out_prefix="", err_prefix="STDERR: ", active_polling=True, keep_stdout=False,
                               keep_stderr=False)
            self.return_code = process.last_return_code
        else:
            if self.unix_valgrind:
                valgrind_suppression_path = os.path.join(self.workspace, self.log_dir, "valgrind.gen.suppression.log")
                cmd.extend(["--leak-check=full", "--show-reachable=yes", "--demangle=no", "--num-callers=64",
                            "--log-file={}".format(valgrind_suppression_path)])
                if not self.unix_valgrind_analyzer:
                    cmd.extend(["--error-limit=no", "--gen-suppressions=all"])
                cmd.extend(["--suppressions=%s" % os.path.join(self.quality_gate, i) for i in
                            ["valgrind_fp.txt", "valgrind_ext.txt", "valgrind_tba.txt"]])
                if self.unix_valgrind_out_file:
                    cmd.extend(["--xml=yes", "--xml-file=%s" % self.unix_valgrind_out_file])
                cmd.extend([os.path.join(self.env.playground_bin_dir, process.apply_on)])
            if self.unix_gdb:
                cmd.extend([os.path.join(self.env.playground_bin_dir, process.apply_on)])
            if self.use_asan_filter:
                os.environ["ASAN_OPTIONS"] = '"suppressions={value}'.format(
                    value=os.path.join(self.workspace, "QualityGate", "asan_ext.txt"))
            if self.use_tsan_filter:
                os.environ["TSAN_OPTIONS"] = '"suppressions={value}'.format(
                    value=os.path.join(self.workspace, "QualityGate", "tsan_ext.txt"))
            self.add_program_options(process, cmd)
            print "  > Command line is : {P} {ARGS}".format(P=process.program, ARGS=" ".join(cmd))
            print "  > Execution directory : {WD}".format(WD=process.cwd)
            if not self.fork_mode:
                cmd.extend([self.env.build_mode, self.env.os_name, self.env.workspace])
                if self.env.platform == "Win64":
                    self.env.workspace = self.env.uworkspace
                process.args = cmd
                self.__run_program(process, out_prefix="", err_prefix="STDERR : ", active_polling=True)
                self.return_code = process.last_return_code
            else:
                tmp = self.__extract_test_list(process) if not self.test_list else self.test_list
                if self.env.platform == "Win64":
                    self.env.workspace = self.env.uworkspace
                for i in tmp:
                    cmd_tmp = copy.deepcopy(cmd)
                    cmd_tmp.extend(["-d", i])
                    process.args = cmd_tmp
                    self.__run_program(process, out_prefix="", err_prefix="STDERR : ", active_polling=True)
                    if process.last_return_code != 0:
                        self.return_code += 1
                printer = common.NX_Colors.print_info
                printer("%s%s%s%s%s" % (os.linesep, os.linesep, os.linesep, os.linesep, "-" * 80))
                printer(" Fork Mode summary (program {P}:".format(P=process.program))
                printer("   [%d/%d] Execution returned successfully." % (len(tmp) - self.return_code, len(tmp)))
        if self.env.platform == "Win64":
            self.env.workspace = original_workspace
        return self.return_code

    def execute(self):
        ret_code = []
        for process in self.__Process:
            if hasattr(process, "apply_on"):
                os.environ["NX_TEST_APPNAME"] = getattr(process, "apply_on").upper()
                print "**********    Executing Process [{P}] on [{WHAT}]   **********".format(P=process.program,
                                                                                              WHAT=process.apply_on)
            else:
                os.environ["NX_TEST_APPNAME"] = process.program_no_ext
                print "**********    Executing Process [{P}]    **********".format(P=process.program)
            return_code = self.__execute_single(process)
            if return_code != 0:
                print "Process [{P}] has terminated badly with return code {RC}".format(P=process.program,
                                                                                        RC=return_code)
                self.__Summary[process.program] = "FAILED ({RC})".format(RC=return_code)
            else:
                print "Process [{P}] has terminated successfully".format(P=process.program)
                self.__Summary[process.program] = "SUCCESS"
            ret_code.append(return_code)
        # Dumping test summary
        print os.linesep * 3
        print "Test Execution Summary"
        print "{:^30}|{:^30}".format("Test suite", "Execution status")
        print 30 * "-" + "+" + 30 * "-"
        for i in self.__Summary.keys():
            print "{:^30}|{:^30}".format(os.path.splitext(i)[0], self.__Summary[i])
        print os.linesep * 3
        # we return the first non zero value
        for rc in ret_code:
            if rc != 0:
                raise build.NX_BuilderException.TestError(self.name, "At least one unit test failed.")
        return 0

    def post_process(self):
        if self.junit_out_file:
            if self.verbose:
                print "Post Processing test reports."
            if os.path.exists("%s_tmp.xml" % self.junit_out_file):
                converter = compliance.NX_ReportConverter.TestsConverter(os.path.join(".", "data", "cppunit2junit.xsl"),
                                                                         "%s_tmp.xml" % self.junit_out_file,
                                                                         self.junit_out_file)
                converter.verbose = True
                converter.xlator = os.path.join(".", "data", "Xslt.java")
                converter.run()
                if converter.return_code == 0:
                    common.NX_Utils.erase_fs_entry("%s_tmp.xml" % self.junit_out_file)
            else:
                pattern = ".*_cpptest.exe" if self.env.platform == "Win64" else ".*_cpptest"
                prg_cpp = [i for i in self.__find_all_programs_to_run() if re.match(pattern, i)]
                if not prg_cpp:
                    print "Nothing to process"
                raise build.NX_BuilderException.TestError(self.name,
                                                          "Cannot run post process task to write JUNIT report.")
