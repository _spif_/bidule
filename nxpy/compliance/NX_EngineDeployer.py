import argparse
import os
import platform
import tempfile
import shutil
import tarfile
import time
import zipfile
import textwrap

import build.NX_ManifestReader
import build.NX_BuilderException
import common.NX_Utils
import common.NX_Env
import common.NX_Colors
import common.NX_RestClient
import common.NX_ArtifactoryDialect


class DeployError(Exception):
    """
    Error raised whenever a problem is encountered during Deploy stage.
    """

    def __init__(self, message, error_code):
        msg = "Deploy Error%s  > %s" % (os.linesep, message)
        super(DeployError, self).__init__(msg)
        self.__err_code = error_code

    @property
    def error_code(self):
        return self.__err_code


WIN_PLATFORM = "Win64"
VERSION = "2.2"

ARCHIVE_EXT_TARBALL = ".tar.gz"
ARCHIVE_EXT_TARBALL_BZ = ".tar.bz2"
ARCHIVE_EXT_ZIP = ".zip"

ENVIRON_FILE = "nx_env.sh"
MANIFEST = "manifest.xml"

ARTIFACT_REPOSITORY = "compliance-deployer:compliance123@http://nx-artifacts:8085/artifactory"
REPOSITORY_NAME = "com.neoxam.compliance"

# ERROR CODES
ERROR_CANNOT_RETRIEVE_PROPERTIES = 11
ERROR_CANNOT_RETRIEVE_PLATFORMS = 12
ERROR_CANNOT_FIND_MANIFEST = 13
ERROR_INVALID_ENVIRONMENT = 14
ERROR_INVALID_MANIFEST = 15
ERROR_CANNOT_PERFORM_SEARCH = 16
ERROR_CANNOT_RETRIEVE_STATS = 17
ERROR_CANNOT_LIST_REPOSITORY = 18
ERROR_INVALID_OPTION_VALUE = 19
ERROR_BAD_USAGE = 20
ERROR_BAD_USAGE_CONTEXT = 21
ERROR_BAD_SETUP = 22
ERROR_CANNOT_DOWNLOAD_ARTEFACT = 23
ERROR_NO_ARTEFACT_FOUND = 24
ERROR_INVALID_ARCHIVE_FORMAT = 25
ERROR_BACKUP_EXISTS = 26
ERROR_INVALID_TARGET = 27
ERROR_REST_EXCEPTION_RAISED = 28
ERROR_UNKNOWN = 29
ERROR_MANIFEST_INCONSISTENCY = 30
ERROR_BAD_VERSION = 31


def test_as_bzip2_archive(file_path):
    """
    Validates that the given archive is a valid bzip2 file.
    :param file_path: Path to the archive. Must exists.
    :type file_path: str
    :return: True if the file is a valid bzip2 file, False otherwise.
    """
    try:
        if not tarfile.is_tarfile(file_path):
            return False
        arch = tarfile.open(file_path, "r:bz2")
        arch.close()
    except tarfile.TarError:
        return False
    return True


def test_as_gzip_archive(file_path):
    """
    Validates that the given archive is a valid gzip file.
    :param file_path: Path to the archive. Must exists.
    :type file_path: str
    :return: True if the file is a valid gzip file, False otherwise.
    """
    try:
        if not tarfile.is_tarfile(file_path):
            return False
        arch = tarfile.open(file_path, "r:gz")
        arch.close()
    except tarfile.TarError:
        return False
    return True


def test_as_zip_archive(file_path):
    """
    Validates that the given archive is a valid zip file.
    :param file_path: Path to the archive. Must exists.
    :type file_path: str
    :return: True if the file is a valid zip file, False otherwise.
    """
    try:
        if not zipfile.is_zipfile(file_path):
            return False
        arch = zipfile.ZipFile(file_path, 'r')
        arch.close()
    except zipfile.BadZipfile:
        return False
    except zipfile.LargeZipFile:
        return False
    return True


def get_default_os():
    if platform.system() == "AIX":
        return "aix"
    elif platform.system() == "Linux":
        return "linuxCentOS"
    elif platform.system() == "SunOS":
        return "solaris"
    elif platform.system() == "Windows":
        return "win"
    return None


class EngineDeployer(object):
    """
    This class handle several aspects of the deployment from downloading a version (or revision or
    even a specific build) to the deployment. Nonetheless, the tool can interact with a distant repository in order
    to provide a CLI, easy-to-use system for :
     * querying information
     * searching for archives (with criteria)
     * listing artefacts by revision
     * deployment using a distant source.
    Checks are done to provide a trustworthy tool in all aspects, from parameter analysis to deployment steps.
    If a step cannot complete, this class will automatically take care of it by reverting the
    install-dir in its previous state.
    """

    def __init__(self):
        # Command line parser
        self.__args = None
        # Flag used for indicating a restoration point is asked.
        self.__restore_point = False
        # Used for interacting with the artifactory. Created in __ReadOptionFromContext
        self.__rest_client = None
        self.__version_id = None
        self.__artifact_ext = {"solaris": ".Sol10.tar.bz2", "aix": ".Aix71.tar.bz2",
                               "linuxUbuntu": ".Linuxubu1204.tar.bz2",
                               "linuxCentOS": ".Linuxcentos7.tar.bz2", "win": ".Win64.zip"}
        # Extraction directory
        self.__xtract_dir = ""
        # Archive to install
        self.__cur_archive = ""
        # Used to read the manifest
        self.__bck_folder = []
        self.__progress_counter = 0
        self.__progress_total = 0
        self.__archive_contents = []
        self.__arch_reader = None
        self.__arch_type = None
        self.__properties_desc = {
            "os": """Os name for which the artifact is intended for. Valid values are 
'linuxUbuntu','linuxCentOS','win','aix','solaris' and 'all' if you want a cross platform description.""",
            "osversion": "Os level. Possible values : '10', '12.04'...",
            "bitmode": "Bit mode of the application. Valid values are '32','64'",
            "debug": "Indicates if the artifact is intended for debug purpose.",
            "compiler": "Compiler family used to generate the artifact. Valid values are 'GCC', 'MSVC'",
            "compilerversion": "Level of the compiler. Possible values : '4.8.5', '1900'.",
            "subsystem": "Subsystem to target (engine, ccl and so on..."}
        self.__properties = {}

    @staticmethod
    def dump_version():
        """
        Dumps version information each time an action is to be executed.
        """
        print
        print common.NX_Colors.colored_text("BOLD", "NX-PM Deployer")
        print "Version ", common.NX_Colors.colored_text("BOLD", VERSION)
        print

    @property
    def args(self):
        return self.__args

    @property
    def version_id(self):
        return self.__version_id

    @version_id.setter
    def version_id(self, value):
        self.__version_id = value

    @property
    def version_major(self):
        """
        Retrieves the major version : 2.9.1 --> 2.9
                                      2.9.bld33 --> 2.9
                                      2.9-SNAPSHOT --> 2.9
                                      2.9.bld222-HEAD --> 2.9
                                      2-X -> raise
                                      2 -> raise
        :rtype: str
        """
        all_ids = self.version_id
        if all_ids.find("-") >= 0:
            all_ids = all_ids[:all_ids.find("-")]
        all_ids = all_ids.split(".")
        if len(all_ids) < 2:
            raise DeployError("Invalid version string!", ERROR_BAD_VERSION)
        return ".".join(all_ids[:2])

    @property
    def version_flag(self):
        if self.version_id.find("-") >= 0:
            return self.version_id.split("-")[-1]
        return None

    @property
    def version_minor(self):
        """
        Retrieves the full minor version : 2.9.1 --> 2.9.1
                                      2.9.bld33 --> bld33
                                      2.9-SNAPSHOT --> None
                                      2.9.bld222-HEAD --> bld222
        :rtype: str or None
        """
        all_ids = self.version_id
        if all_ids.find("-") >= 0:
            all_ids = all_ids[:all_ids.find("-")]
        all_ids = all_ids.split(".")
        if all_ids[-1].startswith("bld"):
            return all_ids[-1]
        if len(all_ids) >= 3:
            return ".".join(all_ids)
        return ""

    @property
    def current_archive(self):
        return self.__cur_archive

    @current_archive.setter
    def current_archive(self, value):
        self.__cur_archive = value

    @property
    def extraction_dir(self):
        return self.__xtract_dir

    @extraction_dir.setter
    def extraction_dir(self, value):
        self.__xtract_dir = value
        common.NX_Utils.erase_fs_entry(self.__xtract_dir)

    @property
    def rest_client(self):
        """
        :return:
        :rtype: common.NX_ArtifactoryDialect.ArtifactoryInterpret
        """
        return self.__rest_client

    @property
    def show_artifacts(self):
        return self.args.artifact_flag

    @property
    def show_builds(self):
        return self.args.bld_flag

    @property
    def backup_folders(self):
        return self.__bck_folder

    @property
    def look_up_properties(self):
        return self.__properties

    def archive_reader_list_members(self):
        """
        List all files in the archive. Directories are filtered out.
        Once members have been listed, progress informations are set.
        """
        if self.__arch_type == ARCHIVE_EXT_ZIP:
            info = self.__arch_reader.infolist()
            self.__archive_contents = [i.filename for i in info if i.file_size > 0]
        elif self.__arch_type == ARCHIVE_EXT_TARBALL_BZ:
            info = self.__arch_reader.getmembers()
            self.__archive_contents = [i.name[2:] for i in info if not i.isdir()]
        elif self.__arch_type == ARCHIVE_EXT_TARBALL:
            info = self.__arch_reader.getmembers()
            self.__archive_contents = [i.name[2:] for i in info if not i.isdir()]
        self.__progress_total = len(self.__archive_contents)
        self.__progress_counter = 0

    def arch_reader_extract(self, what, where):
        """
        Extract the specified entry from the archive and put it under the given path.
        :param what: entry to extract
        :type what: str
        :param where: storage location
        :type where: str
        """
        if self.__arch_type == ARCHIVE_EXT_ZIP:
            self.__arch_reader.extract(what, where)
        elif self.__arch_type == ARCHIVE_EXT_TARBALL_BZ:
            self.__arch_reader.extract("./" + what, where)
        elif self.__arch_type == ARCHIVE_EXT_TARBALL:
            self.__arch_reader.extract("./" + what, where)

    def __delete_back_up_folders(self):
        """
        Erase all the entries identified as backup folders to let the installation as clean as possible.
        """
        for d in self.backup_folders:
            common.NX_Utils.erase_fs_entry(d)
        self.__bck_folder = []

    def __revert_to_backup(self):
        """
        Replace the current installation by the backup point.
        """
        print "reverting backup"
        for d in self.backup_folders:
            current, ext = os.path.splitext(d)
            if self.args.verbose:
                print " processing {} ...".format(current)
            common.NX_Utils.erase_fs_entry(current)
            shutil.move(d, current)

    def set_url_info(self, namespace):
        try:
            url = namespace.url.split("@")[1]
            user = namespace.url.split("@")[0].split(":")[0]
            password = namespace.url.split("@")[0].split(":")[1]
        except Exception:
            raise DeployError("Invalid url given. Format must be USER:PASSWORD@URI", ERROR_INVALID_OPTION_VALUE)
        self.__rest_client = common.NX_ArtifactoryDialect.ArtifactoryInterpret(user=user, password=password, url=url)
        self.__rest_client.translator = common.NX_ArtifactoryDialect.JSonTranslator()
        self.__rest_client.group_id = namespace.group_id

    @staticmethod
    def check_repository_entry_property(entry_properties, key, expected_value):
        """
        Validate a property of an artefact stored on the distant repository.
        :param entry_properties: Property container
        :type entry_properties: dict
        :param key: property name to validate
        :type key: str
        :param expected_value: expected value
        :type expected_value: str
        :return: True if the property exists and match the expected value, False otherwise.
        :rtype: bool
        """
        return key in entry_properties and entry_properties[key][0] == expected_value

    def is_repository_entry_valid_version(self, entry_properties):
        """
        Check that the entry in the distant repository locates a version.
        :param entry_properties: Property container
        :type entry_properties: dict
        :return: True if the property exists and match the expected value, False otherwise.
        :rtype: bool
        """
        return self.check_repository_entry_property(entry_properties, "isversion", "true")

    def is_repository_version_deprecated(self, entry_properties):
        """
        Check that the entry in the distant repository locates a deprecated version.
        :param entry_properties: Property container
        :type entry_properties: dict
        :return: True if the property exists and match the expected value, False otherwise.
        :rtype: bool
        """
        return self.check_repository_entry_property(entry_properties, "isversion", "true") and \
            self.check_repository_entry_property(entry_properties, "deprecated", "true")

    def keep_version_id(self, version_id):
        if not self.args.versions:
            return True
        version_id = str(version_id)
        return version_id in self.args.versions

    def get_path_of_item_from_version(self, target_os=None, with_artifact=False):
        resource = ""
        if self.version_minor:
            if self.version_flag:
                resource += "/".join([self.version_major, self.version_flag, self.version_minor])
            else:
                resource += "/".join([self.version_major, "SNAPSHOT"])
        else:
            resource += "/".join([self.version_major, "SNAPSHOT"])
        if with_artifact:
            if not target_os:
                target_os = get_default_os()
            resource = "/".join([resource, self.args.artifact + self.__artifact_ext[target_os]])
        return resource

    def __do_list_properties(self):
        """
        Dump an inline help regarding the different properties one can set for specific actions.
        """
        print "Properties listing"
        print ""
        print "-" * 30 + "+" + "-" * 65
        print " " * 30 + "|" + " " * 64
        print "{:^30}|{:^64}".format("Property Key", "Description")
        print " " * 30 + "|" + " " * 64
        print "-" * 30 + "+" + "-" * 65
        for p in self.__properties_desc:
            wrap = textwrap.wrap(self.__properties_desc[p], 64)
            if len(wrap) == 1:
                print "{:^30}| {}".format(p, self.__properties_desc[p])
            else:
                print "{:^30}| {}".format(p, wrap[0])
                for l in wrap[1:]:
                    print " " * 30 + "| {}".format(l)
            print "-" * 30 + "+" + "-" * 65

    def __do_info(self):
        """
        Dump information about an artefact as well as all its properties set. Raise if the artefact cannot be found.
        :raise: build.NX_BuilderException.DeployError
        """
        try:
            auto = self.rest_client.get_item_properties(self.get_path_of_item_from_version(with_artifact=True),
                                                        all=True)
            if not auto.ok:
                print common.NX_Colors.error("No artifact found matching these requirements!")
            else:
                print "Properties"
                print "{:.<32} {}".format("GroupID ", self.rest_client.group_id)
                print "{:.<32} {}".format("ArtifactID ", self.args.artifact)
                print "{:.<32} {}".format("VersionID ", self.version_id)
                for p in auto.content["properties"]:
                    print "{:.<32} {}".format(p, auto.content["properties"][p][0])
                print ""
                print ""
        except common.NX_RestClient.RestException, e:
            raise DeployError(str(e), ERROR_CANNOT_RETRIEVE_PROPERTIES)

    def __get_all_platforms_for_revision(self):
        """
        Retrieve all platforms for a given revision (or build), based on the artefacts part of the revision (or build).
        Raise in case of error.
        :return: A list of string, corresponding to the operating system provided.
        :raise: build.NX_BuilderException.DeployError
        """
        try:
            result = []
            item_path = self.get_path_of_item_from_version(None, False)
            children = self.rest_client.get_item_children(item_path, ["uri"])
            for child in children:
                # We are at artifact level.
                the_path = "/".join([item_path, child])
                artifact_properties = self.rest_client.get_item_properties(the_path, params={"properties": "os"})
                os_value = artifact_properties.content["properties"]["os"][0]
                if os_value not in result:
                    result.append(os_value)
            return result
        except common.NX_RestClient.RestException, e:
            raise DeployError(str(e), ERROR_CANNOT_RETRIEVE_PLATFORMS)

    def __create_manifest_reader(self):
        """
        Validate the manifest and the environment context. If one of them fails the test, an error will be raised and
        the archive will not be deployed. Otherwise, the environment will be set accordingly for the archive to
        be deployed and a manifest reader will be created.
        :return: A manifest reader which has already parsed the archive's manifest file.
        :rtype: build.NX_ManifestReader.PackageManifestReader
        :raise: build.NX_BuilderException.DeployError
        """
        # Let's check for manifest existence.
        if not os.path.exists(os.path.join(self.extraction_dir, MANIFEST)):
            msg = "Cannot find {M} file in archive {A}".format(A=os.path.basename(self.current_archive), M=MANIFEST)
            raise DeployError(msg, ERROR_CANNOT_FIND_MANIFEST)
        # Environment ?
        with open(common.NX_Utils.find_file_in_directory(self.extraction_dir, ENVIRON_FILE)) as env_file:
            for l in env_file:
                line = l.strip()
                if not line.startswith("#"):
                    tokens = line.replace("export ", "").split("=")
                    os.environ[tokens[0]] = tokens[1]

        # Validate environment
        if not common.NX_Env.Env().valid_for_deployment():
            raise DeployError("Environment is invalid for deployment!", ERROR_INVALID_ENVIRONMENT)

        # Setting up reader.
        reader = build.NX_ManifestReader.PackageManifestReader(os.path.join(self.extraction_dir, MANIFEST))
        # extend manifest reader mapping to match platform names used in archives and
        # dev envs with names of target env folders
        os_tags = {'AIX71': 'aix', 'SOL10': 'sol', 'LinuxUBU1204': 'linux', 'LinuxCentOS7': 'linux', 'Win64': 'win'}

        path_deploy_logs = 'tmp/others/deployLogs/%s' % os_tags[os.environ["NX_PLATFORM"]]
        path_ccl_bin_dir = 'IFT/%s/bin' % os_tags[os.environ["NX_PLATFORM"]]
        path_engine_bin_dir = 'opt/CS/bin/%s' % os_tags[os.environ["NX_PLATFORM"]]
        path_pma_bin_dir = 'opt/PMA/bin/%s' % os_tags[os.environ["NX_PLATFORM"]]
        path_sls_bin_dir = 'opt/SLS/bin/%s' % os_tags[os.environ["NX_PLATFORM"]]
        path_bondprobe_bin_dir = 'opt/IDBP/cel/%s' % os_tags[os.environ["NX_PLATFORM"]]

        reader.extend_mapping({"nx.compliance.engine.deployLogDir": "'%s'" % path_deploy_logs,
                               "nx.compliance.ccl.bindir": "'%s'" % path_ccl_bin_dir,
                               "nx.compliance.engine.bindir": "'%s'" % path_engine_bin_dir,
                               "nx.compliance.pma.bindir": "'%s'" % path_pma_bin_dir,
                               "nx.compliance.sls.bindir": "'%s'" % path_sls_bin_dir,
                               "nx.compliance.bondprobe.bindir": "'%s'" % path_bondprobe_bin_dir
                               },
                              {"nx.compliance.engine.deployLogDir": path_deploy_logs,
                               "nx.compliance.ccl.bindir": path_ccl_bin_dir,
                               "nx.compliance.engine.bindir": path_engine_bin_dir,
                               "nx.compliance.pma.bindir": path_pma_bin_dir,
                               "nx.compliance.sls.bindir": path_sls_bin_dir,
                               "nx.compliance.bondprobe.bindir": path_bondprobe_bin_dir
                               }
                              )
        try:
            reader.read()
            # Print some informations
            c_os = common.NX_Colors.colored_text("GREEN:BOLD", os.environ["NX_PLATFORM"])
            c_mode = common.NX_Colors.colored_text("GREEN:BOLD", os.environ["NX_BUILD_MODE"])
            if self.args.verbose:
                print "Package is intended for {OS} and has been built under {MODE}".format(OS=c_os, MODE=c_mode)
        except build.NX_BuilderException.ManifestError as me:
            raise DeployError(me.raw_msg, ERROR_INVALID_MANIFEST)
        return reader

    def __do_search(self):
        try:
            auto = self.rest_client.property_search(self.look_up_properties)
            if not auto.ok:
                print common.NX_Colors.error("Invalid request!")
            if not auto.content["results"]:
                print common.NX_Colors.warning("No artifacts found matching your criteria!")
            else:
                print "{:^32}|{:^32}".format("Artifact id", "Version")
                print "-" * 32 + "+" + "-" * 32
                for entry in auto.content["results"]:
                    entry_version = entry["uri"][
                                    entry["uri"].find(self.rest_client.group_id) + len(self.rest_client.group_id):]
                    entry_subsystem = self.rest_client.get_item_properties(entry_version,
                                                                           params={"properties": "subsystem"})
                    prop_subsystem_pos = entry_version.find(entry_subsystem.content["properties"]["subsystem"][0])
                    entry_version = entry_version[:prop_subsystem_pos].split("/")
                    if "SNAPSHOT" in entry_version:
                        entry_version = entry_version[0] + "-SNAPSHOT"
                    elif "HEAD" in entry_version:
                        entry_version = entry_version[0] + "-" + entry_version[2]
                    else:
                        entry_version = entry_version[0]
                    print "{:^32}|{:^32}".format(entry_subsystem.content["properties"]["subsystem"][0], entry_version)
        except common.NX_RestClient.RestException, e:
            raise DeployError(str(e), ERROR_CANNOT_PERFORM_SEARCH)

    def __do_stat(self):
        try:
            path_from_version = self.get_path_of_item_from_version(self.args.platform, True)
            auto = self.rest_client.get_item_stats(path_from_version)
            if not auto.ok:
                print common.NX_Colors.error("No artifact found matching these requirements!")
            else:
                for i in filter(lambda x: x != "uri", auto.content):
                    print "{:.<32} {:<}".format(i, auto.content[i])
        except common.NX_RestClient.RestException, e:
            raise DeployError(str(e), ERROR_CANNOT_RETRIEVE_STATS)

    def __do_list(self):
        """
        List the distant repository for versions, revisions and build.
        The listing can be configured from the command line.
        """
        # Versions under support are dumped in green, unsupported in YELLOW
        revision_by_version = {}
        deprecated_versions = set()
        build_by_version = {}
        artifacts_by_version = {}
        try:
            children = self.rest_client.get_item_children("", ['uri', 'folder'])
            for version in children:
                if children[version]["folder"]:
                    p = {"properties": "deprecated,isversion"}
                    version_info = self.rest_client.get_item_properties(version, params=p)
                    if self.is_repository_entry_valid_version(version_info.content["properties"]):
                        if self.keep_version_id(version):
                            if self.is_repository_version_deprecated(version_info.content["properties"]):
                                deprecated_versions.add(version)
                                continue
                            v_listing = self.rest_client.get_item_children(version, ["folder", "uri"])
                            # Retrieve all children for this version
                            revision_by_version[version] = set(c for c in v_listing if v_listing[c]["folder"])
                            if self.show_artifacts:
                                for folder in filter(lambda x: x != "HEAD", revision_by_version[version]):
                                    artifacts = self.rest_client.get_item_children("/".join([version, folder]), ["uri"])
                                    # Get artifacts properties
                                    art_id = "/".join([version, folder])
                                    artifacts_by_version[art_id] = set()
                                    p = {"properties": "subsystem"}
                                    for item in artifacts:
                                        path = "/".join([version, folder, item])
                                        subsystem = self.rest_client.get_item_properties(path, params=p)
                                        subsystem = subsystem.content["properties"]["subsystem"][0]
                                        artifacts_by_version[art_id].add(subsystem)
                            if self.show_builds:
                                child_path = "/".join([children[version]["uri"], "HEAD"])
                                listing = self.rest_client.get_item_children(child_path, ["folder"])
                                build_by_version[version] = set(child for child in listing if listing[child]["folder"])
                                if self.show_artifacts:
                                    for folder in build_by_version[version]:
                                        path = "/".join([version, "HEAD", folder])
                                        artifacts = self.rest_client.get_item_children(path, ["uri"])
                                        # Get artifacts properties
                                        artifacts_by_version["/".join([version, "HEAD", folder])] = set()
                                        p = {"properties": "subsystem"}
                                        for item in artifacts:
                                            path = "/".join([version, "HEAD", folder, item])
                                            subsystem = self.rest_client.get_item_properties(path, params=p)
                                            subsystem = subsystem.content["properties"]["subsystem"][0]
                                            artifacts_by_version["/".join([version, "HEAD", folder])].add(subsystem)
        except common.NX_RestClient.RestException, e:
            raise DeployError(str(e), ERROR_CANNOT_LIST_REPOSITORY)
        print self.rest_client.group_id
        for v in revision_by_version:
            print "   [{VERSION}]".format(VERSION=common.NX_Colors.colored_text("GREEN:BOLD", v))
            for r in revision_by_version[v]:
                print "      | -- {REV}".format(REV=common.NX_Colors.colored_text("BOLD", r))
                if r == "HEAD":
                    if self.show_builds:
                        for b in build_by_version[v]:
                            print "      |      | -- {BLD}".format(BLD=common.NX_Colors.colored_text("BOLD", b))
                            if self.show_artifacts:
                                sub = ", ".join(artifacts_by_version["/".join([v, "HEAD", b])])
                                print "      |      |    > {SUB}".format(SUB=sub)
                elif self.show_artifacts:
                    print "      |    > {SUB}".format(SUB=", ".join(artifacts_by_version["/".join([v, r])]))
        for v in deprecated_versions:
            print "   [{VERSION}]".format(VERSION=common.NX_Colors.colored_text("YELLOW", v))
        print os.linesep, "-" * 30, os.linesep, "Legend: "
        print " *", common.NX_Colors.colored_text("GREEN:BOLD", " VERSION UNDER SUPPORT")
        print " *", common.NX_Colors.colored_text("YELLOW", " UNSUPPORTED VERSION")

    def print_progress(self, step, file_base_name):
        """
        Utility method to print the deployment' progress bar for a single archive.
        :param step: step identifier. Must be 4 letters (not checked)
        :type step: str
        :param file_base_name: file being processed. Truncated if needed.
        :type file_base_name: str
        """
        self.__progress_counter += 1
        base_name = os.path.basename(file_base_name)
        if len(base_name) > 40:
            base_name = file_base_name[:40] + " ..."
        rw = "{S}. {W}".format(W=base_name, S=step)
        message = "{0:<50}".format(rw)
        common.NX_Utils.progress_bar(self.__progress_counter, self.__progress_total, msg=message)

    def __download_archive(self, uri):
        """
        Download an artifact from the repository and store it as a temporary file.
        Raise in case of error during communication with the server.
        :param uri: Path to the artifact to download.
        :type uri: str
        :return: Path of the downloaded archive.
        :raise: build.NX_BuilderException.DeployError
        """
        if self.args.verbose:
            print "Contacting server ..."
        auto = self.rest_client.get_artefact(uri)
        if not auto.ok:
            msg = "Cannot download artifact '{}': cannot find it under distant repository {}"
            raise DeployError(msg.format(uri, self.rest_client.group_id), ERROR_CANNOT_DOWNLOAD_ARTEFACT)
        chunk_size = 2 ** 16
        length = auto.headers["content-Length"]
        total_iter = int(length) / chunk_size + 1
        iter_count = 0
        if self.args.verbose:
            print "Beginning download ({} bytes)".format(length)
        common.NX_Utils.progress_bar(iter_count, total_iter, msg="Downloading archive")
        where = tempfile.gettempdir()
        time_point = time.time()
        text_msgs = ["Downloading archive", "Downloading archive.", "Downloading archive..", "Downloading archive..."]
        with open(os.path.join(where, os.path.basename(uri)), "wb") as fd:
            for chunk in auto.iter_content(chunk_size=chunk_size):
                fd.write(chunk)
                iter_count += 1
                idx = int(time.time() - time_point)
                if idx > 3:
                    time_point = time.time()
                    idx = 0
                common.NX_Utils.progress_bar(iter_count, total_iter, msg=text_msgs[idx])
                time.sleep(0.005)
        if total_iter == iter_count and self.args.verbose:
            print "Download complete."
        return os.path.join(where, os.path.basename(uri))

    def __create_archive_reader(self):
        basename = os.path.basename(self.current_archive)
        for ext in [ARCHIVE_EXT_ZIP, ARCHIVE_EXT_TARBALL, ARCHIVE_EXT_TARBALL_BZ]:
            if basename.endswith(ext):
                basename = basename[:len(basename) - len(ext)]
                if ext == ARCHIVE_EXT_TARBALL_BZ:
                    if not test_as_bzip2_archive(self.current_archive):
                        raise DeployError("Archive {} is not in bz2 format!".format(self.current_archive),
                                          ERROR_INVALID_ARCHIVE_FORMAT)
                    self.__arch_reader = tarfile.open(self.current_archive, "r:bz2")
                    self.__arch_type = ext
                    return
                elif ext == ARCHIVE_EXT_TARBALL:
                    if not test_as_gzip_archive(self.current_archive):
                        raise DeployError("Archive {} is not in gz format!".format(self.current_archive),
                                          ERROR_INVALID_ARCHIVE_FORMAT)
                    self.__arch_reader = tarfile.open(self.current_archive, "r:gz")
                    self.__arch_type = ext
                    return
                elif ext == ARCHIVE_EXT_ZIP:
                    if not test_as_zip_archive(self.current_archive):
                        raise DeployError("Archive {} is not in zip format!".format(self.current_archive),
                                          ERROR_INVALID_ARCHIVE_FORMAT)
                    self.__arch_reader = zipfile.ZipFile(self.current_archive, 'r')
                    self.__arch_type = ext
                    return
        raise DeployError("Unknown archive type for file {}".format(self.current_archive), ERROR_INVALID_ARCHIVE_FORMAT)

    def __extract(self, extract_all=False):
        """
        Extract the current Archive file in a temporary directory.
        :param extract_all: If True, then all the archive is extracted, the counter being increased
                            accordingly file by file. If False, only the environment and the manifest are extracted.
        :type extract_all: bool
        """
        # Let's compute base name of archive
        if extract_all:
            for i in self.__archive_contents:
                self.arch_reader_extract(i, self.extraction_dir)
                self.print_progress("extr", i)
        else:
            self.extraction_dir = tempfile.mkdtemp(suffix="deployer", prefix="tmp", dir=tempfile.gettempdir())
            os.makedirs(self.extraction_dir)
            self.arch_reader_extract(MANIFEST, self.extraction_dir)
            self.arch_reader_extract("scripts/nx_env.sh", self.extraction_dir)
            self.archive_reader_list_members()

    def __do_check(self):
        if self.args.command == "local-check":
            try:
                self.__do_integrity_check()
            finally:
                common.NX_Utils.erase_fs_entry(self.extraction_dir)
        else:
            try:
                remote_path = self.get_path_of_item_from_version(self.args.platform, True)
                self.current_archive = self.__download_archive(remote_path)
                self.__do_integrity_check()
            finally:
                self.__arch_reader.close()
                common.NX_Utils.erase_fs_entry(self.extraction_dir)
                common.NX_Utils.erase_fs_entry(self.current_archive)

    def __do_integrity_check(self):
        """
        Validate the integrity of the archive: all files in the archive must be declared in the manifest and
        the manifest must list all files in the archive. If one of these two rules is not respected, we throw a
        build.NX_BuilderException.DeployError and the deployment process will get interrupted.
        :raise: build.NX_BuilderException.DeployError
        """
        try:
            self.__create_archive_reader()
            self.__extract()
            self.__reader = self.__create_manifest_reader()
            manifest_entries = [i.replace("\\", "/") for i in self.__reader.manifest.entries.keys()]

            # folders are not listed in the manifest, and zipfile does not allow us to detect them
            only_in_archive = [item for item in self.__archive_contents if
                               item not in manifest_entries and item != MANIFEST]
            only_in_manifest = [item for item in manifest_entries if item not in self.__archive_contents]

            errors = []
            if only_in_archive:
                errors.extend(["File has not been declared : {}".format(i) for i in only_in_archive])
            if only_in_manifest:
                errors.extend(["File not found in archive : {}".format(i) for i in only_in_manifest])
            if errors:
                msg = "Inconsistency detected between archive '{A}' and its manifest '{M}'!"
                errors.insert(0, msg.format(A=self.current_archive, M=MANIFEST))
                raise DeployError(os.linesep.join(errors), ERROR_MANIFEST_INCONSISTENCY)
            print common.NX_Colors.info("Integrity check OK")
        finally:
            common.NX_Utils.erase_fs_entry(self.extraction_dir)
            self.__arch_reader.close()

    def __backup(self):
        """
        Backup all folders that will be impacted. This means that during the installation, we might need up to twice the
        current installation size.
        :return: The exhaustive list of backed up folders.
        :rtype: list
        :raise: build.NX_BuilderException.DeployError
        """
        # build a list of unique destination folders for this archive
        dest_folders = set()
        for entry in self.__reader.manifest.entries:
            dest_folder, flag = self.__reader.manifest.entries[entry]
            dest_folders.add(os.path.normpath(os.path.dirname(os.path.join(self.args.prefix, dest_folder))))

        # backup each folder in the list
        for folder in dest_folders:
            folder_to_bck = folder + '.bak'
            if os.path.exists(folder_to_bck):
                raise DeployError("A backup folder already exists for {E}".format(E=folder_to_bck), ERROR_BACKUP_EXISTS)
            if os.path.exists(folder):
                if self.args.verbose:
                    print "Renaming {} -> {}".format(folder, folder_to_bck)
                os.rename(folder, folder_to_bck)
                self.__bck_folder.append(folder_to_bck)
        return self.backup_folders

    def __do_deploy(self):
        if self.args.command == "local-deploy":
            self.__do_deploy_archive()
        elif self.args.command == "deploy":
            for tmp_platform in self.args.platform:
                print "Downloading artifact {} for platform '{}'".format(self.args.artifact, tmp_platform)
                remote_path = self.get_path_of_item_from_version(tmp_platform, True)
                self.current_archive = self.__download_archive(remote_path)
                try:
                    self.__do_deploy_archive()
                finally:
                    common.NX_Utils.erase_fs_entry(self.current_archive)

    def __deploy_files(self):
        """
        Deploy all files and make links.
        :return: True on success. Throw a build.NX_BuilderException.DeployError exception
                 upon issue to interrupt and revert the install.
        :raise: build.NX_BuilderException.DeployError
        """
        self.__progress_counter = 0
        manifest_entries = self.__reader.manifest.entries
        link_entries = self.__reader.manifest.links
        for entry in manifest_entries:
            dest_path, exec_flag = manifest_entries[entry]
            source_file = os.path.join(self.extraction_dir, entry)
            dest_file = os.path.join(self.args.prefix, dest_path)
            dest_folder = os.path.dirname(dest_file)

            if not os.path.exists(dest_folder):
                os.makedirs(dest_folder)
            elif not os.path.isdir(dest_folder):
                raise DeployError("Target folder {} is in use by a regular file".format(dest_folder),
                                  ERROR_INVALID_TARGET)

            shutil.copy2(source_file, dest_file)
            if WIN_PLATFORM == os.environ["NX_PLATFORM"] or exec_flag:
                common.NX_Utils.set_x_rights_to(dest_file, True, True, True)
            self.print_progress("depl", dest_file)

        for link in link_entries:
            link_as, link_cwd = link_entries[link]
            common.NX_Utils.sym_link(link, link_as, os.path.join(self.args.prefix, link_cwd))
            self.print_progress("link", link_as)
        common.NX_Utils.progress_bar(1, 1)
        return False

    def __do_deploy_archive(self):
        """
        Deploy the archive stored in self.CurrentArchive and cleans temporary files (or revert on issue) once done.
        :raise: build.NX_BuilderException.DeployError, Exception
        """
        b_revert = True
        if self.args.verbose:
            archive = common.NX_Colors.colored_text("CYAN", self.current_archive)
            print "Processing archive for deployment [{}]".format(archive)
        try:
            self.__create_archive_reader()
            self.__extract(extract_all=False)
            print "Reading archive ..."
            self.__extract(extract_all=True)
            self.__reader = self.__create_manifest_reader()
            print "Backup-ing current install, if any ..."
            self.__backup()
            print "Deploying artifact ..."
            b_revert = self.__deploy_files()
        except DeployError as lcl_error:
            b_revert = True
            raise lcl_error
        except Exception as lcl_error:
            b_revert = True
            raise lcl_error
        finally:
            self.__arch_reader.close()
            common.NX_Utils.erase_fs_entry(self.extraction_dir)
            if b_revert:
                print common.NX_Colors.warning("Something went wrong, reverting backup.")
                self.__revert_to_backup()
                print "Ending deployment with status [{}]".format(common.NX_Colors.colored_text("RED", "KO"))
            else:
                self.__delete_back_up_folders()
                print "Ending deployment with status [{}]".format(common.NX_Colors.colored_text("GREEN:BOLD", "OK"))

    def set_options(self):
            """
            Set options to be parsed by the command line parser.
            """
            parser = argparse.ArgumentParser("deployer", version="Version : %(prog)s {}".format(VERSION))
            parser.description = """Extracts contents of NX-archives provided by successful build and deploy them 
 accordingly to their internal manifest. Integrity checks are performed based on the included manifest file.
"""
            parser.add_argument("--verbose", action="store_true", default=False,
                                help="Provide additional details while executing commands")
            commands = parser.add_subparsers(title="Commands", dest="command")

            parser_list = commands.add_parser("list")
            parser_list.description = """List the distant repository for available artifacts given some criteria."""
            fun = parser_list.add_argument
            fun("--builds", action="store_true", default=False, dest="bld_flag",
                help="If passed, builds will be collected and dumped when requesting a repository listing.")
            fun("--versions", action="store", nargs="+", dest="versions",
                help="Provide restrictions capabilities on versions for listings.")
            fun("--artifacts", action="store_true", default=False, dest="artifact_flag",
                help="If set, all artifacts id will be listed when requesting a repository listing.")
            fun("--url", action="store", default=ARTIFACT_REPOSITORY, dest="url",
                help="Indicates the address for the artifacts repository. Format is USER:PWD@PATH.")
            fun("--groupId", action="store", default=REPOSITORY_NAME, dest="group_id",
                help="Indicates the repository name from which artifacts will be searched.")

            parser_info = commands.add_parser("info")
            parser_info.description = """Dumps information about an artifact as well as all its properties."""
            fun = parser_info.add_argument
            fun("--version", action="store", required=True, dest="version",
                help="Provide the version for which the information is requested.")
            fun("--artifact", action="store", required=True, dest="artifact",
                help="Set the artifact's name for which the information request is about.")
            fun("--url", action="store", default=ARTIFACT_REPOSITORY, dest="url",
                help="Indicates the address for the artifacts repository. Format is USER:PWD@PATH.")
            fun("--groupId", action="store", default=REPOSITORY_NAME, dest="group_id",
                help="Indicates the repository name from which artifacts will be searched.")

            commands.add_parser("list-properties")

            parser_search = commands.add_parser("search")
            parser_search.description = """Search for artifact(s) given properties. This command will scan the whole 
repository, thus it may take a long time before completing. Use it knowingly."""
            fun = parser_search.add_argument
            fun("--url", action="store", default=ARTIFACT_REPOSITORY, dest="url",
                help="Indicates the address for the artifacts repository. Format is USER:PWD@PATH.")
            fun("--groupId", action="store", default=REPOSITORY_NAME, dest="group_id",
                help="Indicates the repository name from which artifacts will be searched.")
            fun("--platform", choices=["aix", "win", "linuxUbuntu", "linuxCentOS", "solaris", "all"], dest="platform",
                default="all", help="Indicates the repository name from which artifacts will be searched")
            fun("--properties", action="store", dest="properties", nargs="+",
                help="Allows you to specify criteria for your search. Format is <key>:<value>.")

            parser_stat = commands.add_parser("stat")
            parser_stat.description = "Collect statistics and information about the requested artifact."
            fun = parser_stat.add_argument
            fun("--version", action="store", required=True, dest="version",
                help="Provide the version for which the information is requested.")
            fun("--artifact", action="store", required=True, dest="artifact",
                help="Set the artifact's name for which the information request is about.")
            fun("--url", action="store", default=ARTIFACT_REPOSITORY, dest="url",
                help="Indicates the address for the artifacts repository. Format is USER:PWD@PATH.")
            fun("--groupId", action="store", default=REPOSITORY_NAME, dest="group_id",
                help="Indicates the repository name from which artifacts will be searched.")
            fun("--platform", choices=["aix", "win", "linuxUbuntu", "linuxCentOS", "solaris", "all"], dest="platform",
                default=get_default_os(), help="Indicates the repository name from which artifacts will be searched")

            parser_download = commands.add_parser("download")
            parser_download.description = "Download an artifact from the remote server."
            fun = parser_download.add_argument
            fun("--artifact", action="store", dest="artifact", required=True,
                help="Set the artifact's name to download.")
            fun("--url", action="store", default=ARTIFACT_REPOSITORY, dest="url",
                help="Indicates the address for the artifacts repository. Format is USER:PWD@PATH.")
            fun("--groupId", action="store", default=REPOSITORY_NAME, dest="group_id",
                help="Indicates the repository name from which artifacts will be searched.")
            fun("--version", action="store", dest="version", required=True,
                help="Provide the version for which the download is requested.")
            fun("--to", action="store", default=os.path.abspath(os.curdir),
                help="Path to where the archive must be downloaded. Default is current working directory.")
            fun("--platform", action="store", default=get_default_os(),
                help="Specific platform to download. If not set, current platform is considered.")

            parser_check = commands.add_parser("check")
            parser_check.description = "Executes the integrity check on the given archive"
            check_group = parser_check.add_argument_group("remote-access",
                                                          "Options for controlling remote archive checks.")
            fun = check_group.add_argument
            fun("--artifact", action="store", dest="artifact", default=None,
                help="Set the artifact's name for which the check is requested.")
            fun("--url", action="store", default=ARTIFACT_REPOSITORY, dest="url",
                help="Indicates the address for the artifacts repository. Format is USER:PWD@PATH.")
            fun("--groupId", action="store", default=REPOSITORY_NAME, dest="group_id",
                help="Indicates the repository name from which artifacts will be searched.")
            fun("--version", action="store", dest="version", default=None,
                help="Provide the version for which the check is requested.")
            fun("--platform", action="store", default=get_default_os(),
                help="Specific platform to check. If not set, current platform is considered.")
            check_group = parser_check.add_argument_group("local-access",
                                                          "Options for controlling local archive checks.")
            fun = check_group.add_argument
            fun("--archive", action="store", dest="archive", default=None,
                help="Indicate the full path of the archive to test.")

            parser_deploy = commands.add_parser("deploy")
            parser_deploy.description = """Deploy the requested artifact. The artifact can be located on the file system 
or on a distant re repository. Backup can be kept for later use."""
            parser_deploy.add_argument("--prefix", action="store", default=os.path.abspath(os.path.curdir),
                                       help="Root path to where the deployment must be done.")
            deploy_group = parser_deploy.add_argument_group("remote-access",
                                                            "Options for controlling remote archive deployment.")
            fun = deploy_group.add_argument
            fun("--artifact", action="store", dest="artifact", default=None,
                help="Set the artifact's name for which the deployment request is about.")
            fun("--url", action="store", default=ARTIFACT_REPOSITORY, dest="url",
                help="Indicates the address for the artifacts repository. Format is USER:PWD@PATH.")
            fun("--groupId", action="store", default=REPOSITORY_NAME, dest="group_id",
                help="Indicates the repository name from which artifacts will be searched.")
            fun("--version", action="store", dest="version", default=None,
                help="Provide the version for which the deployment is requested.")
            fun("--platform", action="store", default=get_default_os(),
                help="Specific platform to deploy. If not set, current platform is considered.")
            deploy_group = parser_deploy.add_argument_group("local-access",
                                                            "Options for controlling local archive deployment.")
            fun = deploy_group.add_argument
            fun("--archive", action="store", dest="archive", default=None,
                help="Indicate the full path of the archive to deploy.")

            self.__args = parser.parse_args()

    def setup(self):
        """
        Validate parameters read by configure.
        The split is needed in order not to interfere with other WAF commands.
        """
        self.dump_version()
        if self.args.command == "list":
            self.set_url_info(self.args)
            print "Listing repository {} from {} ...".format(self.rest_client.group_id, self.rest_client.url)
        elif self.args.command == "info":
            self.set_url_info(self.args)
            self.version_id = self.args.version
            print "Collecting information ..."
        elif self.args.command == "stat":
            self.set_url_info(self.args)
            self.version_id = self.args.version
            print "Collecting statistics ..."
        elif self.args.command == "search":
            self.set_url_info(self.args)
            self.__properties["os"] = self.args.platform
            if self.__properties["os"] == "all":
                self.__properties["os"] = [valid_os for valid_os in self.__artifact_ext]
            for key_value in self.args.properties:
                tmp = key_value.split(":")
                if len(tmp) != 2:
                    print common.NX_Colors.error("Invalid property found : '{}'".format(key_value))
                self.__properties[tmp[0]] = tmp[1]
            print "Searching for artifacts ..."
            print "Filters:"
            for i in self.look_up_properties:
                print " {} -> {}".format(i, self.look_up_properties[i])
        elif self.args.command == "check":
            # Two cases: local and remote. We first check if local is requested
            if self.args.archive is not None:
                self.args.command = "local-check"
                self.current_archive = self.args.archive
                print "Performing integrity check from local artifact ..."
            else:
                if self.args.artifact is None:
                    raise DeployError("No artifact given!", ERROR_BAD_SETUP)
                if self.args.version is None:
                    raise DeployError("No valid version given!", ERROR_BAD_SETUP)
                if self.args.platform == "all":
                    raise DeployError("This operation can be done on multiple platforms!", ERROR_BAD_SETUP)
                self.set_url_info(self.args)
                self.version_id = self.args.version
                print "Performing integrity check from remote artifact ..."
        elif self.args.command == "download":
            if self.args.platform == "all":
                raise DeployError("This operation can be done on multiple platforms!", ERROR_BAD_SETUP)
            self.set_url_info(self.args)
            self.version_id = self.args.version
            self.args.to = os.path.abspath(self.args.to)
            print "Downloading artifact ..."
        elif self.args.command == "deploy":
            # Two cases: local and remote. We first check if local is requested
            if self.args.archive is not None:
                self.args.command = "local-deploy"
                self.current_archive = os.path.abspath(self.args.archive)
                print "Performing deployment from local artifact ..."
            else:
                if self.args.artifact is None:
                    raise DeployError("No artifact given!", ERROR_BAD_SETUP)
                if self.args.version is None:
                    raise DeployError("No valid version given!", ERROR_BAD_SETUP)
                if self.args.platform == "all":
                    self.args.platform = [p for p in self.__artifact_ext]
                else:
                    self.args.platform = [self.args.platform]
                self.set_url_info(self.args)
                self.version_id = self.args.version
                print "Performing deployment from remote artifact ..."

    def process(self):
        """
        Main entry point. Launch the requested action.
        :raise: build.NX_BuilderException.DeployError
        """
        try:
            if self.args.command == "list":
                self.__do_list()
            elif self.args.command == "info":
                self.__do_info()
            elif self.args.command == "list-properties":
                self.__do_list_properties()
            elif self.args.command == "stat":
                self.__do_stat()
            elif self.args.command == "search":
                self.__do_search()
            elif self.args.command in ["local-check", "check"]:
                self.__do_check()
            elif self.args.command == "download":
                remote_path = self.get_path_of_item_from_version(self.args.platform, True)
                self.current_archive = self.__download_archive(remote_path)
                shutil.move(self.current_archive, self.args.to)
            elif self.args.command in ["local-deploy", "deploy"]:
                self.__do_deploy()
        except DeployError:
            raise
        except Exception as local_error:
            msg = "Unknown error raised while running command '{}'.{}   {}"
            raise DeployError(msg.format(self.args.command, os.linesep, local_error), ERROR_UNKNOWN)
