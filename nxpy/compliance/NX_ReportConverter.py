import os
import common.NX_Env
import common.NX_Colors
import common.NX_Process


class ReportConverter(object):
    def __init__(self):
        self.__env = common.NX_Env.Env()
        self.__verbose = False
        self.__rc = 0

    @property
    def return_code(self):
        return self.__rc

    @property
    def verbose(self):
        return self.__verbose

    @verbose.setter
    def verbose(self, value):
        self.__verbose = value

    @property
    def env(self):
        return self.__env


class TestsConverter(ReportConverter):
    def __init__(self, xslt_file_name, input_file, output_file):
        super(TestsConverter, self).__init__()
        self.__xlator = ""
        self.__xslt_file = xslt_file_name
        self.__input_file = input_file
        self.__output_file = output_file

    @property
    def xlator(self):
        return self.__xlator

    @xlator.setter
    def xlator(self, xlator_class_path):
        self.__xlator = xlator_class_path

    @property
    def input(self):
        return self.__input_file

    @input.setter
    def input(self, value):
        self.__input_file = value

    @property
    def output(self):
        return self.__output_file

    @output.setter
    def output(self, value):
        self.__output_file = value

    @property
    def xslt(self):
        return self.__xslt_file

    @xslt.setter
    def xslt(self, value):
        self.__xslt_file = value

    def __compile_xslt(self):
        if not self.xlator:
            if self.verbose:
                common.NX_Colors.print_warning("  -> Skipping translator compilation step.")
            return 0
        java_cc = common.NX_Process.Process("javac")
        java_cc.program_path = self.env.java_bin
        java_cc.cwd = os.path.join(self.env.workspace, "UnitTests")
        java_cc.args = [self.xlator]
        if self.verbose:
            common.NX_Colors.print_info("  - Compiling translator [{FILE}] ...".format(FILE=self.xlator))
        java_cc.run_and_poll(keep_stderr=False, keep_stdout=False, err_prefix="JavaC Error: ")
        if self.verbose:
            if java_cc.last_return_code == 0:
                common.NX_Colors.print_info("  - Translator [{FILE}] compiled successfully".format(FILE=self.xlator))
            else:
                msg = "  - Translator [{}] compilation failed with rc [{}]!"
                msg = msg.format(java_cc.last_return_code, self.xlator)
                common.NX_Colors.print_info(msg)
                msg = "  - Command line was : {} {}".format(java_cc.full_path, " ".join(java_cc.args))
                common.NX_Colors.print_info(msg)
                common.NX_Colors.print_info("  - Execution dir : {}".format(java_cc.cwd))

        self.__rc = java_cc.last_return_code
        return java_cc.last_return_code

    def __translate(self):
        java_cc = common.NX_Process.Process("java")
        java_cc.program_path = self.env.java_bin
        java_cc.cwd = os.path.join(self.env.workspace, "UnitTests")
        java_cc.args = ["-classpath", "data", "Xslt", self.xslt, self.input, self.output]
        if self.verbose:
            common.NX_Colors.print_info("  - Performing test report conversion into JUnit format ...")
            msg = "  - Command line is : {} {}".format(java_cc.full_path, " ".join(java_cc.args))
            common.NX_Colors.print_info(msg)
        java_cc.run_and_poll(err_prefix="Java Error: ", keep_stderr=False, keep_stdout=False)
        if self.verbose:
            if java_cc.last_return_code == 0:
                common.NX_Colors.print_info("  - Conversion performed successfully".format(FILE=self.xlator))
            else:
                msg = "  - Stylesheet [{}] compilation failed with rc [{}]!"
                msg = msg.format(java_cc.last_return_code, self.xlator)
                common.NX_Colors.print_info(msg)
                msg = "  - Command line was : {} {}".format(java_cc.full_path, " ".join(java_cc.args))
                common.NX_Colors.print_info(msg)
                common.NX_Colors.print_info("  - Execution dir : {}".format(java_cc.cwd))
        self.__rc = java_cc.last_return_code
        return java_cc.last_return_code

    def run(self):
        if 0 != self.__compile_xslt():
            raise Exception("  - Translator build has failed. Aborting...")
        if 0 != self.__translate():
            raise Exception("  - Translator cannot transform data! Aborting...")


class ValgrindConverter(ReportConverter):
    def __init__(self, xslt_file_name, input_file, output_file):
        super(ValgrindConverter, self).__init__()
        if self.env.os_name == "Win64":
            raise Exception("Cannot create a valgrind object under Windows")
        self.__xlator = ""
        self.__xslt_file = xslt_file_name
        self.__input_file = input_file
        self.__output_file = output_file

    @property
    def xlator(self):
        return self.__xlator

    @xlator.setter
    def xlator(self, xlator_class_path):
        self.__xlator = xlator_class_path

    @property
    def input(self):
        return self.__input_file

    @input.setter
    def input(self, value):
        self.__input_file = value

    @property
    def output(self):
        return self.__output_file

    @output.setter
    def output(self, value):
        self.__output_file = value

    @property
    def xslt(self):
        return self.__xslt_file

    @xslt.setter
    def xslt(self, value):
        self.__xslt_file = value

    def __compile_xslt(self):
        if not self.xlator:
            if self.verbose:
                common.NX_Colors.print_warning("  -> Skipping translator compilation step.")
            return 0
        java_cc = common.NX_Process.Process("javac")
        java_cc.program_path = self.env.java_bin
        java_cc.cwd = os.path.join(self.env.workspace, "UnitTests")
        java_cc.args = [self.xlator]
        if self.verbose:
            common.NX_Colors.print_info("  - Compiling translator [{FILE}] ...".format(FILE=self.xlator))
        java_cc.run_and_poll(keep_stderr=False, keep_stdout=False, err_prefix="JavaC Error: ")
        if java_cc.last_return_code == 0:
            common.NX_Colors.print_info("  - Translator [{FILE}] compiled successfully".format(FILE=self.xlator))
        else:
            msg = "  - Translator [{}] compilation failed with rc [{}]!"
            msg = msg.format(java_cc.last_return_code, self.xlator)
            common.NX_Colors.print_info(msg)
            msg = "  - Command line was : {} {}".format(java_cc.full_path, " ".join(java_cc.args))
            common.NX_Colors.print_info(msg)
            common.NX_Colors.print_info("  - Execution dir : {}".format(java_cc.cwd))
        self.__rc = java_cc.last_return_code
        return java_cc.last_return_code

    def __translate(self):
        java_cc = common.NX_Process.Process("java")
        java_cc.program_path = self.env.java_bin
        java_cc.cwd = os.path.join(self.env.workspace, "UnitTests")
        java_cc.args = ["-classpath", "data", "Xslt", self.xslt, self.input, self.output]
        if self.verbose:
            common.NX_Colors.print_info("  - Performing conversion to JUnit format ...")
            msg = "  - Command line is : {} {}".format(java_cc.full_path, " ".join(java_cc.args))
            common.NX_Colors.print_info(msg)
        java_cc.run_and_poll(err_prefix="Java Error: ", keep_stderr=False, keep_stdout=False)
        if self.verbose:
            if java_cc.last_return_code == 0:
                common.NX_Colors.print_info("  - Conversion performed successfully".format(FILE=self.xlator))
            else:
                msg = "  - Stylesheet [{}] compilation failed with rc [{}]!"
                msg = msg.format(java_cc.last_return_code, self.xlator)
                common.NX_Colors.print_info(msg)
                msg = "  - Command line was : {} {}".format(java_cc.full_path, " ".join(java_cc.args))
                common.NX_Colors.print_info(msg)
                common.NX_Colors.print_info("  - Execution dir : {}".format(java_cc.cwd))
        self.__rc = java_cc.last_return_code
        return java_cc.last_return_code

    def run(self):
        if 0 != self.__compile_xslt():
            raise Exception("  - Translator build has failed. Aborting...")
        if 0 != self.__translate():
            raise Exception("  - Translator cannot transform data! Aborting...")
