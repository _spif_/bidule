import os
import common.NX_Env
import common.NX_Process
import common.NX_Colors
import common.NX_Utils


class GCOVAdaptor(object):
    def __init__(self, result_root_dir, gcovr_version="last"):
        self.__env = common.NX_Env.Env()
        bin_ = os.path.join(
            os.path.join(self.__env.tools_root_dir, "utils", "gcovr", gcovr_version, "scripts", "gcovr"))
        self.__GCovr = common.NX_Process.Process(bin_)
        self.__rc = 0
        self.__result_root = os.path.abspath(result_root_dir)
        self.__output_filename = ""
        self.__root = ""
        self.__filter_data_in = []
        self.__filter_data_out = []
        self.__filter_gcov_data_in = []
        self.__filter_gcov_data_out = []
        self.__branches = False
        self.__exclude_unreachable_branch = True
        self.__verbose = False
        self.__debug = False
        self.__output_format = "xml"

    def clear_params(self):
        self.coverage_process.Args = []

    @property
    def coverage_process(self):
        return self.__GCovr

    @property
    def env(self):
        return self.__env

    @property
    def verbose(self):
        return self.__verbose

    @verbose.setter
    def verbose(self, flag):
        self.__verbose = flag

    @property
    def debug(self):
        return self.__debug

    @debug.setter
    def debug(self, flag):
        self.__debug = flag

    @property
    def branch_coverage(self):
        return self.__branches

    @branch_coverage.setter
    def branch_coverage(self, flag):
        self.__branches = flag

    @property
    def exclude_unreachable_branches(self):
        return self.__exclude_unreachable_branch

    @exclude_unreachable_branches.setter
    def exclude_unreachable_branches(self, flag):
        self.__exclude_unreachable_branch = flag

    @property
    def output_format(self):
        return self.__output_format

    @output_format.setter
    def output_format(self, output_format):
        self.__output_format = output_format

    @property
    def root(self):
        return self.__root

    @root.setter
    def root(self, path):
        self.__root = path

    @property
    def output_file_name(self):
        return self.__output_filename

    @output_file_name.setter
    def output_file_name(self, file_name):
        self.__output_filename = file_name

    @property
    def data_in(self):
        return self.__filter_data_in

    @data_in.setter
    def data_in(self, values):
        if type(values) is str:
            if values not in self.__filter_data_in:
                self.__filter_data_in.append(values)
        elif type(values) is list:
            for v in values:
                if v not in self.__filter_data_in:
                    self.__filter_data_in.append(v)
        elif values is None:
            self.__filter_data_in = []

    @property
    def data_out(self):
        return self.__filter_data_out

    @data_out.setter
    def data_out(self, values):
        if type(values) is str:
            if values not in self.__filter_data_out:
                self.__filter_data_out.append(values)
        elif type(values) is list:
            for v in values:
                if v not in self.__filter_data_out:
                    self.__filter_data_out.append(v)
        elif values is None:
            self.__filter_data_out = []

    @property
    def gcov_data_out(self):
        return self.__filter_gcov_data_out

    @gcov_data_out.setter
    def gcov_data_out(self, values):
        if type(values) is str:
            if values not in self.__filter_gcov_data_out:
                self.__filter_gcov_data_out.append(values)
        elif type(values) is list:
            for v in values:
                if v not in self.__filter_gcov_data_out:
                    self.__filter_gcov_data_out.append(v)
        elif values is None:
            self.__filter_gcov_data_out = []

    @property
    def gcov_data_in(self):
        return self.__filter_gcov_data_in

    @gcov_data_in.setter
    def gcov_data_in(self, values):
        if type(values) is str:
            if values not in self.__filter_gcov_data_in:
                self.__filter_gcov_data_in.append(values)
        elif type(values) is list:
            for v in values:
                if v not in self.__filter_gcov_data_in:
                    self.__filter_gcov_data_in.append(v)
        elif values is None:
            self.__filter_gcov_data_in = []

    @property
    def return_code(self):
        return self.__rc

    @property
    def result_root_dir(self):
        return self.__result_root

    def prepare(self):
        if self.env.os_name != "linux":
            raise Exception("""Invalid operating system for reporting coverage. 
Only 'linux' operating system is authorized for such a task.""")
        try:
            if self.verbose:
                common.NX_Colors.print_info("Trying to create analysis tree.")
            os.makedirs(self.result_root_dir)
            os.environ["GCOV"] = os.path.join(self.env.tools_root_dir, "compilers", self.env.platform, "GCC",
                                              self.env.compiler_version, "bin", "gcov")
            if self.verbose:
                common.NX_Colors.print_info("Setting up GCOV environment to [{VAR}]".format(VAR=os.environ["GCOV"]))
        except Exception, e:
            raise Exception("While creating directory [{D}] : {R}".format(D=self.result_root_dir, R=e))
        return True

    def compiler_parameters(self):
        if self.debug:
            self.coverage_process.args.append("-v")
        if self.root:
            self.coverage_process.args.append("-r")
            self.coverage_process.args.append(self.root)
        if self.branch_coverage:
            self.coverage_process.args.append("--branches")
        if self.exclude_unreachable_branches:
            self.coverage_process.args.append("--exclude-unreachable-branches")
        if self.output_format == "xml":
            self.coverage_process.args.append("--xml")
        if self.output_format == "html":
            self.coverage_process.args.append("--html")
            self.coverage_process.args.append("--html-details")
        if self.output_file_name:
            self.coverage_process.args.append("-o")
            self.coverage_process.args.append(os.path.join(self.result_root_dir, self.output_file_name))
        if self.data_in:
            for d in self.data_in:
                self.coverage_process.args.append("-f")
                self.coverage_process.args.append(d)
        if self.data_out:
            for d in self.data_out:
                self.coverage_process.args.append("-e")
                self.coverage_process.args.append(d)
        if self.gcov_data_out:
            for d in self.gcov_data_out:
                self.coverage_process.args.append("--gcov-exclude")
                self.coverage_process.args.append(d)
        if self.gcov_data_in:
            for d in self.gcov_data_in:
                self.coverage_process.args.append("--gcov-filter")
                self.coverage_process.args.append(d)
        if self.verbose:
            print "Coverage command line  : {ARGS}".format(ARGS=" ".join(self.coverage_process.args))
            print "Coverage execution dir : {DIR}".format(DIR=self.coverage_process.cwd)


class ComplianceEngine(GCOVAdaptor):
    def __init__(self, result_root_dir, gcovr_version):
        super(ComplianceEngine, self).__init__(result_root_dir, gcovr_version)

    def prepare(self):
        super(ComplianceEngine, self).prepare()
        for subdir in ["coverage_full", "coverage_filtered"]:
            p = os.path.join(self.result_root_dir, subdir)
            common.NX_Utils.erase_fs_entry(p)
            os.makedirs(p)

    def __run_full_coverage(self):
        if self.verbose:
            print "About to compute full coverage..."
        self.output_format = "html"
        self.output_file_name = os.path.join("coverage_full", "index.html")
        self.compiler_parameters()
        self.coverage_process.run(err_prefix="GCOV ERROR : ", keep_stderr=False, keep_stdout=False)
        return self.coverage_process.last_return_code

    def __run_filtered_coverage(self, root_dir):
        if self.verbose:
            print "About to compute strict coverage..."
        valid_modules = set()
        for root, directories, files in os.walk(root_dir):
            for d in directories:
                if d in ["src", "include"]:
                    if root.find("cscppunit") == -1 and root.find("csgoogletest") == -1:
                        valid_modules.add(root)
        self.output_format = "xml"
        self.exclude_unreachable_branches = True
        if not root_dir.endswith(os.pathsep):
            root_dir += os.pathsep
        for mod in valid_modules:
            self.data_in = None
            self.clear_params()
            module_in_ws = mod[len(root_dir):]
            os.makedirs(os.path.join(self.result_root_dir, "coverage_filtered", module_in_ws))
            print " + Computing coverage for module [{MOD}]".format(MOD=module_in_ws)
            self.data_in = os.path.join(".*", module_in_ws, ".*")
            self.output_file_name = os.path.join(self.result_root_dir, "coverage_filtered", module_in_ws, "gcovr.xml")
            self.root = "."
            self.coverage_process.cwd = os.path.join(root_dir, mod, "src")
            self.compiler_parameters()
            self.coverage_process.run(err_prefix="GCOV ERROR : ", keep_stderr=False, keep_stdout=False)
        return self.coverage_process.last_return_code

    def run_analysis(self, run_dir):
        self.coverage_process.cwd = run_dir
        self.exclude_unreachable_branches = True
        self.root = "."
        if self.__run_full_coverage() == 0:
            return self.__run_filtered_coverage(run_dir)
