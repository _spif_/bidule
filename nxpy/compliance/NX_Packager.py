import os
import sys
import glob
import shutil
import argparse
import platform
import time

import build.NX_ManifestReader
import common.NX_Colors
import common.NX_Env
import common.NX_Utils


class Packager:
    def __init__(self, pkg_name, config_dir):
        self.__env = common.NX_Env.Env()
        self.__name = "%s.%s" % (pkg_name, self.env.platform)
        self.__config_dir = config_dir

    @property
    def env(self):
        return self.__env

    @property
    def config_dir(self):
        return self.__config_dir

    @property
    def package_root_dir(self):
        return os.path.abspath(self.package_name)

    @property
    def package_bin_dir(self):
        return os.path.join(self.package_root_dir, "bin")

    @property
    def package_lib_dir(self):
        return os.path.join(self.package_root_dir, "lib")

    @property
    def package_data_dir(self):
        return os.path.join(self.package_root_dir, "data")

    @property
    def package_script_dir(self):
        return os.path.join(self.package_root_dir, "scripts")

    @property
    def package_runtime_dir(self):
        return os.path.join(self.package_lib_dir, "runtime")

    @property
    def package_name(self):
        return self.__name

    def get_condition_mapping(self):
        return {"nx.options.asan": "'%s'" % str(self.env.build_mode == "asan"),
                "nx.options.tsan": "'%s'" % str(self.env.build_mode == "tsan"),
                "nx.options.usan": "'%s'" % str(self.env.build_mode == "usan"),
                "nx.pkg.root": "'%s'" % self.package_root_dir,
                "nx.pkg.libdir": "'%s'" % self.package_lib_dir,
                "nx.pkg.datadir": "'%s'" % self.package_data_dir,
                "nx.pkg.bindir": "'%s'" % self.package_bin_dir,
                "nx.pkg.runtime": "'%s'" % self.package_runtime_dir
                }

    def get_value_mapping(self):
        return {"nx.options.asan": "1" if self.env.build_mode == "asan" else "0",
                "nx.options.tsan": "1" if self.env.build_mode == "tsan" else "0",
                "nx.options.usan": "1" if self.env.build_mode == "usan" else "0",
                "nx.pkg.root": self.package_root_dir,
                "nx.pkg.libdir": self.package_lib_dir,
                "nx.pkg.datadir": self.package_data_dir,
                "nx.pkg.bindir": self.package_bin_dir,
                "nx.pkg.runtime": self.package_runtime_dir
                }

    def make_tree(self):
        try:
            for i in [self.package_root_dir, self.package_bin_dir, self.package_lib_dir, self.package_runtime_dir,
                      self.package_data_dir, self.package_script_dir]:
                os.makedirs(i)
        except Exception, err:
            raise Exception("While making tree : '%s'" % err)

    def add_local_libs(self, workspace, exclusion_trees):
        common.NX_Colors.print_info(" * Validating playground")
        playground_lib = os.path.join(workspace, "playground", "lib", self.env.platform_id_ext)
        playground_bin = os.path.join(workspace, "playground", "bin", self.env.platform_id_ext)
        if not os.path.exists(playground_bin):
            raise Exception("Playground binary directory [%s] does not exists!" % playground_bin)
        if not os.path.exists(playground_lib):
            raise Exception("Playground library directory [%s] does not exists!" % playground_lib)
        common.NX_Colors.print_info(" * Packaging locals binaries")
        entries = glob.glob(os.path.join(playground_bin, "*"))
        for entry in entries:
            link = os.readlink(entry)
            if link.startswith(workspace):
                b_copy = True
                for excl in exclusion_trees:
                    p = os.path.join(workspace, excl)
                    if link.startswith(p):
                        b_copy = False
                if b_copy:
                    try:
                        shutil.copyfile(link, os.path.join(self.package_root_dir, "bin", os.path.basename(link)))
                        print "   * Packaged local binary : {WHAT}".format(
                            WHAT=common.NX_Colors.colored_text("GREEN", os.path.basename(link)))
                    except Exception, err:
                        raise Exception(
                            "Cannot copy local binary [{WHAT}] : {R}".format(WHAT=os.path.basename(link), R=err))
                else:
                    print "   * Skipping local binary : {WHAT}".format(
                        WHAT=common.NX_Colors.colored_text("YELLOW", os.path.basename(link)))
        common.NX_Colors.print_info(" * Packaging locals libraries")
        entries = glob.glob(os.path.join(playground_lib, "*"))
        for entry in entries:
            link = os.readlink(entry)
            if link.startswith(workspace):
                b_copy = True
                for excl in exclusion_trees:
                    p = os.path.join(workspace, excl)
                    if link.startswith(p):
                        b_copy = False
                if b_copy:
                    try:
                        shutil.copyfile(link, os.path.join(self.package_root_dir, "lib", os.path.basename(link)))
                        print "   * Packaged local library : {WHAT}".format(
                            WHAT=common.NX_Colors.colored_text("GREEN", os.path.basename(link)))
                    except Exception, err:
                        raise Exception(
                            "Cannot copy local library [{WHAT}] : {R}".format(WHAT=os.path.basename(link), R=err))
                else:
                    print "   * Skipping local library : {WHAT}".format(
                        WHAT=common.NX_Colors.colored_text("YELLOW", os.path.basename(link)))

    def add_externals(self, external_list):
        common.NX_Colors.print_info(" * Packaging externals")
        for ext in external_list:
            tmp = ext.split(":", 1)
            name, version = tmp[0], tmp[1]
            emr = build.NX_ManifestReader.ExternalManifestReader(os.path.join(self.config_dir, name + ".xml"), name,
                                                                 version)
            emr.extend_mapping(self.get_condition_mapping(), self.get_value_mapping())
            emr.read()
            for entry in emr.manifest.package:
                try:
                    cf, cd = emr.manifest.package[entry]
                    shutil.copyfile(cf, cd)
                    s = "   * Packaged from [{}] : {}"
                    print s.format("%s-%s" % (name, version), common.NX_Colors.colored_text("GREEN",
                                                                                            os.path.basename(cf)))
                except Exception, err:
                    cf, cd = emr.manifest.package[entry]
                    raise Exception("Cannot copy external entry [%s] from [%s-%s]: '%s'" % (cf, name, version, err))
            for entry in emr.manifest.package_links:
                lef, lea, lei = emr.manifest.package_links[entry]
                try:
                    common.NX_Utils.sym_link(lef, lea)
                    s = "   * Linking from [{D}-{V}] : [{N}] as [{T}] "
                    print s.format(N=common.NX_Colors.colored_text("GREEN", os.path.basename(os.path.abspath(lef))),
                                   T=common.NX_Colors.colored_text("CYAN", os.path.basename(os.path.abspath(lea))),
                                   D=emr.manifest.name, V=emr.manifest.version)
                except Exception, err:
                    s = "Cannot link external entry [{N}] as [{T}] from [{D}]: '{R}'"
                    raise Exception(s.format(N=os.path.basename(os.path.abspath(lef)),
                                             T=os.path.basename(os.path.abspath(lea)),
                                             R=str(err), D=emr.manifest.name))
                try:
                    shutil.move(lea, os.path.join(os.path.abspath(lei), lea))
                    s = "   * Pushing link into package : [{TGT}] "
                    print s.format(TGT=common.NX_Colors.colored_text("CYAN", os.path.basename(os.path.abspath(lea))))
                except Exception, err:
                    s = "Cannot push link for external entry [{NAME}] as [{TGT}] into package : '{R}'"
                    raise Exception(
                        s.format(NAME=os.path.basename(os.path.abspath(lef)),
                                 TGT=os.path.basename(os.path.abspath(lea)),
                                 R=str(err)))

    def add_compiler_dependencies(self):
        common.NX_Colors.print_info(" * Packaging compiler")
        cmr = build.NX_ManifestReader.CompilerManifestReader(os.path.join(self.config_dir, "compilers.xml"))
        cmr.extend_mapping(self.get_condition_mapping(), self.get_value_mapping())
        cmr.read()
        for entry in cmr.manifest.package:
            try:
                cf, cd = cmr.manifest.package[entry]
                shutil.copyfile(cf, cd)
                s = "   * Packaged from [{NAME}] version [{VERSION}] : '{WHAT}'"
                print s.format(NAME=self.env.compiler_type, VERSION=self.env.compiler_version,
                               WHAT=common.NX_Colors.colored_text("GREEN", os.path.basename(cf)))
            except Exception, err:
                cf, cd = cmr.manifest.package[entry]
                s = "Cannot copy compiler entry [{ENTRY}] from compiler [{NAME}] version [{VERSION}]: '{R}'"
                raise Exception(s.format(ENTRY=os.path.basename(cf), NAME=self.env.compiler_type,
                                         VERSION=self.env.compiler_version, R=err))
        for entry in cmr.manifest.package_links:
            lcf, lca, lci = cmr.manifest.package_links[entry]
            try:
                common.NX_Utils.sym_link(lcf, lca)
                s = "   * Linking from [{C}] version [{V}] : [{N}] as [{T}] "
                print s.format(N=common.NX_Colors.colored_text("GREEN", os.path.basename(os.path.abspath(lcf))),
                               T=common.NX_Colors.colored_text("CYAN", os.path.basename(os.path.abspath(lca))),
                               C=self.env.compiler_type, V=self.env.compiler_version)
            except Exception, err:
                s = "Cannot link compiler entry [{NAME}] as [{TGT}] : '{R}'"
                raise Exception(s.format(NAME=os.path.basename(os.path.abspath(lcf)),
                                         TGT=os.path.basename(os.path.abspath(lca)), R=str(err)))
            try:
                shutil.move(lca, os.path.join(os.path.abspath(lci), lca))
                s = "   * Pushing link into package : [{TGT}]"
                print s.format(TGT=common.NX_Colors.colored_text("CYAN", os.path.basename(os.path.abspath(lca))))
            except Exception, err:
                s = "Cannot push link for compiler entry [{NAME}] as [{TGT}] into package : '{R}'"
                raise Exception(s.format(NAME=os.path.basename(os.path.abspath(lcf)),
                                         TGT=os.path.basename(os.path.abspath(lca)),
                                         R=str(err)))

    def __write_nx_env_script(self):
        print "   * Writing script : {NAME}".format(NAME=common.NX_Colors.colored_text("GREEN", "nx_env.sh"))
        contents = []
        for k, v in zip(["NX_TOOLS", "NX_COMPILER_TYPE", "NX_COMPILER_VERSION", "NX_BUILD_BITS", "NX_BUILD_MODE",
                         "NX_PLATFORM"],
                        [self.env.tools_root_dir, self.env.compiler_type, self.env.compiler_version,
                         self.env.build_bits, self.env.build_mode, self.env.platform]):
            contents.append("export {K}={V}".format(K=k, V=v))
        contents.insert(0, "# You can source the file to setup your environment ready for a build")
        contents.insert(0, "# ")
        contents.insert(0, "# Host was [%s]" % platform.node())
        contents.insert(0, "# Packaged on %s" % time.asctime())
        try:
            with open(os.path.join(self.package_script_dir, "nx_env.sh"), "w") as f:
                f.write(os.linesep.join(contents))
        except IOError as expt:
            raise Exception("While writing [nx_env.sh] : %s  %s" % (os.linesep, expt))

    def generate_scripts(self, ccl=True):
        common.NX_Colors.print_info(" * Generating scripts")
        self.__write_nx_env_script()
        print "   * Writing manifest : {NAME}".format(NAME=common.NX_Colors.colored_text("GREEN", "manifest.xml"))
        shutil.copyfile(os.path.join(self.config_dir, "Packaging", "package.manifest.xml"),
                        os.path.join(self.package_root_dir, "manifest.xml"))
        if ccl:
            shutil.copyfile(os.path.join(self.config_dir, "Packaging", "package.{E}.ccl".format(E=self.env.os_name)),
                            os.path.join(self.package_script_dir, "ccl"))
        else:
            print "   * Skipping script : {NAME}".format(NAME=common.NX_Colors.colored_text("YELLOW", "ccl"))

    def finalize(self, no_del_root, archive_format):
        common.NX_Colors.print_info(" * Making archive")
        if archive_format != "none":
            shutil.make_archive(self.package_root_dir, archive_format, os.path.dirname(self.package_root_dir),
                                self.package_name)
        if not no_del_root:
            shutil.rmtree(self.package_root_dir)


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser("binaries packager",
                                         description="Packaging utility for NeoXam Compliance' binaries.")
        parser.add_argument("--pkg-name", dest="pkg_name", action="store", default="",
                            help="Basename of the package to create, without extension or platform specificities.")
        parser.add_argument("--config-dir", dest="config_dir", action="store", default="",
                            help="Configuration directory to read settings from.")
        parser.add_argument("--externals", nargs="*", default=[],
                            help="List of external dependencies to read, format is <name>:<version>.")
        parser.add_argument("--workspace", action="store", default="",
                            help="Workspace from which libraries and binaries must be taken from")
        parser.add_argument("--no-del-root", dest="no_del_root", action="store_true",
                            help="Delete non-compressed package when done.")
        parser.add_argument("--exclude-tree", dest="exclusion_tree", nargs="*", default=[],
                            help="Do not package elements within <workspace>/<subtree>.")
        parser.add_argument("--archive-format", dest="archive_format", choices=["zip", "gztar", "bztar", "tar", "none"],
                            default="bztar",
                            help="Do not package elements within <workspace>/<subtree>.")
        parser.add_argument("--no-ccl-scripts", dest="no_ccl_scripts", action="store_true", default="false",
                            help="Do not generate ccl script. By default the generation is active.")
        args = parser.parse_args()

        if not args.pkg_name:
            raise Exception("No package basename has been provided!")
        if not args.config_dir:
            raise Exception("No configuration directory has been provided!")
        if not os.path.exists(args.config_dir):
            raise Exception("The configuration directory [%s] does not exists!" % args.config_dir)
        if not args.workspace:
            raise Exception("No workspace directory has been provided!")
        if not os.path.exists(args.workspace):
            raise Exception("The workspace directory [%s] does not exists!" % args.workspace)

        pkg = Packager(args.pkg_name, args.config_dir)
        pkg.make_tree()
        pkg.add_compiler_dependencies()
        pkg.add_externals(args.externals)
        pkg.add_local_libs(args.workspace, args.exclusion_tree)
        pkg.generate_scripts(args.no_ccl_scripts)
        pkg.finalize(args.no_del_root, args.archive_format)
    except Exception, e:
        print e
        sys.exit(1)
    sys.exit(0)
