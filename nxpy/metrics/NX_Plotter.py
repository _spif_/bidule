#!/usr/bin/python
# coding: utf-8
import numpy as np
import NX_MetricsException

# Plotting library (use Agg backend)
import matplotlib as mpl

mpl.use("Agg")
import matplotlib.pyplot as plt

mpl.rcParams['lines.color'] = "C0"
mpl.rcParams['lines.linewidth'] = 1
mpl.rcParams['grid.linestyle'] = ":"  # dotted equivalent
mpl.rcParams['grid.linewidth'] = 0.5


class Plot(object):
    """
    Wrapper for matplotlib figure object following RAII idiom.

    Variables:
       fig: matplotlib figure  -- Matplotlib figure holder
       width: integer          -- Image's width in px
       height: integer         -- Image's height in px
       dpi: integer            -- Image's dpi
    """

    def __init__(self, fig, width=None, height=None, dpi=None):
        """
        Construct a plot instance.

        Keyword arguments:
        :param fig    -- matplotlib figure. We take ownership to the figure
        :param width  -- plot width in px
        :param height -- plot height in px
        :param dpi    -- plot dpi
        """
        if not fig:
            NX_MetricsException.InputError("fig", "figure is empty")

        # Convert width, height px to inches
        # Matplotlib work with physical sizes and dpi. In order to generate
        # an image of a wanted size in pixel, a conversion from pixels to inches is necessary
        # By default, dpi = 1 to avoid computation.
        if dpi:
            width = width / dpi
            height = height / dpi

        # Setup internal variables
        self.__fig = fig
        self.__width = width if width else 200
        self.__height = height if height else 200
        self.__dpi = dpi if dpi else 100

    def __enter__(self):
        return self

    def __del__(self):
        """
        Destroy a plot instance. Close the figure held.
        """
        plt.close(self.__fig)

    def __exit__(self, my_type, value, traceback):
        return True

    def save(self, output):
        """
        Save the current figure in a file (.png)

        Keyword arguments:
        :param output -- file were the image will be writen
        """
        self.__fig.set_dpi(self.__dpi)
        self.__fig.set_size_inches(self.__width, self.__height, forward=True)
        self.__fig.savefig(output, dpi=self.__dpi)


class Plotter(object):
    """
    Object responsible for plotting.

    Variables:
       Title: string     -- title of the plot. Set to 200 by default.
       Width: integer    -- Plot's width in px. Set to 200 by default.
       Height: integer   -- Plot's height in px. Set to 100 by default.
    """

    def __init__(self, title, width, height, dpi=None):
        """
        Construct a plotter instance

        Keyword arguments:
        :param title  -- Title of the plot
        :param width  -- Plot's width in px
        :param height -- Plot's height in px
        :param dpi    -- Plot's dpi
        """
        self.__title = title
        self.__width = width if width else 200
        self.__height = height if height else 200
        self.__dpi = dpi if dpi else 100

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, new_title):
        self.__title = new_title if new_title else self.__title

    @property
    def width(self):
        return self.__width

    @width.setter
    def width(self, w):
        if int == type(w) or float == type(w):
            self.__width = w

    @property
    def height(self):
        return self.__height

    @height.setter
    def height(self, h):
        if int == type(h) or float == type(h):
            self.__height = h

    def __empty(self, txt=None):
        """
        Plot an empty figure

        Keyword arguments:
        :param txt -- text to plot. Can be used for displaying an error message.
        """
        txt = txt if txt else "Empty plot"

        fig, ax = plt.subplots()
        ax.set_title(self.__title)
        ax.axis([0, 10, 0, 10])
        ax.text(5, 5, txt)

        return Plot(fig, width=self.__width, height=self.__height, dpi=self.__dpi)

    def __cdf(self, data, column, xlabel=None, ylabel=None):
        """
        Plot an empirical Cumulative Distribution Function.
        For more details : https://en.wikipedia.org/wiki/Cumulative_distribution_function

        Keyword arguments:
        :param data   -- data to use
        :param column -- column of the dataframe to use
        :param xlabel -- x-axis label
        :param ylabel -- y-axis label
        """
        xlabel = xlabel if xlabel else ""
        ylabel = ylabel if ylabel else "Probability"
        prefix_title = "Cumulative Distribution Function "

        # Processing data
        data = data.df.loc[:, column]
        x, y = np.unique(data, return_counts=True)
        y = y.astype(np.float32).cumsum()
        y = y / max(y)

        # Plot
        fig, ax = plt.subplots()
        ax.plot(x, y)

        # Cosmetics
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_title(prefix_title + " - " + self.__title)
        ax.grid(linestyle="dotted", antialiased=True)
        ax.xaxis.get_major_formatter().set_useOffset(False)

        return Plot(fig, width=self.__width, height=self.__height, dpi=self.__dpi)

    def __timeseries(self, data, columns, xlabel=None, ylabel=None):
        """
        Plot a time-series

        Keyword arguments:
        :param data    -- data to use
        :param columns -- column of the dataframe to use
        :param xlabel  -- x-axis label
        :param ylabel  -- y-axis label
        """
        # Plot related variables
        xlabel = xlabel if xlabel else "Time period"
        ylabel = ylabel if ylabel else ""
        prefix_title = "Time-series"

        # Data
        period = data.period
        dates = data.df.index

        datas = data.df.loc[:, columns]

        if not data.is_raw:
            datas_min = data.df.loc[:, columns + "_min"]
            datas_max = data.df.loc[:, columns + "_max"]

            # Remove aberant data
            if len(datas_min) >= 15:
                datas_min.replace(min(datas_min), np.nan, inplace=True)
                datas_min.replace(min(datas_min), np.nan, inplace=True)
                datas_min.replace(min(datas_min), np.nan, inplace=True)

                datas_max.replace(max(datas_max), np.nan, inplace=True)
                datas_max.replace(max(datas_max), np.nan, inplace=True)
                datas_max.replace(max(datas_max), np.nan, inplace=True)

        # print(data)

        # For maintainability
        day, week, month = 1, 6, 30

        # Setup locator depending of the period to plot
        if period < day:
            x_major_locator = mpl.dates.HourLocator()
            x_minor_locator = mpl.dates.MinuteLocator()
            x_format = mpl.dates.DateFormatter("%H:%M")
        elif period <= week:
            x_major_locator = mpl.dates.DayLocator()
            #    x_minor_locator = mpl.dates.HourLocator(interval=4)
            x_minor_locator = mpl.dates.DayLocator()
            x_format = mpl.dates.DateFormatter("%x")
        elif period <= month:
            x_major_locator = mpl.dates.WeekdayLocator()
            x_minor_locator = mpl.dates.DayLocator()
            x_format = mpl.dates.DateFormatter("%x")
        else:
            # Almost by default case
            x_major_locator = mpl.dates.MonthLocator()
            x_minor_locator = mpl.dates.WeekdayLocator()
            x_format = mpl.dates.DateFormatter("%B, %Y")

        # timeFormatter = mpl.ticker.FuncFormatter(timedeltaTicks)

        # Compute trend
        # coef, res, _, _, _ = np.polyfit(range(len(dates)), data, 1, full=True)

        # Plot data + trend
        fig, ax = plt.subplots()
        ax.plot_date(dates, datas, '-')

        # For aggregate data, plot min and max
        if not data.is_raw:
            min_mask = np.isfinite(datas_min)
            ax.plot_date(dates[min_mask], datas_min[min_mask], '-')

            max_mask = np.isfinite(datas_max)
            ax.plot_date(dates[max_mask], datas_max[max_mask], '-')

        # ax.fill_between(dates, datas, datas_max, facecolor="blue", interpolate=True)
        #    ax.fill_between(dates, datas, datas_min, facecolor="blue", interpolate=True)

        # ax.plot_date(dates, [coef[0]*x+coef[1] for x in range(len(dates))], '-')

        # Cosmetics
        # Legend
        patches = []
        if not data.is_raw:
            patches.append(mpl.patches.Patch(color="blue", label="Aggregate"))
            patches.append(mpl.patches.Patch(color="green", label="Maximum"))
            patches.append(mpl.patches.Patch(color="orange", label="Minimum"))
        else:
            patches.append(mpl.patches.Patch(color="blue", label="Data"))
        plt.legend(handles=patches)

        # Format ticks
        ax.xaxis.set_major_locator(x_major_locator)
        ax.xaxis.set_minor_locator(x_minor_locator)
        ax.xaxis.set_major_formatter(x_format)
        # ax.yaxis.set_major_formatter(timeFormatter)
        ax.yaxis.get_major_formatter().set_useOffset(False)
        ax.autoscale_view()

        ax.fmt_xdata = x_format
        ax.grid(linestyle="dotted", antialiased=True)
        ax.set_title(prefix_title + " - " + self.__title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

        fig.autofmt_xdate()
        return Plot(fig, width=self.__width, height=self.__height, dpi=self.__dpi)

    def __bar_time_series(self, data, columns, xlabel=None, ylabel=None):
        """
        Plot a bar chart time-series of the given columns

        Keyword arguments:
        :param data      -- data to use
        :param columns   -- column of the dataframe to use
        :param xlabel    -- x-axis label
        :param ylabel    -- y-axis label
        """
        # Plot related variable
        xlabel = xlabel if xlabel else "Time period"
        ylabel = ylabel if ylabel else ""
        prefix_title = "Bar chart"

        # Data
        data = data.df.loc[:, columns]

        # Plot
        fig, ax = plt.subplots()
        ax = data.plot(kind="bar")

        # Cosmetics
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)
        ax.set_title(prefix_title + " - " + self.__title)

        return Plot(fig, width=self.__width, height=self.__height, dpi=self.__dpi)

    def plot(self, kind, data, columns, xlabel=None, ylabel=None):
        """
        Plot!

        Keyword arguments:
        :param kind -- type of the plot. Following accepted :
           * "ts":  Timeseries
           * "cdf": Cumulated Distribution Function
           * "bar": Bar charts
        :param columns -- columns to use
        :param xlabel  -- x-axis label
        :param ylabel  -- y-axis label
        """
        kind = kind.lower()
        kind_map = {
            "ts": self.__timeseries,
            "cdf": self.__cdf,
            "bar": self.__bar_time_series
        }

        if kind not in kind_map:
            raise NX_MetricsException.InputError("kind", "plot " + kind + " is not supported")

        try:
            return kind_map[kind](data, columns, xlabel, ylabel)
        except KeyError:
            return self.__empty()
