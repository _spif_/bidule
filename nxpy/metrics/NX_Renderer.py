#!/usr/bin/python
# coding: utf-8
import os
# import shutil
from string import Template
from distutils.dir_util import copy_tree

# NXPy related
import NX_Plotter
import NX_MetricsException
import common.NX_Utils as NX_Utils


class Renderer(object):
    """
    Base class for renderer object.
    """

    @staticmethod
    def supported_rendering():
        return ["html"]

    @staticmethod
    def render(dashboard):
        my_type = dashboard.cfg["renderer"]["type"].lower()
        map_type = {
            "html": HtmlRenderer
        }

        if my_type not in map_type:
            raise NX_MetricsException.InputError("type", "renderer type not supported")
        r = map_type[my_type](dashboard)
        r.render()


class HtmlRenderer(Renderer):
    """
    Renderer for html

    Variables:
       dashboard: NX_Dashboard -- dashboard instance
       input_dir: os.path      -- input directory path
       output_dir: os.path     -- output directory path
       template_dir: os.path   -- template directory path
       plotter: NX_Plotter     -- plotter instance
    """

    def __init__(self, dashboard):
        """
        Construct an html renderer instance.

        Keyword argument:
        :param dashboard -- dashboard to render
        """
        input_dir = dashboard.cfg["renderer"]["input_dir"]
        output_dir = os.path.join("www")
        if dashboard.cfg["renderer"]["output_dir"] is not None:
            output_dir = dashboard.cfg["renderer"]["output_dir"]

        # Setup HTML Environment
        # Check existance of necessary directories
        vendors_input_dir = os.path.join(input_dir, "Vendors")
        vendors_output_dir = os.path.join(output_dir, "Vendors")
        template_input_dir = os.path.join(input_dir, "Template")
        resources_input_dir = os.path.join(input_dir, "Resources")
        resources_output_dir = os.path.join(output_dir, "Resources")

        if not os.path.isdir(vendors_input_dir):
            raise NX_MetricsException.RendererError("No vendors directory found")
        if not os.path.isdir(template_input_dir):
            raise NX_MetricsException.RendererError("No template directory found")
        if not os.path.isdir(resources_input_dir):
            raise NX_MetricsException.RendererError("No resources directory found")

        # Create / overwrite output directory
        NX_Utils.erase_fs_entry(output_dir)
        os.makedirs(vendors_output_dir)
        os.makedirs(resources_output_dir)

        # Copy vendros & resources
        copy_tree(vendors_input_dir, vendors_output_dir)
        copy_tree(resources_input_dir, resources_output_dir)

        # Setup internal variables
        self.__dashboard = dashboard
        self.__input_dir = input_dir
        self.__output_dir = output_dir
        self.__template_dir = template_input_dir

        default_title = str(dashboard.date_start)
        if dashboard.date_end and dashboard.date_start != dashboard.date_end:
            default_title = default_title + " to " + str(dashboard.date_end)

        self.__plotter = NX_Plotter.Plotter(title=default_title, width=1100, height=700)

    def render(self):
        """
        Render a dashboard
        """
        view_to_render = self.__dashboard.metrics_view

        # Create all views
        monitored = []
        n_views = len(view_to_render)
        for idx, view in enumerate(view_to_render):
            if self.__dashboard.verbose:
                NX_Utils.progress_bar(idx + 1, n_views, suffix="[OK] Render dashboard", msg="Rendering...")

            output_file = self.__render_view(view)
            monitored.append({"link": output_file, "name": view.data.name,
                              "description": view.data.description if view.data.description else ""})

        # Create index
        self.__render_html_page("index", "dashboard.template", {
            "table_rows": '\n'.join(
                ["<tr><td><a href=\"{link}\">{name}</a></td><td>{description}</td></tr>".format(**m) for m in
                 monitored])
        })

    def __render_layout(self, layout, data, prefix=None):
        """
        Render a layout
        """
        img_path = os.path.join(self.__output_dir, "Resources", "img")
        plot_suffix = str(self.__dashboard.date_start)
        if self.__dashboard.date_end:
            plot_suffix += " to " + str(self.__dashboard.date_end)

        res = dict(plot=[])

        # Render each boxes of the layout
        for idx, box in enumerate(layout.boxes):
            if "plot" in box:
                plot_file = prefix + '_' + box["plot"]["kind"] + '_' + str(idx) + ".png"
                with self.__plotter.plot(data=data, **box["plot"]) as plot:
                    plot.save(os.path.join(img_path, plot_file))
                res["plot"].append(plot_file)

            if "summary" in box:
                unit = box["summary"]["unit"] if box["summary"]["unit"] else ""
                col = data.df.loc[:, box["summary"]["data"]]
                res["summary"] = {
                    "n_elements": len(col),
                    "mean_value": "%.2f " % col.mean() + unit,
                    "std_value": "%.2f " % col.std() + unit,
                    "min_value": "%.2f " % col.min() + unit,
                    "max_value": "%.2f " % col.max() + unit,
                }

        return res

    def __render_view(self, view):
        """
        Render a view
        """
        rendered_layout = self.__render_layout(view.layout, prefix=view.title, data=view.data)

        # Value to substitute (format to HTML)
        substitute_html = dict(rendered_layout["summary"])
        substitute_html.update({
            "metricName": view.data.name,
            "page_title": self.__dashboard.title + " - " + view.title,
            "rendered_box": '\n'.join(
                ["<div class=\"box\"><img src=\"Resources/img/{img_file}\"/></div>".format(img_file=e) for e in
                 rendered_layout["plot"]])
        })

        # Render the page
        return self.__render_html_page(view.title, view.template, substitute_html)

    def __render_html_page(self, output, template, rdr_dict):
        """
        Render an html page using the given template.

        Keyword arguments:
        :param output     -- output html page (without extension)
        :param template   -- name of the template to use
        :param rdr_dict       -- dictionary used to substitute value
        """
        if not template:
            raise NX_MetricsException.InputError("template", "argument is empty")

        output += ".html"
        try:
            with open(os.path.join(self.__template_dir, template), 'r') as f:
                html_template = Template(f.read())

            # Substitute & Write the page
            html_content = html_template.substitute(rdr_dict)
            with open(os.path.join(self.__output_dir, output), 'w') as html_page:
                html_page.write(html_content)
            return output
        except IOError as e:
            raise NX_MetricsException.RendererError("IOError" + e.message)
        except (KeyError, TypeError) as e:
            raise NX_MetricsException.RendererError(e.message)
