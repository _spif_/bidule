#!/usr/bin/python
# coding: utf-8
import NX_MetricsException

# Scientific libraries
import numpy as np
import pandas as pd

import time
import datetime


class Data(object):
    """
    This class manage data such as measure on a specific metric.

    Variables:
        name: string                -- name of the metric
        type: string                -- type of the metric
        description: string         -- brief description of the purpose of the metric
        date_range: datetime tuple  --
        df: pandas dataframe        -- bi-dimensional array which store measures
    """

    def __init__(self, con, metric_name, date_debut, date_end, aggregate=True, f_agg=np.mean):
        """
        Construct a data instance. From the given connection it retrieve all data stored
        for a metric at a given time point / date range.

        Keyword arguments:
        :param con         -- A database connection instance. It must be compatible with pandas.read_sql_query
        :param metric_name -- Name of the metric
        :param date_debut  -- debut date
        :param date_end    -- end date
        :param aggregate   -- If set to true, it aggregate depending of the date range
        :param f_agg       -- Aggregation function to use. By default mean is used.
        """
        # Check input arguments
        if not metric_name:
            raise NX_MetricsException.InputError("metric_name", "argument is empty")

        # Convert date string to timestamp
        date_end += datetime.timedelta(hours=23, minutes=59, seconds=59)

        timestamp_subquery = ["timestamp <= {}".format(int(time.mktime(date_end.timetuple())) * 1e9),
                              "timestamp >= {}".format(int(time.mktime(date_debut.timetuple())) * 1e9)]

        # Reorder
        # if date_range[0] > date_range[1]:
        # date_range = date_range[::-1]
        # date_range["start"], date_range["end"] = date_range["end"], date_range[]

        # Check if any infos is available for the corresponding metric
        metric_info = con.execute(
            "SELECT type, description FROM RT_METRIC_INFO WHERE Name='{}'".format(metric_name)).fetchone()
        if not metric_info:
            raise NX_MetricsException.MeasuresError("No informations found for metric {}.".format(metric_name))

        # Construct the query
        subquery = ""
        if timestamp_subquery:
            subquery = "AND {}".format(" AND ".join(timestamp_subquery))
        query = "SELECT * FROM RT_METRIC_{MType} WHERE name='{MName}' {Subquery}".format(MType=metric_info[0],
                                                                                         MName=metric_name,
                                                                                         Subquery=subquery)

        # Retrieve all measures
        df = pd.read_sql_query(query, con)

        # Postprocess df :
        #   1. Add datetime column and sort (using mergesort 'cause it's stable)
        #   2. Drop unecessary column (ID, Name, Timestamp)
        df = df.assign(datetime=np.sort(pd.to_datetime(df.timestamp, unit="ns", errors="coerce"), kind="mergesort"))
        df.set_index("datetime", inplace=True)
        df.drop("id", axis=1, inplace=True)
        df.drop("name", axis=1, inplace=True)
        df.drop("timestamp", axis=1, inplace=True)

        period = (max(df.index) - min(df.index)).days
        if aggregate:
            """
            Resample the dataframe depending of the date range.
            Metrics library can take multiple measure in less than a second, leading to a chunk of measure which
            can be ... in this short amount of time.
            For visualization purpose such precision is not necessary, that is where resampling can be usefull.
            """
            if period <= 0:
                freq = "1T"
            elif period <= 7:
                freq = "1D"
            elif period <= 30:
                freq = "1D"
            else:
                freq = "1D"
            df_agg = df.resample(freq).aggregate(f_agg)

            df_min = df.resample(freq).aggregate(np.amin)
            df_min = df_min.add_suffix("_min")

            df_max = df.resample(freq).aggregate(np.amax)
            df_max = df_max.add_suffix("_max")

            df = pd.concat([df_min, df_agg, df_max], axis=1, join="inner", join_axes=[df_min.index])
            df.dropna(axis=0, how="any", inplace=True)

        # Assign internal variables
        self.__df = df
        self.__period = period
        self.__name = metric_name
        self.__type = metric_info[0]
        self.__description = metric_info[1]
        self.__date_range = (date_debut, date_end)
        self.__raw_data = (aggregate == 0)

    @property
    def name(self):
        return self.__name

    @property
    def type(self):
        return self.__type

    @property
    def description(self):
        return self.__description

    @property
    def period(self):
        return self.__period

    @property
    def is_raw(self):
        return self.__raw_data

    @property
    def df(self):
        return self.__df

    def __str__(self):
        """
        String representation of a data instance
        """
        return "Measures for metric : {MName}\nSummary : \n{Summary}".format(MName=self.__name,
                                                                             Summary=self.__df.describe())

        # TODO : Subset method
