#!/usr/bin/python
# coding: utf-8


class MetricsError(Exception):
    """
    Exception base class for all NX_Metrics package exception.
    """

    def __init__(self, *args, **kwargs):
        super(MetricsError, self).__init__(self, *args, **kwargs)


class InputError(MetricsError):
    """
    Exception raised for invalidate input arguments

    Attributes:
        argument -- The invalid argument
        reason   -- reason of the error
    """

    def __init__(self, arg, reason):
        formated_msg = "Input Error, argument {arg} : {reason}".format(arg=arg, reason=reason)
        super(InputError, self).__init__(formated_msg, None)

        self.__reason = reason
        self.__argument = arg


class MeasuresError(MetricsError):
    """
    Exception raised for measures related errors

    Attributes:
        msg -- explanation of the error
    """

    def __init__(self, msg):
        formated_msg = "Measures Error : %s" % msg
        super(MeasuresError, self).__init__(formated_msg, None)

        self.__msg = msg


class RendererError(MetricsError):
    """
    Exception raised by renderer module

    Attributes:
        msg -- explanation of the error
    """

    def __init__(self, msg):
        formated_msg = "Renderer Error : %s" % msg
        super(RendererError, self).__init__(formated_msg, None)

        self.__msg = msg


class DashboardError(MetricsError):
    """
    Exception raised by manager module

    Attributes:
        msg -- explanation of the error
    """

    def __init__(self, msg):
        formated_msg = "Manager Error : %s" % msg
        super(DashboardError, self).__init__(formated_msg, None)

        self.__msg = msg
