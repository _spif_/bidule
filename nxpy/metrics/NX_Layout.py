#!/usr/bin/python
# coding: utf-8


class Layout(object):
    """
    A layout is a set of boxes with their own properties.

    Variables:
       boxes: list -- list of boxes
    """

    def __init__(self):
        """
        Construct a layout instance
        """
        self.__boxes = []

    @property
    def boxes(self):
        return self.__boxes

    def insert_plot(self, kind, column, xlabel=None, ylabel=None):
        """
        Insert a plot box to the current layout.

        Keyword arguments:
        :param kind       -- Kind of plot
        :param column     -- Column of the dataframe to use to plot
        :param xlabel     -- x-axis label
        :param ylabel     -- y-axis label
        """
        self.__boxes.append({
            "plot": {
                "kind": kind,
                "columns": column,
                "xlabel": xlabel,
                "ylabel": ylabel
            }
        })

    def insert_summary(self, column, unit=None):
        """
        Insert a summary box to the current layout.

        Keyword arguments:
        :param column     -- Column of the dataframe to use
        :param unit       -- Unit of the value
        """
        self.__boxes.append({
            "summary": {
                "data": column,
                "unit": unit
            }
        })
