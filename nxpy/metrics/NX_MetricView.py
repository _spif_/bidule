#!/usr/bin/python
# coding: utf-8
import NX_Data
import NX_Layout
import NX_MetricsException


class MetricView(object):
    """
    MetricView contains everything necessary for generating the graphical interface of a metric.

    Variables:
       title: string      -- Title of the view
       template: string   -- Template of the graphical interface
       data: NX_Data      -- Contains the data to be displayed
       layout: NX_Layout  -- Describe how data should be displayed
    """

    def __init__(self, con, metric_name, title, date_range, fmt=None, layout=None):
        """
        Construct a metric view instance

        Keyword arguments:
        :param con              -- database connnection object
        :param metric_name      -- ID of the metric
        :param date_range       -- Range of date
        :param fmt           -- Date format used to specified the range.
        :param layout           -- Layout to use.
        """
        # Check input arguments
        if not metric_name:
            NX_MetricsException.InputError("title", "title is empty")

        # Assign internal variables
        self.__title = title
        self.__template = "metric_view.template"
        self.__data = NX_Data.Data(con, metric_name, date_range[0], date_range[1], fmt)
        self.__layout = default_layout(a_type=self.__data.type) if layout is None else layout

    @property
    def layout(self):
        return self.__layout

    @property
    def data(self):
        return self.__data

    @property
    def template(self):
        return self.__template

    @property
    def title(self):
        return self.__title


def default_layout(a_type):
    """
    Generate a default layout for each type of metric.

    Keyword arguments:
    :param a_type  -- type of data to handle
    """
    d_layout = NX_Layout.Layout()

    if "chrono" == a_type.lower():
        d_layout.insert_summary("elapsedUS", "us")
        d_layout.insert_plot("ts", "elapsedUS", ylabel="Execution time (us)")
        d_layout.insert_plot("cdf", "elapsedUS", xlabel="Execution time (us)")
    elif "counter" == a_type.lower():
        d_layout.insert_summary("value")
        d_layout.insert_plot("bar", "value")
        d_layout.insert_plot("ts", "value")
    else:
        d_layout.insert_plot("empty", "nodata")

    return d_layout
