#!/usr/bin/python
# coding: utf-8
import os
import json
import sqlite3
import datetime

# Metrics NXPy
import NX_Renderer
import NX_MetricView
import NX_MetricsException

# Common NXPy
import common.NX_Utils as NX_Utils
import common.NX_Colors as NX_Colors


# TODO: set default value for date_start and end_date
# TODO: use our database module instead of sqlite3

class Dashboard(object):
    """
    A dashboard is a set of metrics views. This class tends to organize and manage
    those views.

    Variables:
       cfg: dictionnary        -- dashboard configuration
       con: sqlite3            -- database connection object
       monitor: list           -- list of monitored metrics
       metricsView: metricView -- metric view object
    """

    def __init__(self, cfg_file):
        """
        Construct a dashboard instance.

        Keyword arguments:
        :param cfg_file -- configuration file.

        Exception raised:
        :exception NX_MetricsException.InputError
        :exception IOError
        """
        if not cfg_file:
            raise NX_MetricsException.InputError("cfg_file", "argument is empty")

        with open(cfg_file, 'r') as f:
            cfg = json.load(f)

        # Check mandatory fields
        mandatory_fields = ["database", "renderer"]
        for field in mandatory_fields:
            if field not in cfg:
                raise NX_MetricsException.DashboardError("Missing mandatory field {F}".format(F=field))

        # Check configuration values
        if cfg["renderer"]["type"] not in NX_Renderer.Renderer.supported_rendering():
            raise NX_MetricsException.DashboardError("{R} is not supported".format(R=cfg["renderer"]["type"]))

        con = sqlite3.connect(cfg["database"])

        # Set default values & validate date
        cfg["title"] = cfg["title"] if "title" in cfg else ""
        cfg["format"] = cfg["format"] if "format" in cfg else "%x"
        cfg["verbose"] = cfg["verbose"] if "verbose" in cfg else False

        try:
            cfg["date_end"] = datetime.datetime.today()
            if "date_end" in cfg:
                cfg["date_end"] = datetime.datetime.strptime(cfg["date_end"], cfg["format"])
            cfg["date_start"] = datetime.datetime(1993, 1, 1)
            if "date_start" in cfg:
                cfg["date_start"] = datetime.datetime.strptime(cfg["date_start"], cfg["format"])
        except ValueError as e:
            raise NX_MetricsException.DashboardError("Date cannot be parsed: {E}".format(E=e.message))

        # Setup internal variables
        self.__cfg = cfg
        self.__con = con
        self.__monitor = []
        self.__metricsView = []

        # Monitor metrics
        if "monitor" in cfg:
            self.__monitor = filter(lambda x: cfg["monitor"][x] is True, cfg["monitor"])
            del cfg["monitor"]

        if cfg["verbose"]:
            self.__print_infos()

    def __del__(self):
        """
        Destroy a dashboard instance.
        """
        self.__con.close()

    @property
    def date_start(self):
        return self.__cfg["date_start"]

    @date_start.setter
    def date_start(self, str_date):
        try:
            self.__cfg["date_start"] = datetime.datetime.strptime(str_date, self.__cfg["format"])
        except ValueError as e:
            raise NX_MetricsException.DashboardError("Date cannot be parsed: {E}".format(E=e.message))

    @property
    def date_end(self):
        return self.__cfg["date_end"]

    @date_end.setter
    def date_end(self, str_date):
        try:
            self.__cfg["date_end"] = datetime.datetime.strptime(str_date, self.__cfg["format"])
        except ValueError as e:
            raise NX_MetricsException.DashboardError("Date canno be parsed: {E}".format(E=e.message))

    @property
    def title(self):
        return self.__cfg["title"]

    @title.setter
    def title(self, new_title):
        self.__cfg["title"] = new_title

    @property
    def verbose(self):
        return self.__cfg["verbose"]

    @verbose.setter
    def verbose(self, value):
        self.__cfg["verbose"] = value

    @property
    def metrics_view(self):
        return self.__metricsView

    @property
    def cfg(self):
        return self.__cfg

    def __print_infos(self):
        """
        Print basic information
        """
        print """{SEP}
Workspace set to [ {WSDIR} ]
Database used for extracting data [{DB}]

Using {RENDERER_TYPE} Renderer [Output directory = {RENDERER_OUTPUT}]
{SEP}
Monitored metrics : {MONITORED}
""".format(SEP='-' * 80,
           WSDIR=NX_Colors.colored_text("GREEN", os.getcwd()),
           DB=NX_Colors.colored_text("GREEN", self.cfg["database"]),
           RENDERER_TYPE=self.__cfg["renderer"]["type"].upper(),
           RENDERER_OUTPUT=NX_Colors.colored_text("GREEN", self.__cfg["renderer"]["output_dir"]),
           MONITORED=", ".join(self.__monitor)
           )

    def add_to_monitor(self, metric_input):
        """
        Add metric to monitor

        Keyword argument:
        :param metric_input -- list of metrics to monitor
        """
        if isinstance(metric_input, basestring):
            metric_input = metric_input.split()
        else:
            metric_input = list(metric_input)
        self.__monitor += metric_input

    def __generate_metric_view(self, layout=None):
        """
        Create a new metric view which will be handled by the dashboard.

        Keyword arguments:
        :param layout -- layout to use by the view
        """
        title_prefix = ""
        n_metric = len(self.__monitor)
        dr = (self.__cfg["date_start"], self.__cfg["date_end"])

        # Generate metric view for each metric
        for idx, m in enumerate(self.__monitor):
            if self.__cfg["verbose"]:
                NX_Utils.progress_bar(idx + 1, n_metric, suffix="[OK] Retrieve data", msg="Retrieving data...")
            self.__metricsView.append(
                NX_MetricView.MetricView(self.__con, m, date_range=dr, fmt=self.__cfg["format"],
                                         title=title_prefix + m, layout=layout))

    def render(self):
        """
        Render the dashboard using the renderer specified in the configuration file
        It does nothing if no metrics is monitored.
        """
        if self.__monitor:
            self.__generate_metric_view()
            NX_Renderer.Renderer.render(self)
