import common.NX_RestClient
import common.NX_Colors
import compliance.NX_VersionMgr
import sys

if __name__ == "__main__":
    try:
        vm = compliance.NX_VersionMgr.PMVersionManager()
        vm.set_options()
        vm.setup()
        vm.process()
    except compliance.NX_VersionMgr.PMError as sys_error:
        print common.NX_Colors.error("Error : ")
        print common.NX_Colors.error(" > {} ".format(sys_error))
        sys.exit(sys_error.rc)
    except common.NX_RestClient.RestException as rest_error:
        print common.NX_Colors.error("Rest Client Error : ")
        print common.NX_Colors.error(" > {} ".format(rest_error.message))
        sys.exit(100)
    except Exception as error:
        print common.NX_Colors.error("Unknown Error ({}): ".format(type(error)))
        print common.NX_Colors.error(" > {} ".format(str(error)))
        sys.exit(11)
    else:
        sys.exit(0)
