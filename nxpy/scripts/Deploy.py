import sys
import traceback
import compliance.NX_EngineDeployer
import common.NX_Colors

if __name__ == "__main__":
    try:
        my_deployer = compliance.NX_EngineDeployer.EngineDeployer()
        my_deployer.set_options()
        my_deployer.setup()
        my_deployer.process()
    except compliance.NX_EngineDeployer.DeployError, e:
        common.NX_Colors.print_error(str(e))
        print traceback.print_exc()
        sys.exit(e.error_code)
    except Exception, e:
        common.NX_Colors.print_error(str(e))
        print traceback.print_exc()
        sys.exit(1)
