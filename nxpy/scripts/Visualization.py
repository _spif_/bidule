#!/usr/bin/python
# coding: utf-8
import sys
import argparse
import traceback
import common.NX_Colors

from metrics import *

if __name__ == "__main__":
    """
    Script to generate visualization of a set of metrics
    """
    try:
        parser = argparse.ArgumentParser(description="Generate artifact for metrics visualization")
        parser.add_argument("-c", "--conf", help="Configuration file", action="store", dest="cfg_file", required=True)
        parser.add_argument("-s", "--date-start", action="store", dest="date_start", required=False,
                            help="Analysis from this date.")
        parser.add_argument("-e", "--date-end", required=False, action="store", dest="date_end",
                            help="Analysis to this date")
        parser.add_argument("-t", "--title", help="Title", action="store", dest="title", required=False)
        parser.add_argument("-q", "--quiet", required=False, action="store_false", dest="verbose",
                            help="Disable verbosity")
        # parser.add_argument("-m", "--monitor", help="Monitored file", action="store", dest="")
        args = parser.parse_args()

        # Create dashboard
        d_mngr = NX_Dashboard.Dashboard(cfg_file=args.cfg_file)

        # Command line arguments overwrite configuration entry
        d_mngr.verbose = args.verbose
        if args.date_start:
            d_mngr.date_start = args.date_start
        if args.date_end:
            d_mngr.date_end = args.date_end
        if args.title:
            d_mngr.title = args.title

        # Render the dashboard
        d_mngr.render()
    except (NX_MetricsException, Exception) as e:
        common.NX_Colors.print_error(str(e))
        print traceback.print_exc()
        sys.exit(1)
