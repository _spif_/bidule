import os
import sys
import argparse
from git import Repo
from git.exc import InvalidGitRepositoryError, CommandError, CheckoutError

RC_REPO_HAS_BEEN_CLONED = 10
RC_COMMIT_CHECKOUT_ONLY = 11
RC_IN_ERROR = 12


def main():
    parser = argparse.ArgumentParser("sf_auto_clone", description="Software Factory tool for retrieving a repository.")
    parser.add_argument("commit_hash", default=None, action="store", help="Commit hash to retrieve.")
    parser.add_argument("working_copy", default=None, action="store", help="Path of the local repository.")
    parser.add_argument("url", default=None, action="store", help="Remote URL.")

    args = parser.parse_args()
    rc = RC_COMMIT_CHECKOUT_ONLY
    try:
        if not os.path.isdir(os.path.abspath(args.working_copy)):
            print "Working copy '{}' does not exists. Let's clone from remote.".format(args.working_copy)
            Repo.clone_from(args.url, args.working_copy)
            rc = RC_REPO_HAS_BEEN_CLONED
            print "Done"

        repo = Repo(args.working_copy)
        print "Fetching data ..."
        repo.git.fetch()
        print "About to checkout commit '{}'...".format(args.commit_hash)
        repo.git.checkout(args.commit_hash)
        print "Done"
    except InvalidGitRepositoryError as git_error:
        print "The given repository '{}' is not a valid  git working copy. Aborting.".format(args.working_copy)
        print "Detailed error : {}".format(git_error)
        rc = RC_IN_ERROR
    except CommandError as git_error:
        print "Git command error : {}".format(git_error)
        rc = RC_IN_ERROR
    except CheckoutError as git_error:
        print "Git checkout error : {}".format(git_error)
        rc = RC_IN_ERROR
    except Exception as script_error:
        print "Unknown error : {}".format(script_error)
        rc = RC_IN_ERROR
    finally:
        sys.exit(rc)


if __name__ == "__main__":
    main()
