import logging

__init = False


class StdFormatter(logging.Formatter):
    """Simple log formatter which handles colors"""
    def __init__(self):
        logging.Formatter.__init__(self, "%(asctime)s %(name)s %(levelID)s %(message)s", '%H:%M:%S')

    def format(self, rec):
        """
        Formats records and adds colors as needed. The records do not get
        a leading hour format if the logging level is above *INFO*.
        """
        lvl = {logging.DEBUG: "D", logging.INFO: "I", logging.WARNING: "W", logging.ERROR: "E", logging.CRITICAL: "C"}
        rec.levelID = lvl[rec.levelno]
        return logging.Formatter.format(self, rec)


def init_log_systems(path, name):
    global __init
    if not __init:
        lgr = logging.getLogger(name)
        fh = logging.FileHandler(path)
        fmtr = StdFormatter()
        fh.setFormatter(fmtr)
        lgr.addHandler(fh)
        lgr.setLevel(logging.INFO)
        __init = True
