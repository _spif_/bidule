import os
import platform


class Env(object):
    """
    This class wraps all the necessary information that must be collected from the environment.
    It provides some getter to access these information in a read only way but also provides some
    specific method to extend critical environment variable (such as the LD PATH)
    """
    def __init__(self):
        """
        Construct the Env object, and read from the shell environment mandatory environment variables.
        """
        keys = ["PLATFORM", "COMPILER_TYPE", "COMPILER_VERSION", "BUILD_BITS", "BUILD_MODE", "TOOLS", "SPECIAL_MODE"]
        ids = ["NX_%s" % i for i in keys]
        self.__values = {"WORKSPACE": os.getenv("WORKSPACE", "./")}
        for key in ids:
            self.__values[key] = os.getenv(key, "")
        self.__osname = "unknown"
        if platform.system() == "AIX":
            self.__values["OSNAME"] = "aix"
        elif platform.system() == "Linux":
            self.__values["OSNAME"] = "linux"
        elif platform.system() == "SunOS":
            self.__values["OSNAME"] = "sol"
        elif platform.system() == "Windows":
            self.__values["OSNAME"] = "Win64"
        else:
            raise Exception("Unknown operating system '{}'".format(platform.system()))

    def get_lib_loader_env_key(self):
        """
        Get the corresponding environment variable name that controls the dynamic library search path.
        :rtype: string
        :return: An environment variable key.
        """
        search_path_key = "LD_LIBRARY_PATH"
        if platform.system() == "AIX":
            search_path_key = "LIBPATH"
        elif platform.system() == "SunOS" and self.build_bits == "64":
            search_path_key += "_64"
        elif platform.system() == "Windows":
            search_path_key = "PATH"
        return search_path_key

    @staticmethod
    def extend_environment(key, value, at_front=False):
        """
        Extend any environment variable with the given value (must be of type sequence).
        :param key: Environment variable name.
        :type key: str
        :param value: Sequence of values
        :type value: list
        :param at_front:  If set to True, each values will be pushed at front of the
                          existing values (sort of prioritization).
        """
        for v in value:
            if key not in os.environ:
                os.environ[key] = v
            if v not in os.environ[key].split(os.pathsep):
                if at_front:
                    tmp = os.environ[key]
                    os.environ[key] = v+os.pathsep+tmp
                else:
                    os.environ[key] += os.pathsep+v

    def extend_lib_loader_path(self, search_path, at_front=False):
        """
        Utility method for extending loader lib path, whatever the OS."
        :param search_path: Path to add to the loader lib path.
        :type search_path: list
        :param at_front: if True, position the SearchPath at the front (most wanted location).
        """
        self.extend_environment(self.get_lib_loader_env_key(), search_path, at_front)

    def valid(self):
        """
        Utility method that can be used to check if the NX environment is valid or not.
        :return: True if valid, False otherwise.
        """
        for i in self.__values:
            if i not in ["NX_SPECIAL_MODE", "WORKSPACE"] and not self.__values[i]:
                return False
        if self.build_bits not in ["32", "64"]:
            return False
        if self.build_mode not in ["gcov", "debug", "release", "valgrind", "asan", "tsan"]:
            return False
        if not os.path.exists(self.tools_root_dir):
            return False
        if not os.path.exists(self.workspace):
            return False
        return True

    def valid_for_deployment(self):
        """
        Utility method that can be used to check if the NX environment is valid or not.
        :return: True if valid, False otherwise.
        """
        for i in self.__values:
            if i not in ["NX_SPECIAL_MODE", "WORKSPACE"] and not self.__values[i]:
                return False
        if self.build_bits not in ["32", "64"]:
            return False
        if self.build_mode not in ["debug", "release"]:
            return False
        return True

    @property
    def java_bin(self):
        """
        Get the path to the JAVA bin directory. Under windows, can be overwritten with NX_JAVA environment variable.
        :return: A path to the JAVA bin directory.
        :rtype: str
        """
        d = {"LinuxUBU1204": "/BUILD/SOFT/tools/java/1.6.0_41/bin",
             "LinuxCentOS7": os.path.join(self.tools_root_dir, "third_parts", "external_64", "java", "1.6.0_41", "bin"),
             "AIX71": "/usr/java6/bin",
             "SOL10": "/BUILD/SOFT/tools/java/jdk1.6.0_32/bin",
             "Win64": "D:\\tools\\java\\6u32\\bin"}
        if self.platform == "Win64":
            if "NX_JAVA" in os.environ:
                return os.path.join(os.environ["NX_JAVA"], "bin")
        return d[self.platform]

    @property
    def java_lib(self):
        """
        Get the path to the JAVA lib directory. Under windows, can be overwritten with NX_JAVA environment variable.
        :return: A path to the JAVA lib directory.
        :rtype: str
        """
        d = {"LinuxUBU1204": "/BUILD/SOFT/tools/java/1.6.0_41/lib",
             "LinuxCentOS7": os.path.join(self.tools_root_dir, "third_parts", "external_64", "java", "1.6.0_41", "lib"),
             "AIX71": "/usr/java6/lib",
             "SOL10": "/BUILD/SOFT/tools/java/jdk1.6.0_32/lib",
             "Win64": "D:\\tools\\java\\6u32\\lib"}
        if self.platform == "Win64":
            if "NX_JAVA" in os.environ:
                return os.path.join(os.environ["NX_JAVA"], "lib")
        return d[self.platform]

    @property
    def os_name(self):
        """
        Real platform name, all lower case.
        :return: String containing the os name in lower case.
        """
        return self.__values["OSNAME"]

    @property
    def special_mode(self):
        return self.__values["NX_SPECIAL_MODE"]

    @property
    def playground(self):
        """
        Path to real playground according to the environment currently set.
        :return: A path to the playground.
        """
        return os.path.join(self.__values["WORKSPACE"], "playground")

    @property
    def playground_lib_dir(self):
        """
        Path to real playground lib directory according to the environment currently set.
        :return: A path to the playground lib directory.
        """
        return os.path.join(self.playground, "lib", self.platform_id_ext)

    @property
    def playground_bin_dir(self):
        """
        Path to real playground bin directory according to the environment currently set.
        :return: A path to the playground lib directory.
        """
        return os.path.join(self.playground, "bin", self.platform_id_ext)

    @property
    def workspace(self):
        """
        Workspace path set.
        :return:  A path to the current workspace.
        """
        return self.__values["WORKSPACE"]

    @property
    def uworkspace(self):
        """
        Workspace path set, universal flavor (using unix directory separator).
        :return:  A path to the current workspace.
        """
        return self.workspace.replace("\\", "/")

    @workspace.setter
    def workspace(self, val):
        """
        Change the current workspace location.
        :param val: Path to the new workspace.
        :type val: str
        """
        if os.path.exists(val):
            self.__values["WORKSPACE"] = val
            os.environ["WORKSPACE"] = val
        else:
            raise Exception("Cannot set Workspace to [{}] : path does not exists!".format(val))

    @property
    def platform(self):
        """
        The platform set. Somewhat similar to OS.
        :return: The operating system identifier string (i.e. : LinuxUBU1204)
        """
        return self.__values["NX_PLATFORM"]

    @property
    def compiler_path(self):
        """
        Path to the compiler, computed using the environment currently set.
        :return: The path to the compiler
        :rtype: str
        """
        values = [self.tools_root_dir, "compilers", self.platform, self.compiler_type, self.compiler_version]
        if os.altsep is not None:
            return os.altsep.join(values)
        return os.sep.join(values)

    @property
    def compiler_id(self):
        """
        Provide a compiler identification string in the form <TYPE>_<VERSION>
        :return: Compiler identification string.
        :rtype: string
        """
        return "{T}_{V}".format(T=self.compiler_type, V=self.compiler_version)

    @property
    def compiler_type(self):
        """
        Type of the compiler, has set in the environment.
        :return: The compiler type.
        :rtype: str
        """
        return self.__values["NX_COMPILER_TYPE"]

    @property
    def compiler_version(self):
        """
        Version of the compiler, has set in the environment.
        :return: The compiler version.
        :rtype: str
        """
        return self.__values["NX_COMPILER_VERSION"]

    @property
    def compiler_version_mmp(self):
        """
        Get the version of the compiler has set in the environment, but in the form Major.Minor.Patch whenever possible.
        This can be seen as the technical version number not the distribution one (especially for windows).
        :return: Technical compiler version.
        :rtype: str
        """
        if "NX_COMPILER_VERSION" in self.__values:
            if self.compiler_type == "GCC":
                i = int(self.compiler_version)
                major = i/10000
                i -= major*10000
                minor = i/100
                patch = i-minor*100
                return "{MAJ}.{MIN}.{PATCH}".format(MAJ=major, MIN=minor, PATCH=patch)
            elif self.compiler_type == "MSVC":
                d = {"1900": 14}
                return d[self.compiler_version]
        return ""

    @property
    def build_bits(self):
        """
        Bitmode used for generating code.
        :return: The bitmode.
        :rtype: str
        """
        return self.__values["NX_BUILD_BITS"]

    @property
    def build_mode(self):
        """
        build mode (release, debug, asan, tsan, valgrind or gcov)
        :return: The build mode.
        :rtype: str
        """
        return self.__values["NX_BUILD_MODE"]

    @property
    def tools_root_dir(self):
        """
        Path to the tools root directory as set in the environment.
        :return: The absolute path to the tools.
        :rtype: str
        """
        return self.__values["NX_TOOLS"]

    @property
    def platform_id(self):
        """
        The platform id is a string that fully qualifies a build with all the required information which are necessary
        to get all the environment. This property computes this id using the environment.
        :return: The platform identification string, as set from the environment.
        :rtype: str
        """
        if self.valid():
            return "{OS}_{CPLR}_{CPLRVERS}_{BITS}_{MODE}".format(OS=self.platform,
                                                                 CPLR=self.compiler_type,
                                                                 CPLRVERS=self.compiler_version,
                                                                 BITS=self.build_bits,
                                                                 MODE=self.build_mode)
        return ""

    @property
    def platform_id_ext(self):
        """
        The platform id is a string that fully qualifies a build with all the required information which are necessary
        to get all the environment. This property computes this id using the environment along with the special mode
        indicator, if any.
        :return: The platform identification string, as set from the environment.
        :rtype: str
        """
        if self.valid():
            if self.special_mode:
                return "{OS}_{CPLR}_{CPLRVERS}_{BITS}_{MODE}.{SP}".format(OS=self.platform,
                                                                          CPLR=self.compiler_type,
                                                                          CPLRVERS=self.compiler_version,
                                                                          BITS=self.build_bits,
                                                                          MODE=self.build_mode,
                                                                          SP=self.special_mode)
            else:
                return "{OS}_{CPLR}_{CPLRVERS}_{BITS}_{MODE}".format(OS=self.platform,
                                                                     CPLR=self.compiler_type,
                                                                     CPLRVERS=self.compiler_version,
                                                                     BITS=self.build_bits,
                                                                     MODE=self.build_mode)
        return ""

    @staticmethod
    def get_valid_compilers_family():
        return ["GCC", "MSVC"]
