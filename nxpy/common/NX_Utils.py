import sys
import os
import shutil
import NX_Env
import stat
import glob


if NX_Env.Env().os_name == "Win64":
    import ctypes


def super_get_attr(the_object, the_attribute):
    """
    This method allows you to retrieve any property object or attribute from a mother class and reuse it later.
    :param the_object: The object from which the getattr must be executed from
    :type the_object: object
    :param the_attribute:  Attribute/property name to look for.
    :type the_attribute: str
    :return: None if the property could not be resolved, a property object/a member otherwise.
    """
    for o in [the_object]+the_object.__class__.mro():
        if the_attribute in o.__dict__:
            return o.__dict__[the_attribute]
    return None


def find_file_in_directory(search_dir, file_name):
    for root, folders, files in os.walk(search_dir):
        for f in files:
            if file_name == f:
                return os.path.join(root, file_name)
    return None


def set_x_rights_to(file_path, user=True, group=True, other=True):
    file_stat = os.stat(file_path)
    mode = 0
    if user:
        mode |= stat.S_IXUSR
    if group:
        mode |= stat.S_IXGRP
    if other:
        mode |= stat.S_IXOTH
    os.chmod(file_path, file_stat.st_mode | mode)


def list_all_files_in(search_dir):
    files = []
    for entry in glob.glob(os.path.join(search_dir, "*")):
        if os.path.isdir(entry):
            files.extend(list_all_files_in(entry))
        else:
            if entry not in files:
                files.append(entry)
    return files


def win_make_dir(arg):
    arg = os.path.normpath(arg)
    if not os.path.exists(arg):
        print 'Making directory path [{ent}]'.format(ent=arg)
        os.makedirs(arg)


def win_symlink_all_in(*args):
    if len(args) % 2 == 1:
        print "Cannot proceed : missing an argument to create a list of links."
    else:
        for i in range(0, len(args)-1, 2):
            win_sym_link(args[i], args[i+1])


def win_sym_link(src, dst):
    src, dst = os.path.normpath(src.replace("/", os.sep)), os.path.normpath(dst.replace("/", os.sep))
    flag = 1 if src is not None and os.path.isdir(src) else 0
    if not os.path.exists(dst):
        if ctypes.windll.kernel32.CreateSymbolicLinkA(dst, src, flag) == 0:
            print "Cannot create link [%s]" % dst


def sym_link(src, dst, cwd=None):
    current = os.getcwd()
    if cwd is not None:
        os.chdir(os.path.abspath(cwd))
    if NX_Env.Env().os_name == "Win64":
        win_sym_link(src, dst)
    else:
        os.symlink(src, dst)
    if cwd is not None:
        os.chdir(current)


def erase_fs_entry(path):
    if os.path.exists(path):
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)


def extend_unique(seq_a, seq_b):
    r = [i for i in seq_a]
    r.extend([i for i in seq_b if i not in seq_a])
    return r


def make_shlib_name(base_name, version):
    e = NX_Env.Env()
    if version:
        if e.os_name in ["linux", "sol", "aix"]:
            return base_name if base_name.endswith(".so") else "lib%s.so.%s" % (base_name, version)
        if e.os_name in ["Win64"]:
            return base_name if base_name.endswith(".dll") else "%s.%s.dll" % (base_name, version)
    else:
        if e.os_name in ["linux", "sol", "aix"]:
            return base_name if base_name.endswith(".so") else "lib%s.so" % base_name
        if e.os_name in ["Win64"]:
            return base_name if base_name.endswith(".dll") else "%s.dll" % base_name


def make_stlib_name(base_name):
    e = NX_Env.Env()
    if e.os_name in ["linux", "sol", "aix"]:
        return base_name if base_name.endswith(".a") else "lib%s.a" % base_name
    if e.os_name in ["Win64"]:
        return base_name if base_name.endswith(".lib") else "%s.lib" % base_name


def make_bin_name(base_name):
    e = NX_Env.Env()
    if e.os_name in ["linux", "sol", "aix"]:
        return "%s" % base_name
    if e.os_name in ["Win64"]:
        return base_name if base_name.endswith(".exe") else "%s.exe" % base_name


def progress_bar(iteration, total, prefix='', suffix='', decimals=1, bar_length=100, msg=""):
    """
    Call in a loop to create terminal progress bar
    :param iteration: current iteration
    :type iteration: int
    :param total: number of expected iterations
    :type total: int
    :param prefix: prefix string
    :type prefix: str
    :param suffix: suffix string
    :type suffix: str
    :param decimals: positive number of decimals in percent complete
    :type decimals: int
    :param bar_length: character length of bar
    :type bar_length: int
    :param msg: informative text that can be used to describe the current operation being executed.
                Limited to 45 characters.
    :type msg: str
    """
    format_str = "{0:." + str(decimals) + "f}"
    percents = format_str.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '=' * (filled_length-1)+">" + '-' * (bar_length - filled_length)
    final_msg = msg
    if final_msg:
        if len(final_msg) > 45:
            final_msg = final_msg[0:42]+"..."
        else:
            final_msg = "{:<45}".format(msg)
    if iteration == total:
        bar = '=' * filled_length + '-' * (bar_length - filled_length)
        msg_fmt = "{:<45}".format(' ')
        sys.stdout.write('%s |%s| %s%s %s %s\r' % (prefix, bar, percents, '%', suffix, msg_fmt)),
    else:
        if msg:
            sys.stdout.write('%s |%s| %s%s %s %s\r' % (prefix, bar, percents, '%', suffix, final_msg)),
        else:
            sys.stdout.write('%s |%s| %s%s %s\r' % (prefix, bar, percents, '%', suffix)),
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()
