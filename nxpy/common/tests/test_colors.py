import os
import unittest
import mock
import sys
import contextlib
import StringIO
import common.NX_Colors


@contextlib.contextmanager
def capture():
    old_out, old_err = sys.stdout, sys.stderr
    out = [StringIO.StringIO(), StringIO.StringIO()]
    try:
        sys.stdout, sys.stderr = out
        yield out
    finally:
        sys.stdout, sys.stderr = old_out, old_err
        out[0] = out[0].getvalue()
        out[1] = out[1].getvalue()


def os_getenv(key, default):
    if key in os.environ:
        return os.environ[key]
    return default


class TestColors(unittest.TestCase):
    def test_colors(self):
        colors = "RED:BOLD:CYAN"
        if "STD_COLOR_OUTPUT" in os.environ:
            del os.environ["STD_COLOR_OUTPUT"]
        with mock.patch("os.getenv", os_getenv):
            colored = common.NX_Colors.colored_text(colors, "Hello World")
            self.assertEqual(colored, "\x1b[01;31m\x1b[01;1m\x1b[36mHello World\x1b[0m")
            colors = "RED:BOLD:CYAN:NOT_A_COLOR"
            colored = common.NX_Colors.colored_text(colors, "Hello World")
            self.assertEqual(colored, "\x1b[01;31m\x1b[01;1m\x1b[36mHello World\x1b[0m")
            os.environ["STD_COLOR_OUTPUT"] = "1"
            colored = common.NX_Colors.colored_text(colors, "Hello World")
            self.assertEqual(colored, "Hello World")
            colors = "RED:BOLD:CYAN:NOT_A_COLOR"
            colored = common.NX_Colors.colored_text(colors, "Hello World")
            self.assertEqual(colored, "Hello World")

    def test_warning(self):
        if "STD_COLOR_OUTPUT" in os.environ:
            del os.environ["STD_COLOR_OUTPUT"]
        with mock.patch("os.getenv", os_getenv):
            colored = common.NX_Colors.warning("Hello World")
            self.assertEqual(colored, "\x1b[33m\x1b[01;1mHello World\x1b[0m")
            os.environ["STD_COLOR_OUTPUT"] = "1"
            colored = common.NX_Colors.warning("Hello World")
            self.assertEqual(colored, "Hello World")

    def test_info(self):
        if "STD_COLOR_OUTPUT" in os.environ:
            del os.environ["STD_COLOR_OUTPUT"]
        with mock.patch("os.getenv", os_getenv):
            colored = common.NX_Colors.info("Hello World")
            self.assertEqual(colored, "\x1b[01;1mHello World\x1b[0m")
            os.environ["STD_COLOR_OUTPUT"] = "1"
            colored = common.NX_Colors.info("Hello World")
            self.assertEqual(colored, "Hello World")

    def test_error(self):
        if "STD_COLOR_OUTPUT" in os.environ:
            del os.environ["STD_COLOR_OUTPUT"]
        with mock.patch("os.getenv", os_getenv):
            colored = common.NX_Colors.error("Hello World")
            self.assertEqual(colored, "\x1b[01;31m\x1b[01;1mHello World\x1b[0m")
            os.environ["STD_COLOR_OUTPUT"] = "1"
            colored = common.NX_Colors.error("Hello World")
            self.assertEqual(colored, "Hello World")

    def test_printers(self):
        if "STD_COLOR_OUTPUT" in os.environ:
            del os.environ["STD_COLOR_OUTPUT"]
        with mock.patch("os.getenv", os_getenv):
            with capture() as my_stdout:
                colors = "RED:BOLD:CYAN"
                common.NX_Colors.print_text(colors, "Hello World")
                lines = [line for line in my_stdout[0].getvalue().split("\n")]
                self.assertItemsEqual(lines, ["\x1b[01;31m\x1b[01;1m\x1b[36mHello World\x1b[0m", ""])
            with capture() as my_stdout:
                common.NX_Colors.print_warning("Hello World")
                lines = [line for line in my_stdout[0].getvalue().split("\n")]
                self.assertEqual(lines, ["\x1b[33m\x1b[01;1mHello World\x1b[0m", ""])
            with capture() as my_stdout:
                common.NX_Colors.print_info("Hello World")
                lines = [line for line in my_stdout[0].getvalue().split("\n")]
                self.assertEqual(lines, ["\x1b[01;1mHello World\x1b[0m", ""])
            with capture() as my_stdout:
                common.NX_Colors.print_error("Hello World")
                lines = [line for line in my_stdout[0].getvalue().split("\n")]
                self.assertEqual(lines, ["\x1b[01;31m\x1b[01;1mHello World\x1b[0m", ""])
            os.environ["STD_COLOR_OUTPUT"] = "1"
            with capture() as my_stdout:
                colors = "RED:BOLD:CYAN"
                common.NX_Colors.print_text(colors, "Hello World")
                lines = [line for line in my_stdout[0].getvalue().split("\n")]
                self.assertItemsEqual(lines, ["Hello World", ""])
            with capture() as my_stdout:
                common.NX_Colors.print_warning("Hello World")
                lines = [line for line in my_stdout[0].getvalue().split("\n")]
                self.assertEqual(lines, ["Hello World", ""])
            with capture() as my_stdout:
                common.NX_Colors.print_info("Hello World")
                lines = [line for line in my_stdout[0].getvalue().split("\n")]
                self.assertEqual(lines, ["Hello World", ""])
            with capture() as my_stdout:
                common.NX_Colors.print_error("Hello World")
                lines = [line for line in my_stdout[0].getvalue().split("\n")]
                self.assertEqual(lines, ["Hello World", ""])


def create_test_suite():
    return unittest.TestLoader().loadTestsFromTestCase(TestColors)
