import os
import glob
import unittest
import xmlrunner

if __name__ == "__main__":
    current_dir = os.path.abspath(os.path.dirname(__name__))
    test_reports = os.path.join(current_dir, "tests-reports")

    # Making test suites...
    test_modules = map(lambda x: x[:len(x)-3], glob.glob("test_*.py"))
    all_tests = unittest.TestSuite()
    for test_module in test_modules:
        mdl = __import__(test_module)
        all_tests.addTest(mdl.create_test_suite())
    testRunner = xmlrunner.XMLTestRunner(output=test_reports)
    testRunner.run(all_tests)
