import unittest
import mock
import stat
import os
import itertools
import common.NX_Utils
import glob


def os_walk(search_dir):
    for i in "hello":
        yield search_dir, ["{}{}_dir".format(i, j) for j in range(2)], ["file_{}{}".format(i, j) for j in range(100)]


# The ugly but no other solution yet proposal...
os_op_files = {}


def os_stat(file_path):
    class MyStat(object):
        def __init__(self):
            self.st_mode = 0
    global os_op_files
    os_op_files[file_path] = MyStat()
    return os_op_files[file_path]


def os_chmod(file_path, value):
    global os_op_files
    os_op_files[file_path].st_mode = value


def platform_system_aix():
    return "AIX"


def platform_system_linux():
    return "Linux"


def platform_system_sun():
    return "SunOS"


def platform_system_win():
    return "Windows"


def setup_env(platform, cplr_type, cpl_version, bits, mode, tool_path, workspace=None, special_mode=None):
    def work(key, value):
        if value:
            os.environ[key] = value
        else:
            if key in os.environ:
                del os.environ[key]

    work("NX_PLATFORM", platform)
    work("NX_COMPILER_TYPE", cplr_type)
    work("NX_COMPILER_VERSION", cpl_version)
    work("NX_TOOLS", tool_path)
    work("NX_BUILD_BITS", bits)
    work("NX_BUILD_MODE", mode)
    work("WORKSPACE", workspace)
    work("NX_SPECIAL_MODE", special_mode)


class TestUtils(unittest.TestCase):
    def test_find_file_in_directory(self):
        def joiner(*args):
            return "/".join(args)

        with mock.patch("os.walk", os_walk):
            with mock.patch("os.path.join", joiner):
                self.assertIsNone(common.NX_Utils.find_file_in_directory("/", "file_ab"))
                for i in range(100):
                    f_name = "file_e{}".format(i)
                    self.assertEqual("/root/{}".format(f_name), common.NX_Utils.find_file_in_directory("/root", f_name))

    def test_set_x_rights_to(self):
        global os_op_files
        with mock.patch("os.stat", os_stat):
            with mock.patch("os.chmod", os_chmod):
                for u, g, o in itertools.product((True, False), (True, False), (True, False)):
                    common.NX_Utils.set_x_rights_to("/the/toto/file", u, g, o)
                    value = 0
                    value = value if not u else value | stat.S_IXUSR
                    value = value if not g else value | stat.S_IXGRP
                    value = value if not o else value | stat.S_IXOTH
                    self.assertEqual(value, os_op_files["/the/toto/file"].st_mode)

    def test_list_all_files_in(self):
        # We need to avoid pyc files, but also we need to ensure universal file path.
        # Hence this inner method
        def to_uniform(sequence_of_path):
            sequence_of_path = filter(lambda (x): x.endswith(".py"), sequence_of_path)
            sequence_of_path = map(lambda (x): x.replace("\\", "/"), sequence_of_path)
            sequence_of_path = map(lambda (x): x[x.rfind("common/"):], sequence_of_path)
            sequence_of_path.sort()
            return sequence_of_path
        target_dir = os.path.dirname(os.path.dirname(os.path.abspath(__name__)))
        expected = glob.glob(os.path.join(target_dir, "**.py"))
        expected.extend(glob.glob(os.path.join(os.path.dirname(os.path.abspath(__name__)), "**.py")))
        expected = to_uniform(expected)
        got = to_uniform(common.NX_Utils.list_all_files_in(target_dir))
        self.assertItemsEqual(expected, got)

    def test_compute_fun(self):
        bin_expected_name = ["toto", "toto", "toto", "toto.exe"]
        shlib_expected_name = ["libtoto.so", "libtoto.so", "libtoto.so", "toto.dll"]
        shlib_wext_expected_name = ["toto.so", "toto.so", "toto.so", "libtoto.dll"]
        shlib_version_expected_name = ["libtoto.so.1.0", "libtoto.so.1.0", "libtoto.so.1.0", "toto.1.0.dll"]
        stlib_expected_name = ["libtoto.a", "libtoto.a", "libtoto.a", "toto.lib"]
        stlib_wext_expected_name = ["toto.a", "toto.a", "toto.a", "libtoto.lib"]
        for i, f in enumerate([platform_system_aix, platform_system_linux, platform_system_sun, platform_system_win]):
            with mock.patch("platform.system", f):
                self.assertEqual(bin_expected_name[i], common.NX_Utils.make_bin_name("toto"))
                self.assertEqual(stlib_expected_name[i], common.NX_Utils.make_stlib_name("toto"))
                self.assertEqual(shlib_version_expected_name[i], common.NX_Utils.make_shlib_name("toto", "1.0"))
                self.assertEqual(shlib_expected_name[i], common.NX_Utils.make_shlib_name("toto", ""))
                val = shlib_wext_expected_name[i]
                self.assertEqual(val, common.NX_Utils.make_shlib_name(val, "1.0"))
                self.assertEqual(val, common.NX_Utils.make_shlib_name(val, ""))
                val = stlib_wext_expected_name[i]
                self.assertEqual(val, common.NX_Utils.make_stlib_name(val))
        with mock.patch("platform.system", platform_system_win):
            self.assertEqual("toto.exe", common.NX_Utils.make_bin_name("toto.exe"))


def create_test_suite():
    return unittest.TestLoader().loadTestsFromTestCase(TestUtils)
