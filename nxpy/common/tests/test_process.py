import common.NX_Process
import mock
import os
import sys
import tempfile
import unittest
import platform
import contextlib
import StringIO
import common.NX_Colors


@contextlib.contextmanager
def capture():
    old_out, old_err = sys.stdout, sys.stderr
    out = [StringIO.StringIO(), StringIO.StringIO()]
    try:
        sys.stdout, sys.stderr = out
        yield out
    finally:
        sys.stdout, sys.stderr = old_out, old_err
        out[0] = out[0].getvalue()
        out[1] = out[1].getvalue()


def find_program(program_name, extention=None):
    for path in os.environ["PATH"].split(os.pathsep):
        test_path = os.path.join(path, program_name)
        os_ext = ["", ".py", ".sh"] if platform.system() != "Windows" else [".exe", ".bat", ".cmd", ".py"]
        if extention is not None:
            os_ext = extention
        for ext in os_ext:
            test_path_ext = test_path + ext
            if os.path.isfile(test_path_ext):
                return test_path
    return None


def os_getenv(key, default):
    os.environ["STD_COLOR_OUTPUT"] = "1"
    return os.environ[key] if key in os.environ else default


class TestProcess(unittest.TestCase):
    def test_simple_case_run(self):
        py_path = find_program("python")
        self.assertTrue(py_path is not None)
        py = common.NX_Process.Process(py_path)
        py.cwd = os.path.abspath("/")
        py.args = ["-c", "import sys; sys.stdout.write('Hello World'); sys.stderr.write('Hell, World!')"]
        with capture() as out_channels:
            with mock.patch("os.getenv", os_getenv):
                py.run(keep_stderr=True, keep_stdout=True, out_prefix="[O] ", err_prefix="[E] ")
            out_lines = [line for line in out_channels[0].getvalue().split("\n")]
            self.assertEqual(len(out_lines), 3)
            self.assertTrue("[O] Hello World" in out_lines, out_lines)
            self.assertTrue("[E] Hell, World!" in out_lines, out_lines)
            self.assertTrue("" in out_lines)
        self.assertEqual(py.err, ["Hell, World!"])
        self.assertEqual(py.out, ["Hello World"])
        self.assertEqual(py.last_return_code, 0)

    def test_simple_case_run_and_poll(self):
        py_path = find_program("python")
        self.assertTrue(py_path is not None)
        py = common.NX_Process.Process(py_path)
        py.cwd = os.path.abspath("/")
        py.args = ["-c", "import sys; sys.stdout.write('Hello World'); sys.stderr.write('Hell, World!')"]
        with capture() as out_channels:
            with mock.patch("os.getenv", os_getenv):
                py.run_and_poll(keep_stderr=True, keep_stdout=True, out_prefix="[O] ", err_prefix="[E] ")
            out_lines = [line for line in out_channels[0].getvalue().split("\n")]
            self.assertEqual(len(out_lines), 3)
            self.assertTrue("[O] Hello World" in out_lines)
            self.assertTrue("[E] Hell, World!" in out_lines)
            self.assertTrue("" in out_lines)
        self.assertEqual(py.err, ["Hell, World!"])
        self.assertEqual(py.out, ["Hello World"])
        self.assertEqual(py.last_return_code, 0)

    def test_property(self):
        py_path = find_program("python")
        self.assertTrue(py_path is not None)
        py = common.NX_Process.Process(py_path)
        self.assertEqual(py.program_no_ext, "python")
        self.assertEqual(py.program, "python.exe" if platform.system() == "Windows" else "python")
        self.assertEqual(os.path.dirname(py_path), py.program_path)

    def test_to_file_with_run(self):
        py_path = find_program("python")
        self.assertTrue(py_path is not None)
        py = common.NX_Process.Process(py_path)
        py.cwd = os.path.abspath("/")
        py.args = ["-c", "import sys; sys.stdout.write('Hello World'); sys.stderr.write('Hell, World!'); sys.exit(1)"]
        temp_out = tempfile.mkstemp(text=True)
        temp_err = tempfile.mkstemp(text=True)
        with mock.patch("os.getenv", os_getenv):
            self.assertRaises(Exception, py.run, stdout_file_path=temp_out, stderr_file_path=temp_err)
            self.assertRaises(Exception, py.run, stdout_file_path=temp_out, stderr_file_path=temp_err[1])
            py.run(stdout_file_path=temp_out[1], stderr_file_path=temp_err[1])
        self.assertTrue(os.path.isfile(temp_err[1]))
        self.assertTrue(os.path.isfile(temp_out[1]))
        with open(temp_out[1], "r") as f:
            lines = [line.strip() for line in f]
            self.assertEqual(["Hello World"], lines)
        with open(temp_err[1], "r") as f:
            lines = [line.strip() for line in f]
            self.assertEqual(["Hell, World!"], lines)
        for f in [temp_err, temp_out]:
            os.close(f[0])
            os.remove(f[1])

    def test_to_file_with_run_and_poll(self):
        py_path = find_program("python")
        self.assertTrue(py_path is not None)
        py = common.NX_Process.Process(py_path)
        py.cwd = os.path.abspath("/")
        py.args = ["-c", "import sys; sys.stdout.write('Hello World'); sys.stderr.write('Hell, World!'); sys.exit(1)"]
        temp_out = tempfile.mkstemp(text=True)
        temp_err = tempfile.mkstemp(text=True)
        with mock.patch("os.getenv", os_getenv):
            self.assertRaises(Exception, py.run_and_poll, stdout_file_path=temp_out, stderr_file_path=temp_err)
            self.assertRaises(Exception, py.run_and_poll, stdout_file_path=temp_out, stderr_file_path=temp_err[1])
            py.run_and_poll(stdout_file_path=temp_out[1], stderr_file_path=temp_err[1])
        self.assertTrue(os.path.isfile(temp_err[1]))
        self.assertTrue(os.path.isfile(temp_out[1]))
        with open(temp_out[1], "r") as f:
            lines = [line.strip() for line in f]
            self.assertEqual(["Hello World"], lines)
        with open(temp_err[1], "r") as f:
            lines = [line.strip() for line in f]
            self.assertEqual(["Hell, World!"], lines)
        for f in [temp_err, temp_out]:
            os.close(f[0])
            os.remove(f[1])

    def test_usage(self):
        def os_path_exists(path):
            return path.find("exists") != -1

        def os_path_isdir(path):
            return path.startswith("d")

        with mock.patch("os.path.exists", os_path_exists), mock.patch("os.path.isdir", os_path_isdir):
            py = common.NX_Process.Process("/c/python27/python")
            py.cwd = "/exists/not/a/dir"
            self.assertRaisesRegexp(Exception, "Given path \[.*\] is not a directory.$", py.run_and_poll)
            self.assertRaisesRegexp(Exception, "Given path \[.*\] is not a directory.$", py.run)
            py.cwd = "/invalid/dir"
            self.assertRaisesRegexp(Exception, "Given working directory \[.*\] does not exists.$", py.run_and_poll)
            self.assertRaisesRegexp(Exception, "Given working directory \[.*\] does not exists.$", py.run)
            py.cwd = "d/exists/is/a/dir"
            self.assertRaisesRegexp(Exception, "Program cannot be found at the given path \[.*\].$", py.run_and_poll)
            self.assertRaisesRegexp(Exception, "Program cannot be found at the given path \[.*\].$", py.run)


def create_test_suite():
    return unittest.TestLoader().loadTestsFromTestCase(TestProcess)
