import common.NX_Env
import unittest
import os
import mock
import itertools


def platform_system_aix():
    return "AIX"


def platform_system_sunos():
    return "SunOS"


def platform_system_linux():
    return "Linux"


def platform_system_windows():
    return "Windows"


def platform_system_unknown():
    return "Unknown"


def setup_env(platform, cplr_type, cpl_version, bits, mode, tool_path, validity_fun, workspace=None, special_mode=None):
    def work(key, value):
        if value:
            os.environ[key] = value
        else:
            if key in os.environ:
                del os.environ[key]

    work("NX_PLATFORM", platform)
    work("NX_COMPILER_TYPE", cplr_type)
    work("NX_COMPILER_VERSION", cpl_version)
    work("NX_TOOLS", tool_path)
    work("NX_BUILD_BITS", bits)
    work("NX_BUILD_MODE", mode)
    work("WORKSPACE", workspace)
    work("NX_SPECIAL_MODE", special_mode)
    # Will we have a valid environment?
    return validity_fun()


def path_exists(value):
    if value in ["/my/tools", "./", "/my/workspace", "\\my\\workspace"]:
        # "./" is for workspace
        return True
    else:
        return False


def os_getenv(key, default):
    if key in os.environ:
        return os.environ[key]
    return default


class TestEnv(unittest.TestCase):
    def test_constructor(self):
        for m in [platform_system_aix, platform_system_linux, platform_system_sunos, platform_system_windows]:
            with mock.patch("platform.system", m):
                try:
                    common.NX_Env.Env()
                except Exception as e:
                    self.fail("Exception caught (emulating system '{}': {}".format(m(), e))
        with mock.patch("platform.system", platform_system_unknown):
            self.assertRaises(Exception, common.NX_Env.Env)

    def test_lib_loader_env_key(self):
        with mock.patch("platform.system", platform_system_aix):
            self.assertEqual("LIBPATH", common.NX_Env.Env().get_lib_loader_env_key())
        with mock.patch("platform.system", platform_system_windows):
            self.assertEqual("PATH", common.NX_Env.Env().get_lib_loader_env_key())
        for m in [platform_system_linux, platform_system_sunos]:
            with mock.patch("platform.system", m):
                self.assertEqual("LD_LIBRARY_PATH", common.NX_Env.Env().get_lib_loader_env_key())
        with mock.patch("platform.system", platform_system_sunos):
            os.environ["NX_BUILD_BITS"] = "64"
            self.assertEqual("LD_LIBRARY_PATH_64", common.NX_Env.Env().get_lib_loader_env_key())
        with mock.patch("platform.system", platform_system_aix):
            env = common.NX_Env.Env()
            with mock.patch("platform.system", platform_system_unknown):
                self.assertEqual("LD_LIBRARY_PATH", env.get_lib_loader_env_key())

    def test_extend_env(self):
        # non existing value
        env = common.NX_Env.Env()
        self.assertFalse("NX_TOTO" in os.environ)
        env.extend_environment("NX_TOTO", ["A", "B", "C"])
        self.assertEqual(os.environ["NX_TOTO"], os.pathsep.join(["A", "B", "C"]))
        env.extend_environment("NX_TOTO", ["D", "B", "C", "E"], True)
        # Duplicates are not inserted
        self.assertEqual(os.environ["NX_TOTO"], os.pathsep.join(["E", "D", "A", "B", "C"]))

    def test_extend_loader_env(self):
        env = common.NX_Env.Env()
        old_value = os.environ[env.get_lib_loader_env_key()]
        values = ["This", "Path", "And", "That", "One", "Too"]
        env.extend_lib_loader_path(values)
        self.assertEqual(old_value + os.pathsep + os.pathsep.join(values), os.environ[env.get_lib_loader_env_key()])
        values = ["This", "Duplicated", "Path", "Is", "Not", "A", "Real", "Duplicate"]
        old_value = os.environ[env.get_lib_loader_env_key()]
        env.extend_lib_loader_path(values, True)
        self.assertEqual(os.pathsep.join(["Duplicate", "Real", "A", "Not", "Is", "Duplicated", old_value]),
                         os.environ[env.get_lib_loader_env_key()])

    def test_validity(self):
        def expected_validity():
            for key in ["PLATFORM", "COMPILER_TYPE", "COMPILER_VERSION", "BUILD_BITS", "BUILD_MODE", "TOOLS"]:
                if "NX_{}".format(key) not in os.environ:
                    return False
            if os.environ["NX_TOOLS"] == "/i/dont/exists":
                return False
            return os.environ["NX_BUILD_BITS"] in ["32", "64"] and \
                os.environ["NX_BUILD_MODE"] in ["debug", "release", "tsan", "asan", "valgrind", "gcov"]

        platfms = ["AIX71", "SOL10", "LinuxUBU1204", "LinuxCentOS7", "Win64", ""]
        cmplr_typ = ["GCC", "MSVC", ""]
        cmplr_vers = ["40905", "1900", ""]
        bld_bits = ["32", "64", "", "invalid"]
        bld_mode = ["gcov", "debug", "release", "valgrind", "asan", "tsan", "", "unknown"]
        tools = ["/my/tools", "", "/i/dont/exists"]
        with mock.patch("os.getenv", os_getenv):
            with mock.patch("os.path.exists", path_exists):
                for (j, k, l, m, n, o) in itertools.product(platfms, cmplr_typ, cmplr_vers, bld_bits, bld_mode, tools):
                    setup_assert_value = setup_env(j, k, l, m, n, o, expected_validity)
                    env = common.NX_Env.Env()
                    self.assertEqual(setup_assert_value, env.valid(),
                                     msg="{}_{}_{}_{}_{}_{}".format(j, k, l, m, n, o))
                    if env.valid():
                        self.assertEqual("{}_{}_{}_{}_{}".format(j, k, l, m, n), env.platform_id)
                        self.assertEqual("{}_{}_{}_{}_{}".format(j, k, l, m, n), env.platform_id_ext)
                    else:
                        self.assertEqual("", env.platform_id)
                        self.assertEqual("", env.platform_id_ext)
                    self.assertEqual(env.platform, j)
                    self.assertEqual(env.compiler_type, k)
                    self.assertEqual(env.compiler_version, l)
                    self.assertEqual(env.build_bits, m)
                    self.assertEqual(env.build_mode, n)
                    self.assertEqual(env.tools_root_dir, o)
                    for spe_mode in ["", "special", "mode"]:
                        os.environ["NX_SPECIAL_MODE"] = spe_mode
                        e = common.NX_Env.Env()
                        if e.valid():
                            self.assertEqual("{}_{}_{}_{}_{}".format(j, k, l, m, n), e.platform_id)
                            if spe_mode:
                                self.assertEqual("{}_{}_{}_{}_{}.{}".format(j, k, l, m, n, spe_mode), e.platform_id_ext)
                            else:
                                self.assertEqual("{}_{}_{}_{}_{}".format(j, k, l, m, n), e.platform_id_ext)
                        else:
                            self.assertEqual("", e.platform_id)
                            self.assertEqual("", e.platform_id_ext)

                setup_env("Win64", "MSVC", "1900", "32", "release", "/my/tools", expected_validity, "/i/dont/exists")
                self.assertFalse(common.NX_Env.Env().valid())

    def test_deployment_validity(self):
        def expected_validity():
            for k in ["PLATFORM", "COMPILER_TYPE", "COMPILER_VERSION", "BUILD_BITS", "BUILD_MODE", "TOOLS"]:
                if "NX_{}".format(k) not in os.environ:
                    return False
            if os.environ["NX_TOOLS"] == "/i/dont/exists":
                return False
            return os.environ["NX_BUILD_BITS"] in ["32", "64"] and \
                os.environ["NX_BUILD_MODE"] in ["debug", "release"]

        platfms = ["AIX71", "SOL10", "LinuxUBU1204", "LinuxCentOS7", "Win64", ""]
        cmplr_typ = ["GCC", "MSVC", ""]
        cmplr_vers = ["40905", "1900", ""]
        bld_bits = ["32", "64", "", "invalid"]
        bld_mode = ["gcov", "debug", "release", "valgrind", "asan", "tsan", ""]
        tools = ["/my/tools", ""]
        with mock.patch("os.getenv", os_getenv):
            with mock.patch("os.path.exists", path_exists):
                for (a, b, c, d, e, f) in itertools.product(platfms, cmplr_typ, cmplr_vers, bld_bits, bld_mode, tools):
                    setup_assert_value = setup_env(a, b, c, d, e, f, expected_validity)
                    env = common.NX_Env.Env()
                    self.assertEqual(setup_assert_value, env.valid_for_deployment(),
                                     msg="{}_{}_{}_{}_{}_{}".format(a, b, c, d, e, f))

    def test_property_osname(self):
        with mock.patch("platform.system", platform_system_aix):
            env = common.NX_Env.Env()
            self.assertEqual(env.os_name, "aix")
        with mock.patch("platform.system", platform_system_sunos):
            env = common.NX_Env.Env()
            self.assertEqual(env.os_name, "sol")
        with mock.patch("platform.system", platform_system_linux):
            env = common.NX_Env.Env()
            self.assertEqual(env.os_name, "linux")
        with mock.patch("platform.system", platform_system_windows):
            env = common.NX_Env.Env()
            self.assertEqual(env.os_name, "Win64")

    def test_playground(self):
        def joiner(*args):
            return "/".join(args)

        def val_fun():
            return True

        with mock.patch("os.path.join", joiner):
            with mock.patch("os.getenv", os_getenv):
                with mock.patch("os.path.exists", path_exists):
                    setup_env("SOL10", "GCC", "50300", "64", "release", "/my/tools", val_fun, "/my/workspace")
                    e = common.NX_Env.Env()
                    self.assertEqual(e.workspace, "/my/workspace")
                    self.assertEqual(e.playground, "/my/workspace/playground")
                    self.assertEqual(e.playground_bin_dir, "/my/workspace/playground/bin/SOL10_GCC_50300_64_release")
                    self.assertEqual(e.playground_lib_dir, "/my/workspace/playground/lib/SOL10_GCC_50300_64_release")

        with mock.patch("os.path.join", joiner):
            with mock.patch("os.getenv", os_getenv):
                with mock.patch("os.path.exists", path_exists):
                    setup_env("SOL10", "GCC", "50300", "64", "release", "/my/tools", val_fun, "/my/workspace", "sp")
                    e = common.NX_Env.Env()
                    self.assertEqual(e.workspace, "/my/workspace")
                    self.assertEqual(e.playground, "/my/workspace/playground")
                    self.assertEqual(e.playground_bin_dir, "/my/workspace/playground/bin/SOL10_GCC_50300_64_release.sp")
                    self.assertEqual(e.playground_lib_dir, "/my/workspace/playground/lib/SOL10_GCC_50300_64_release.sp")
                    setup_env("SOL10", "GCC", "50300", "64", "release", "/my/tools", val_fun, "")

    def test_compiler(self):
        self.assertEqual(common.NX_Env.Env.get_valid_compilers_family(), ["GCC", "MSVC"])
        with mock.patch("os.getenv", os_getenv):
            os.environ["NX_COMPILER_TYPE"] = "GCC"
            for version, expected in [("50300", "5.3.0"), ("40905", "4.9.5"), ("40803", "4.8.3"), ("60100", "6.1.0")]:
                os.environ["NX_COMPILER_VERSION"] = version
                env = common.NX_Env.Env()
                self.assertEqual(env.compiler_version_mmp, expected)
            os.environ["NX_COMPILER_TYPE"] = "MSVC"
            for version, expected in [("1900", 14)]:
                os.environ["NX_COMPILER_VERSION"] = version
                env = common.NX_Env.Env()
                self.assertEqual(env.compiler_version_mmp, expected)
            os.environ["NX_COMPILER_TYPE"] = "UNK"
            for version, expected in [("abc", "")]:
                os.environ["NX_COMPILER_VERSION"] = version
                env = common.NX_Env.Env()
                self.assertEqual(env.compiler_version_mmp, expected)
            os.environ["NX_COMPILER_VERSION"] = "UNK"
            for version, expected in [("abc", "")]:
                env = common.NX_Env.Env()
                self.assertEqual(env.compiler_version_mmp, expected)
            # Let's set a valid compiler
            for c_type, c_version, c_id in [("GCC", "40905", "GCC_40905"), ("MSVC", "1900", "MSVC_1900")]:
                os.environ["NX_COMPILER_TYPE"] = c_type
                os.environ["NX_COMPILER_VERSION"] = c_version
                env = common.NX_Env.Env()
                self.assertEqual(env.compiler_id, c_id)
            # Last test : retrieving compiler path
            with mock.patch("os.altsep", "/"):
                os.environ["NX_TOOLS"] = "/my/tools"
                os.environ["NX_PLATFORM"] = "Win64"
                env = common.NX_Env.Env()
                self.assertEqual("/my/tools/compilers/Win64/MSVC/1900", env.compiler_path)
            with mock.patch("os.altsep", "\\"):
                os.environ["NX_TOOLS"] = "\\my\\tools"
                os.environ["NX_PLATFORM"] = "Win64"
                env = common.NX_Env.Env()
                self.assertEqual("\\my\\tools\\compilers\\Win64\\MSVC\\1900", env.compiler_path)
            with mock.patch("os.altsep", None):
                with mock.patch("os.sep", "/"):
                    os.environ["NX_TOOLS"] = "/my/tools"
                    os.environ["NX_PLATFORM"] = "Win64"
                    env = common.NX_Env.Env()
                    self.assertEqual("/my/tools/compilers/Win64/MSVC/1900", env.compiler_path)

    def test_workspace(self):
        def unix_joiner(*args):
            return "/".join(args)

        def win_joiner(*args):
            return "\\".join(args)

        def val_fun():
            return True

        with mock.patch("os.path.join", unix_joiner):
            with mock.patch("os.getenv", os_getenv):
                with mock.patch("os.path.exists", path_exists):
                    setup_env("SOL10", "GCC", "50300", "64", "release", "/my/tools", val_fun, "/my/workspace", "sp")
                    env = common.NX_Env.Env()
                    exception_raised = False
                    try:
                        env.workspace = "/i/dont/exists"
                    except Exception:
                        exception_raised = True
                    self.assertTrue(exception_raised)
                    exception_raised = False
                    try:
                        env.workspace = "./"
                    except Exception:
                        exception_raised = True
                    self.assertFalse(exception_raised)

        with mock.patch("os.path.join", win_joiner):
            with mock.patch("os.getenv", os_getenv):
                with mock.patch("os.path.exists", path_exists):
                    setup_env("SOL10", "GCC", "50300", "64", "release", "/my/tools", val_fun, "\\my\\workspace", "sp")
                    env = common.NX_Env.Env()
                    self.assertEqual("/my/workspace", env.uworkspace)


def create_test_suite():
    return unittest.TestLoader().loadTestsFromTestCase(TestEnv)
