import zipfile
import time
import tempfile
import os
import common.NX_ArtifactoryDialect
import common.NX_RestClient
import common.NX_Utils


class Archive(object):
    def __init__(self, name):
        self.__name = name
        self.__path = None
        self.__contents = set()
        self.__mapping = {}

    @property
    def name(self):
        return self.__name

    @property
    def path(self):
        return self.__path

    def download_from_artifactory(self, **kwargs):
        rest_client = common.NX_ArtifactoryDialect.ArtifactoryInterpret(**kwargs)
        quiet = kwargs.get("silent", True)
        try:
            uri = kwargs.get("uri", "")
            if not uri:
                msg = "Cannot download artefact '{}': no uri given!".format(self.name)
                raise common.NX_RestClient.RestException(msg)
            uri = '/'.join([uri, self.name])
            query = rest_client.get_artefact(uri)
            if not query.ok:
                uri = '/'.join([rest_client.url, uri])
                msg = "Cannot download artefact '{}': cannot find it under distant repository {}".format(self.name, uri)
                raise common.NX_RestClient.RestException(msg)
            chunk_size = 2**16
            where = tempfile.gettempdir()
            self.__path = os.path.join(where, os.path.basename(uri))
            if not quiet:
                length = query.headers["Content-Length"]
                total_iter = int(length) / chunk_size + 1
                iter_count = 0
                common.NX_Utils.progress_bar(iter_count, total_iter, msg="Downloading archive")
                stime = time.time()
                text_msgs = ["Downloading archive{}".format(i) for i in ["", ".", "..", "..."]]
                with open(self.path, "wb") as fd:
                    for chunk in query.iter_content(chunk_size=chunk_size):
                        fd.write(chunk)
                        iter_count += 1
                        idx = int(time.time() - stime)
                        if idx > 3:
                            stime = time.time()
                            idx = 0
                        common.NX_Utils.progress_bar(iter_count, total_iter, msg=text_msgs[idx])
                        time.sleep(0.005)
                if total_iter == iter_count:
                    print "Download complete, proceeding to deployment!"
            else:
                with open(self.path, "wb") as fd:
                    for chunk in query.iter_content(chunk_size=chunk_size):
                        fd.write(chunk)
        except common.NX_RestClient.RestException as rst_err:
            raise rst_err

    def delete(self):
        if self.path is not None:
            common.NX_Utils.erase_fs_entry(self.path)

    def uncompress(self):
        if self.path.endswith(".zip"):
            xtract_dir = self.path[:len(self.path)-len(".zip")]
            if os.path.exists(xtract_dir):
                common.NX_Utils.erase_fs_entry(xtract_dir)
            with zipfile.ZipFile(self.path) as zf:
                zf.extractall(path=xtract_dir)
            common.NX_Utils.erase_fs_entry(self.path)
            self.__path = xtract_dir
        else:
            raise Exception("Invalid Jam Archive format : only ZIP is supported")

    def has_file(self, file_name):
        return file_name in self.__contents

    def read_jam(self, mapping_entry_file=None):
        try:
            self.uncompress()
            with open(os.path.join(self.path, "contents.jam"), "r") as f:
                for line in f:
                    self.__contents.add(line.strip())
            if mapping_entry_file and self.has_file(mapping_entry_file):
                with open(os.path.join(self.path, mapping_entry_file), "r") as map_file:
                    for mapping in map_file:
                        pair = mapping.strip().split("=")
                        if len(pair) == 2:
                            self.__mapping[pair[0]] = pair[1]
        except Exception as e:
            raise e
        return self.path

    @property
    def mapping(self):
        return self.__mapping

    def get_mapping_for(self, entry):
        if entry in self.__mapping:
            return self.__mapping[entry]
        print "missing '{}'".format(entry)
        return None

    def __iter__(self):
        for item in self.__contents:
            yield item
