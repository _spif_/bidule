import os

colors = {'USE': True,
          'BOLD': '\x1b[01;1m',
          'RED': '\x1b[01;31m',
          'GREEN': '\x1b[32m',
          'YELLOW': '\x1b[33m',
          'PINK': '\x1b[35m',
          'BLUE': '\x1b[01;34m',
          'CYAN': '\x1b[36m',
          'GREY': '\x1b[37m',
          'NORMAL': '\x1b[0m',
          'cursor_on': '\x1b[?25h',
          'cursor_off': '\x1b[?25l'
          }


def print_text(combination, text):
    """
    Print a text using a color combination (':' separated string).
    :param combination: The full colorized output you want.
                        You can merge entries from colors using ':' to separate each.
    :type combination: str
    :param text: Text to colorize.
    :type text: str
    """
    print colored_text(combination, text)


def colored_text(combination, text):
    """
    Combine several color codes in order to colorize the given text. If the environment variable STD_COLOR_OUTPUT is set
    the colorization step will be skip, thus leaving the text as is.
    :param combination: The color combination, each entry being separated from the other using ':'.
    :type combination: str
    :param text: Text to colorize.
    :type text: str
    :return: Text colorized
    :rtype: str
    """
    if os.getenv("STD_COLOR_OUTPUT", None) is not None:
        return text
    return "%s%s%s" % ("".join([colors[i] for i in combination.split(":") if i in colors]), text, colors["NORMAL"])


def info(text):
    """
    Retrieve the given text enclosed within information marks.
    :param text: Text to colorize.
    :type text: str
    :return: The text, ready to be print.
    :rtype: str
    """
    return colored_text("BOLD", text)


def warning(text):
    """
    Retrieve the given text enclosed within warning marks.
    :param text: Text to colorize.
    :type text: str
    :return: The text, ready to be print.
    :rtype: str
    """
    return colored_text("YELLOW:BOLD", text)


def error(text):
    """
    Retrieve the given text enclosed within error marks.
    :param text: Text to colorize.
    :type text: str
    :return: The text, ready to be print.
    :rtype: str
    """
    return colored_text("RED:BOLD", text)


def print_error(text):
    """
    Print an error message on stdout using Error code.
    :param text: Text to colorize.
    :type text: str
    """
    print error(text)


def print_warning(text):
    """
    Print a warning message on stdout using Warning code.
    :param text: Text to colorize.
    :type text: str
    """
    print warning(text)


def print_info(text):
    """
    Print an information message on stdout using Information code.
    :param text: Text to colorize.
    :type text: str
    """
    print info(text)
