import abc
import os
import ConfigParser


class RestException(Exception):
    def __init__(self, msg, entity="", tech_details=""):
        if entity:
            self.message = "RestException from [{ENT}] : {MSG}".format(ENT=entity, MSG=msg)
        else:
            self.message = "RestException : {MSG}".format(MSG=msg)
        if tech_details:
            self.message += os.linesep + "  {}".format(tech_details)


class RestTranslator(object):
    def __init__(self):
        pass

    @abc.abstractmethod
    def translate(self, req_response):
        pass


class RestClient(object):
    def __init__(self, **kwargs):
        self.__anonymous = kwargs.get("anonymous", False)
        self.__url = kwargs.get("url", None)
        self.__user = kwargs.get("user", None)
        self.__password = kwargs.get("password", None)
        if kwargs.get("settings", ""):
            self.read_settings(kwargs.get("settings", ""), kwargs.get("section", "DEFAULT"))
        self.__url = self.__url.strip("/")
        if not self.url:
            raise RestException(msg="No URL provided!")
        if not self.anonymous:
            if not self.user:
                raise RestException(msg="No user provided!")
            if not self.password:
                raise RestException(msg="No password provided!")

    def read_settings(self, file_settings, sect):
        if not sect:
            raise RestException(msg="no section provided for reading settings from '{F}'".format(F=file_settings))
        try:
            parser = ConfigParser.ConfigParser()
            parser.read(file_settings)
            v = parser.get(sect, "anonymous")
            self.anonymous = v.upper() in ["1", "YES", "TRUE"]
            if not self.anonymous:
                self.__user = parser.get(sect, "user")
                self.__password = parser.get(sect, "password")
            self.__url = parser.get(sect, "url")
            if not self.url:
                msg = "No URL set for access described in file '{F}', section '{S}'.".format(F=file_settings, S=sect)
                raise RestException(msg)
            if not self.anonymous:
                if not self.user:
                    msg = "No user set for non-anonymous access described in file '{}', section '{}'."
                    raise RestException(msg.format(file_settings, sect))
                if not self.password:
                    msg = "No password set for non-anonymous access described in file '{F}', section '{S}'."
                    raise RestException(msg.format(F=file_settings, S=sect))
        except ConfigParser.Error as cfg_parse_error:
            raise RestException(msg="Error while reading settings '{F}'".format(F=file_settings,
                                                                                tech_details=str(cfg_parse_error)))

    @property
    def credentials(self):
        if self.anonymous:
            return None
        return self.user, self.password

    @property
    def anonymous(self):
        return self.__anonymous

    @anonymous.setter
    def anonymous(self, value):
        self.__anonymous = value

    @property
    def user(self):
        return self.__user

    @property
    def password(self):
        return self.__password

    @property
    def url(self):
        return self.__url
