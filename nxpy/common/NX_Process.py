import os
import time
import threading
import Queue
import subprocess
import platform
import NX_Colors


class Poller(object):
    def __init__(self, name, in_stream):
        self.__q = Queue.Queue()
        self.__what = in_stream
        self.__name = name
        self.__th = threading.Thread(target=self.__read)
        self.__th.daemon = True
        self.__th.start()

    @property
    def name(self):
        return self.__name

    @property
    def line(self):
        try:
            return self.__q.get_nowait()
        except Queue.Empty:
            return None

    def __read(self):
        for line in iter(self.__what.readline, ''):
            self.__q.put(line)
            time.sleep(0.05)

    def join(self):
        self.__th.join()


class Process(object):
    def __init__(self, program_path):
        self.__program = program_path
        if platform.system() == 'Windows':
            self.__program += ".exe"
        self.__last_return_code = 0
        self.__args = []
        self.__working_dir = os.getcwd()
        self.__out = []
        self.__err = []
   
    @property
    def out(self):
        return self.__out

    @property
    def err(self):
        return self.__err

    @property
    def cwd(self):
        return self.__working_dir

    @cwd.setter
    def cwd(self, path):
        self.__working_dir = path

    @property
    def args(self):
        return self.__args

    @args.setter
    def args(self, values):
        self.__args = values

    @property
    def program(self):
        return os.path.basename(self.__program)

    @property
    def program_no_ext(self):
        f, e = os.path.splitext(os.path.basename(self.__program))
        return f

    @property
    def full_path(self):
        return self.__program
   
    @property
    def program_path(self):
        return os.path.dirname(self.__program)

    @program_path.setter
    def program_path(self, path):
        tmp = os.path.join(path, self.__program)
        self.__program = tmp

    @property
    def last_return_code(self):
        return self.__last_return_code

    def __check_for_usage(self):
        if self.cwd:
            if not os.path.exists(self.cwd):
                raise Exception("Given working directory [%s] does not exists." % self.cwd)
            elif not os.path.isdir(self.cwd):
                raise Exception("Given path [%s] is not a directory." % self.cwd)
        if not os.path.exists(self.full_path):
            raise Exception("Program cannot be found at the given path [%s]!" % self.full_path)

    def __print_outputs(self, values, prefix, keep, stream, kind):
        if values:
            if type(values) is str:
                values = [i for i in values.split("\n")]
            if not values[-1]:
                values = values[:len(values)-1]
            for i in values:
                data = i.rstrip("\r\n")
                if prefix is not None:
                    if kind == "out":
                        print NX_Colors.info("{PREFIX}{DATA}".format(PREFIX=prefix, DATA=data))
                    else:
                        print NX_Colors.error("{PREFIX}{DATA}".format(PREFIX=prefix, DATA=data))
                if keep:
                    if kind == "out":
                        self.__out.append(data)
                    else:
                        self.__err.append(data)
                if stream is not None:
                    stream.write(data + os.linesep)
            if stream is not None:
                stream.flush()

    def run_and_poll(self, **kwargs):
        self.__check_for_usage()
        cmd_line = [self.full_path] + self.args
        b_keep_std_out = kwargs.get("keep_stdout", False)
        b_keep_std_err = kwargs.get("keep_stderr", False)
        stderr_prefix = kwargs.get("err_prefix", None)
        stdout_prefix = kwargs.get("out_prefix", None)
        stderr_file_path, stderr_file = kwargs.get("stderr_file_path", ""), None
        stdout_file_path, stdout_file = kwargs.get("stdout_file_path", ""), None
        if stderr_file_path:
            try:
                stderr_file = open(stderr_file_path, "w")
            except Exception as e:
                raise Exception("Cannot redirect stderr to [{}]. Reason is [{}]".format(stderr_file_path, str(e)))
        if stdout_file_path:
            try:
                stdout_file = open(stdout_file_path, "w")
            except Exception as e:
                raise Exception("Cannot redirect stdout to [{}]. Reason is [{}]".format(stdout_file_path, str(e)))
        proc = subprocess.Popen(cmd_line, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                cwd=self.cwd, universal_newlines=True, bufsize=-1)
        pout = Poller("out", proc.stdout)
        perr = Poller("err", proc.stderr)
        # Poll process for new output until finished
        while proc.poll() is None:
            self.__print_outputs(pout.line, stdout_prefix, b_keep_std_out, stdout_file, "out")
            self.__print_outputs(perr.line, stderr_prefix, b_keep_std_err, stderr_file, "err")
        for p in [pout, perr]:
            p.join()
            line = p.line
            pref = stdout_prefix if p.name == "out" else stderr_prefix
            keep = b_keep_std_out if p.name == "out" else b_keep_std_err
            f = stdout_file if p.name == "out" else stderr_file
            while line is not None:
                self.__print_outputs(line, pref, keep, f, p.name)
                line = p.line

        out, err = proc.communicate()
        self.__print_outputs(err, stderr_prefix, b_keep_std_err, stderr_file, "err")
        self.__print_outputs(out, stdout_prefix, b_keep_std_out, stdout_file, "out")
        for f in [stdout_file, stderr_file]:
            if f is not None:
                f.flush()
                f.close()
        self.__last_return_code = proc.returncode
        return proc.returncode

    def run(self, **kwargs):
        self.__check_for_usage()
        cmd_line = [self.full_path] + self.args
        b_keep_std_out = kwargs.get("keep_stdout", False)
        b_keep_std_err = kwargs.get("keep_stderr", False)
        stderr_prefix = kwargs.get("err_prefix", None)
        stdout_prefix = kwargs.get("out_prefix", None)
        stderr_file_path, stderr_file = kwargs.get("stderr_file_path", ""), None
        stdout_file_path, stdout_file = kwargs.get("stdout_file_path", ""), None
        if stderr_file_path:
            try:
                stderr_file = open(stderr_file_path, "w")
            except Exception as e:
                raise Exception("Cannot redirect stderr to [{}]. Reason is [{}]".format(stderr_file_path, str(e)))
        if stdout_file_path:
            try:
                stdout_file = open(stdout_file_path, "w")
            except Exception as e:
                raise Exception("Cannot redirect stdout to [{}]. Reason is [{}]".format(stdout_file_path, str(e)))
        proc = subprocess.Popen(cmd_line, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                cwd=self.cwd, universal_newlines=True, bufsize=-1)
        out, err = proc.communicate()
        self.__print_outputs(err, stderr_prefix, b_keep_std_err, stderr_file, "err")
        self.__print_outputs(out, stdout_prefix, b_keep_std_out, stdout_file, "out")
        for f in [stdout_file, stderr_file]:
            if f is not None:
                f.flush()
                f.close()
        self.__last_return_code = proc.returncode
        return proc.returncode
