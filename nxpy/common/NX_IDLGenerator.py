import os
import NX_Colors
import NX_Env
import NX_Process


class IDL(NX_Process.Process):
    def __init__(self, program_path):
        super(IDL, self).__init__(os.path.basename(program_path))
        self.__env = NX_Env.Env()
        self.__dico = {"IN": "", "OUT": "", "GPERF": "", "IDL": ""}
        self.ProgramPath = os.path.dirname(program_path)
        self.__paths = []
        self.__tool_env = {}
        self.__rule = ""

    @property
    def gperf(self):
        return self.__dico["GPERF"]

    @gperf.setter
    def gperf(self, prog):
        self.__dico["GPERF"] = prog

    @property
    def env(self):
        return self.__env

    @property
    def input(self):
        return self.__dico["IN"]

    @input.setter
    def input(self, in_file):
        self.__dico["IN"] = in_file

    @property
    def output(self):
        return self.__dico["OUT"]

    @output.setter
    def output(self, path):
        self.__dico["OUT"] = path

    @property
    def ld_path(self):
        return self.__paths

    def add_loader_path(self, paths):
        for p in paths:
            if p not in self.__paths:
                self.__paths.append(p)

    @property
    def tool_env(self):
        return self.__tool_env

    @tool_env.setter
    def tool_env(self, env):
        self.__tool_env.update(env)

    @property
    def rule(self):
        return self.__rule

    @rule.setter
    def rule(self, rule):
        self.__rule = rule

    def run(self):
        key = self.env.get_lib_loader_env_key()
        tmp = "" if key not in os.environ else os.environ[key]
        the_rule = self.rule.format(**self.__dico)
        self.args = the_rule.split(" ")[1:]
        if self.env.os_name != "Win64":
            self.add_loader_path([os.path.join(self.env.compiler_path, "lib{BITS}".format(BITS=self.env.build_bits))])
        self.env.extend_lib_loader_path(self.ld_path)
        for env_var in self.tool_env:
            self.env.extend_environment(env_var, self.tool_env[env_var], False)
        super(IDL, self).run(keep_stderr=True)
        if self.last_return_code != 0:
            for i in self.err:
                NX_Colors.print_error("IDL Generator : " + i)
        os.environ[self.env.get_lib_loader_env_key()] = tmp
