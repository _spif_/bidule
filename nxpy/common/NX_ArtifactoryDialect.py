# coding: utf-8

import NX_RestClient
import requests
import ast


class JSonTranslator(NX_RestClient.RestTranslator):
    def __init__(self):
        super(JSonTranslator, self).__init__()
        self.__hdr = None
        self.__content = None
        self.__rc = False

    def translate(self, response):
        if not isinstance(response, requests.Response):
            raise NX_RestClient.RestException(entity="JSonTranslator",
                                              msg="Provided object is not a valid requests.Response object.")
        if "Content-Type" not in response.headers:
            raise NX_RestClient.RestException(entity="JSonTranslator", msg="Invalid header!")
        if not response.headers['Content-Type'] in ["application/json"]:
            if not response.headers['Content-Type'].startswith("application/vnd.org.jfrog.artifactory."):
                msg = "Given response format is not in a valid artifactory format (received {})"
                msg = msg.format(response.headers['Content-Type'])
                raise NX_RestClient.RestException(entity="JSonTranslator", msg=msg)
            content_type = response.headers['Content-Type']
            if content_type.find("+json") == -1:
                msg = "Given response format is not in a valid json format (received {})."
                msg = msg.format(response.headers['Content-Type'])
                raise NX_RestClient.RestException(entity="JSonTranslator", msg=msg)

        self.__rc = response.ok
        self.__hdr = response.headers
        self.__content = response.json()

    @property
    def ok(self):
        return self.__rc

    @property
    def content(self):
        return self.__content

    @property
    def header(self):
        return self.__hdr


class ArtifactoryInterpret(NX_RestClient.RestClient):
    def __init__(self, **kwargs):
        super(ArtifactoryInterpret, self).__init__(**kwargs)
        self.__api_root = "/api"
        self.__translator = None
        self.__groupId = kwargs.get("groupId", "")

    @property
    def group_id(self):
        return self.__groupId

    @group_id.setter
    def group_id(self, group_id):
        if group_id.endswith("/"):
            self.__groupId = group_id
        else:
            self.__groupId = group_id + "/"

    @property
    def api_root(self):
        return self.__api_root

    @property
    def translator(self):
        return self.__translator

    @translator.setter
    def translator(self, translator):
        if isinstance(translator, NX_RestClient.RestTranslator):
            self.__translator = translator
        else:
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Invalid translator type.")

    def get_storage_summary_info(self, **kwargs):
        try:
            rsrc = self.url + self.api_root + "/storageinfo/"
            if not self.credentials:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                                  msg="This request requires credentials.")
            response = requests.get(rsrc, auth=self.credentials, **kwargs)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot retrieve Storage Summary Info",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def get_last_item_modified(self, path, **kwargs):
        try:
            rsrc = self.url + self.api_root + "/storage/" + self.group_id + path + "?lastModified"
            if self.credentials:
                response = requests.get(rsrc, auth=self.credentials, **kwargs)
            else:
                response = requests.get(rsrc, **kwargs)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot retrieve Last Item Modified",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def get_item_properties(self, item_path, **kwargs):
        try:
            rsrc = self.url + self.api_root + "/storage/" + self.group_id + item_path
            if kwargs.get("all", False):
                rsrc += "?properties"
            if "all" in kwargs:
                del kwargs["all"]
            if self.credentials:
                response = requests.get(rsrc, auth=self.credentials, **kwargs)
            else:
                response = requests.get(rsrc, **kwargs)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot get Item Properties",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def get_item_children(self, item_path, info_keys, **kwargs):
        try:
            rsrc = self.url + self.api_root + "/storage/" + self.group_id + item_path
            if self.credentials:
                response = requests.get(rsrc, auth=self.credentials, **kwargs)
            else:
                response = requests.get(rsrc, **kwargs)
            if self.translator:
                self.translator.translate(response)
                children = {c["uri"][1:]: {} for c in self.translator.content["children"]}
                for c in self.translator.content["children"]:
                    for i in c:
                        if i in info_keys:
                            children[c["uri"][1:]][i] = c[i]
                return children
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot get Item Children",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def get_item_stats(self, item_path, **kwargs):
        try:
            rsrc = self.url + self.api_root + "/storage/" + self.group_id + item_path + "?stats"
            if self.credentials:
                response = requests.get(rsrc, auth=self.credentials, **kwargs)
            else:
                response = requests.get(rsrc, **kwargs)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot retrieve Item Stats",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def get_artefact(self, artefact_path, **kwargs):
        try:
            rsrc = self.url + "/" + self.group_id + artefact_path
            if not self.credentials and not self.anonymous:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                                  msg="This request requires credentials.")
            if not self.anonymous:
                return requests.get(rsrc, auth=self.credentials, stream=True, **kwargs)
            else:
                return requests.get(rsrc, stream=True, **kwargs)
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot download Artefacts",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def create_directory(self, path, **kwargs):
        try:
            rsrc = self.url + "/" + path
            if not self.credentials:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                                  msg="This request requires credentials.")
            response = requests.get(rsrc, auth=self.credentials, **kwargs)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot Create Directory",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def get_repository_replication_configuration(self, **kwargs):
        try:
            rsrc = self.url + self.api_root + "/replications/" + self.group_id
            if not self.credentials:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                                  msg="This request requires credentials.")
            response = requests.get(rsrc, auth=self.credentials, **kwargs)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                              msg="Cannot retrieve Repository Replication Configuration",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def set_item_properties(self, item_path, properties, recursive=False):
        def check_key(key_value):
            for c in ")(}{][*+^$\\/~`!@#%&<>;=,±§ ":
                if c in key_value:
                    error_msg = "Invalid property key '{}'".format(key_value)
                    raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg=error_msg)

        properties_converted = ""
        for item_property in properties:
            check_key(item_property)
            if type(properties[item_property]) is list:
                to_str = ",".join([str(item) for item in properties[item_property]])
            else:
                to_str = str(properties[item_property])
            to_str = "=".join([item_property, to_str])
            properties_converted = to_str if not properties_converted else "|".join([properties_converted, to_str])

        rsrc = self.url + self.api_root + "/".join(["/storage", self.group_id[:len(self.group_id)-1], item_path])
        props = {"properties": properties_converted, "recursive": "1" if recursive else "0"}
        try:
            response = requests.put(rsrc, auth=self.credentials, params=props)
            return response.ok, response.reason if not response.ok else ""
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot set artifact's properties.",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def deploy_artifact(self, file_to_upload, repo, path_in_repo):
        try:
            rsrc = self.url + "/".join(["", repo, path_in_repo])
            with open(file_to_upload, "rb") as f:
                response = requests.put(rsrc, data=f, auth=self.credentials)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot push artifact.",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def delete_artifact(self, path_in_repo):
        try:
            rsrc = "/".join([self.url, self.group_id, path_in_repo])
            response = requests.delete(rsrc, auth=self.credentials)
            if not response.ok:
                d = eval(response.content)
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                                  msg=d["errors"][0]["message"])
            return response.ok
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot remove artifact.",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def copy_artifact(self, **kwargs):
        try:
            if "src_repo" not in kwargs:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="No source repository set!")
            if "tgt_repo" not in kwargs:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="No target repository set!")
            if "src_file_path" not in kwargs:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="No source file set!")
            if "tgt_file_path" not in kwargs:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="No target file set!")
            rsrc = self.url + self.api_root + "/".join(["/copy", kwargs["src_repo"], kwargs["src_file_path"]])
            rsrc += "?to=/" + "/".join([kwargs["tgt_repo"], kwargs["tgt_file_path"]])
            if "dry" in kwargs:
                rsrc += "&dry=1"
            if not self.credentials:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                                  msg="This request requires credentials.")
            response = requests.post(rsrc, auth=self.credentials)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot copy artifact.",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def property_search(self, properties, **kwargs):
        try:
            rsrc = self.url + self.api_root + "/search/prop/"
            if not self.credentials:
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                                  msg="This request requires credentials.")
            params = kwargs.get("params", {})
            params.update(properties)
            params.update({"repos": self.group_id.strip("/")})
            if "params" in kwargs:
                del kwargs["params"]
            response = requests.get(rsrc, auth=self.credentials, params=params, **kwargs)
            if self.translator:
                self.translator.translate(response)
                return self.translator
            return response
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                              msg="Cannot retrieve Repository Replication Configuration",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))

    def delete_item_properties(self, repo_in_path, properties):
        try:
            rsrc = "/".join([self.url, "api", "storage", self.group_id, repo_in_path])
            if type(properties) is str:
                props = properties
            else:
                props = ",".join(properties)
            response = requests.delete(rsrc, auth=self.credentials, params={"properties": props})
            if not response.ok:
                dico_content = eval(response.content)
                raise NX_RestClient.RestException(entity="ArtifactoryInterpret",
                                                  msg=dico_content["errors"][0]["message"])
            return response.ok
        except requests.HTTPError as e:
            msg = ast.literal_eval(e.message)
            raise NX_RestClient.RestException(entity="ArtifactoryInterpret", msg="Cannot remove artifact.",
                                              tech_details="{} (rc={})".format(msg["errors"][0]["message"],
                                                                               msg["errors"][0]["status"]))
