import os

top="."
out="build"

def options(opt):
    opt.load("compiler_cxx msvc")

def configure(cfg):
    cfg.env.MSVC_VERSIONS = ['msvc 14.0']
    cfg.env.MSVC_TARGETS = ['x86']
    try:
       cfg.load("compiler_cxx msvc")
    except :
        pass

def build(bld):
    bld.program(target="out.exe", source="main.cpp")

def pkg(ctx):
    pass